//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;


namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Interface for data connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public interface IDataConnectionProperties
    {
        /// <summary>   Adds propertyName. </summary>
        /// <param name="propertyName"> Name of the property. </param>
        void Add( string propertyName );

        /// <summary>   Query if this object contains the given propertyName. </summary>
        /// <param name="propertyName"> Name of the property. </param>
        /// <returns>   True if the object is in this collection, false if not. </returns>
        bool Contains( string propertyName );

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        bool IsComplete { get; }

        /// <summary>   Gets a value indicating whether this object is extensible. </summary>
        /// <value> True if this object is extensible, false if not. </value>
        bool IsExtensible { get; }

        /// <summary>   Parses. </summary>
        /// <param name="s">    The string. </param>
        void Parse( string s );

        /// <summary>   Event queue for all listeners interested in PropertyChanged events. </summary>
        event EventHandler PropertyChanged;

        /// <summary>   Removes the given propertyName. </summary>
        /// <param name="propertyName"> Name of the property. </param>
        void Remove( string propertyName );

        /// <summary>   Resets the given propertyName. </summary>
        void Reset();

        /// <summary>   Resets the given propertyName. </summary>
        /// <param name="propertyName"> Name of the property. </param>
        void Reset( string propertyName );

        /// <summary>   Tests this object. </summary>
        void Test();

        /// <summary>
        /// Indexer to get or set items within this collection using array index syntax.
        /// </summary>
        /// <param name="propertyName"> Name of the property. </param>
        /// <returns>   The indexed item. </returns>
        object this[string propertyName] { get; set; }

        /// <summary>   Converts this object to a display string. </summary>
        /// <returns>   This object as a string. </returns>
        string ToDisplayString();

        /// <summary>   Converts this object to a full string. </summary>
        /// <returns>   This object as a string. </returns>
        string ToFullString();
    }
}
