//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Interface for data connection user interface control. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public interface IDataConnectionUIControl
    {
        /// <summary>   Initializes this object. </summary>
        /// <param name="connectionProperties"> The connection properties. </param>
        void Initialize( IDataConnectionProperties connectionProperties );

        /// <summary>   Loads the properties. </summary>
        void LoadProperties();
    }
}
