# About

isr.Microsoft.Data.ConnectionUI.Dialog is a .Net library supporting data connections operations.
This a port of Microsoft.Data.ConnectionUI to .NET Core.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Microsoft.Data.ConnectionUI.Dialog is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Connection UI Repository].

[Connection UI Repository]: https://bitbucket.org/davidhary/dn.connectionui

