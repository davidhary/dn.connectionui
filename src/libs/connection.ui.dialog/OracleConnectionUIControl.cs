//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   An oracle connection user interface control. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public partial class OracleConnectionUIControl : UserControl, IDataConnectionUIControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OracleConnectionUIControl()
        {
            this.InitializeComponent();
            this.RightToLeft = RightToLeft.Inherit;

            int requiredHeight = LayoutUtils.GetPreferredCheckBoxHeight( this.savePasswordCheckBox );
            if ( this.savePasswordCheckBox.Height < requiredHeight )
            {
                this.savePasswordCheckBox.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
                this.loginTableLayoutPanel.Height += this.loginTableLayoutPanel.Margin.Bottom;
                this.loginTableLayoutPanel.Margin = new Padding( this.loginTableLayoutPanel.Margin.Left, this.loginTableLayoutPanel.Margin.Top, this.loginTableLayoutPanel.Margin.Right, 0 );
            }
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <param name="connectionProperties"> The connection properties. </param>
        public void Initialize( IDataConnectionProperties connectionProperties )
        {
            if ( connectionProperties == null )
            {
                throw new ArgumentNullException( nameof( connectionProperties ) );
            }

            if ( !(connectionProperties is OracleConnectionProperties) &&
                !(connectionProperties is OleDBOracleConnectionProperties) )
            {
                throw new ArgumentException( Dialog.Resources.Strings.OracleConnectionUIControl_InvalidConnectionProperties );
            }

            if ( connectionProperties is OdbcConnectionProperties )
            {
                // ODBC does not support saving the password
                this.savePasswordCheckBox.Enabled = false;
            }

            this.Properties = connectionProperties;
        }

        /// <summary>   Loads the properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public void LoadProperties()
        {
            this._Loading = true;

            this.serverTextBox.Text = this.Properties[this.ServerProperty] as string;
            this.userNameTextBox.Text = this.Properties[this.UserNameProperty] as string;
            this.passwordTextBox.Text = this.Properties[this.PasswordProperty] as string;
            this.savePasswordCheckBox.Checked = !(this.Properties is OdbcConnectionProperties) ? ( bool ) this.Properties["Persist Security Info"] : false;

            this._Loading = false;
        }

        /// <summary>   Simulate RTL mirroring. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRightToLeftChanged( EventArgs e )
        {
            base.OnRightToLeftChanged( e );
            if ( this.ParentForm != null &&
                this.ParentForm.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.serverLabel, this.serverTextBox );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.serverLabel, this.serverTextBox );
            }
        }

        /// <summary>   Scales a control's location, size, padding and margin. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="factor">       The factor by which the height and width of the control will be
        ///                             scaled. </param>
        /// <param name="specified">    A <see cref="T:System.Windows.Forms.BoundsSpecified" /> value
        ///                             that specifies the bounds of the control to use when defining its
        ///                             size and position. </param>
        protected override void ScaleControl( SizeF factor, BoundsSpecified specified )
        {
            Size baseSize = this.Size;
            this.MinimumSize = Size.Empty;
            base.ScaleControl( factor, specified );
            this.MinimumSize = new Size(
                ( int ) Math.Round( ( float ) baseSize.Width * factor.Width ),
                ( int ) Math.Round( ( float ) baseSize.Height * factor.Height ) );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            if ( this.Parent == null )
            {
                this.OnFontChanged( e );
            }
        }

        /// <summary>   Sets a server. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetServer( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.ServerProperty] = (this.serverTextBox.Text.Trim().Length > 0) ? this.serverTextBox.Text.Trim() : null;
            }
        }

        /// <summary>   Sets user name. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetUserName( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.UserNameProperty] = (this.userNameTextBox.Text.Trim().Length > 0) ? this.userNameTextBox.Text.Trim() : null;
            }
        }

        /// <summary>   Sets a password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetPassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.PasswordProperty] = (this.passwordTextBox.Text.Length > 0) ? this.passwordTextBox.Text : null;
                this.passwordTextBox.Text = this.passwordTextBox.Text; // forces reselection of all text
            }
        }

        /// <summary>   Sets save password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetSavePassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["Persist Security Info"] = this.savePasswordCheckBox.Checked;
            }
        }

        /// <summary>   Trim control text. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TrimControlText( object sender, EventArgs e )
        {
            Control c = sender as Control;
            c.Text = c.Text.Trim();
        }

        /// <summary>   Gets the server property. </summary>
        /// <value> The server property. </value>
        private string ServerProperty => !(this.Properties is OdbcConnectionProperties) ? "Data Source" : "SERVER";

        /// <summary>   Gets the user name property. </summary>
        /// <value> The user name property. </value>
        private string UserNameProperty => !(this.Properties is OdbcConnectionProperties) ? "User ID" : "UID";

        /// <summary>   Gets the password property. </summary>
        /// <value> The password property. </value>
        private string PasswordProperty => !(this.Properties is OdbcConnectionProperties) ? "Password" : "PWD";

        /// <summary>   Gets or sets the properties. </summary>
        /// <value> The properties. </value>
        private IDataConnectionProperties Properties { get; set; }

        /// <summary>   True to loading. </summary>
        private bool _Loading;
    }
}
