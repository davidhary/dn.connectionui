//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Permissions;

using ThreadState = System.Threading.ThreadState;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A SQL connection user interface control. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public partial class SqlConnectionUIControl : UserControl, IDataConnectionUIControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public SqlConnectionUIControl()
        {
            this.InitializeComponent();
            this.RightToLeft = RightToLeft.Inherit;

            int requiredHeight = LayoutUtils.GetPreferredCheckBoxHeight( this.savePasswordCheckBox );
            if ( this.savePasswordCheckBox.Height < requiredHeight )
            {
                this.savePasswordCheckBox.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
                this.loginTableLayoutPanel.Height += this.loginTableLayoutPanel.Margin.Bottom;
                this.loginTableLayoutPanel.Margin = new Padding( this.loginTableLayoutPanel.Margin.Left, this.loginTableLayoutPanel.Margin.Top, this.loginTableLayoutPanel.Margin.Right, 0 );
            }

            // Apparently WinForms automatically sets the accessible name for text boxes
            // based on a label previous to it, but does not do the same when it is
            // proceeded by a radio button.  So, simulate that behavior here
            this.selectDatabaseComboBox.AccessibleName = TextWithoutMnemonics( this.selectDatabaseRadioButton.Text );
            this.attachDatabaseTextBox.AccessibleName = TextWithoutMnemonics( this.attachDatabaseRadioButton.Text );

            this._UiThread = Thread.CurrentThread;
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <param name="connectionProperties"> The connection properties. </param>
        public void Initialize( IDataConnectionProperties connectionProperties )
        {
            if ( connectionProperties == null )
            {
                throw new ArgumentNullException( nameof( connectionProperties ) );
            }

            if ( !(connectionProperties is SqlConnectionProperties) &&
                !(connectionProperties is OleDBSqlConnectionProperties) )
            {
                throw new ArgumentException( Dialog.Resources.Strings.SqlConnectionUIControl_InvalidConnectionProperties );
            }

            if ( connectionProperties is OleDBSqlConnectionProperties )
            {
                this._CurrentOleDBProvider = connectionProperties["Provider"] as string;
            }

            if ( connectionProperties is OdbcConnectionProperties )
            {
                // ODBC does not support saving the password
                this.savePasswordCheckBox.Enabled = false;
            }

            this.Properties = new ControlProperties( connectionProperties );
        }

        /// <summary>   Loads the properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public void LoadProperties()
        {
            this._Loading = true;

            if ( this._CurrentOleDBProvider != this.Properties.Provider )
            {
                this.selectDatabaseComboBox.Items.Clear(); // a provider change requires a refresh here
                this._CurrentOleDBProvider = this.Properties.Provider;
            }

            this.serverComboBox.Text = this.Properties.ServerName;
            if ( this.Properties.UseWindowsAuthentication )
            {
                this.windowsAuthenticationRadioButton.Checked = true;
            }
            else
            {
                this.sqlAuthenticationRadioButton.Checked = true;
            }
            if ( this._CurrentUserInstanceSetting != this.Properties.UserInstance )
            {
                this.selectDatabaseComboBox.Items.Clear(); // this change requires a refresh here
            }
            this._CurrentUserInstanceSetting = this.Properties.UserInstance;
            this.userNameTextBox.Text = this.Properties.UserName;
            this.passwordTextBox.Text = this.Properties.Password;
            this.savePasswordCheckBox.Checked = this.Properties.SavePassword;
            if ( this.Properties.DatabaseFile == null || this.Properties.DatabaseFile.Length == 0 )
            {
                this.selectDatabaseRadioButton.Checked = true;
                this.selectDatabaseComboBox.Text = this.Properties.DatabaseName;
                this.attachDatabaseTextBox.Text = null;
                this.logicalDatabaseNameTextBox.Text = null;
            }
            else
            {
                this.attachDatabaseRadioButton.Checked = true;
                this.selectDatabaseComboBox.Text = null;
                this.attachDatabaseTextBox.Text = this.Properties.DatabaseFile;
                this.logicalDatabaseNameTextBox.Text = this.Properties.LogicalDatabaseName;
            }

            this._Loading = false;
        }

        /// <summary>   Simulate RTL mirroring. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRightToLeftChanged( EventArgs e )
        {
            base.OnRightToLeftChanged( e );
            if ( this.ParentForm != null &&
                this.ParentForm.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.serverLabel, this.serverTableLayoutPanel );
                LayoutUtils.MirrorControl( this.windowsAuthenticationRadioButton );
                LayoutUtils.MirrorControl( this.sqlAuthenticationRadioButton );
                LayoutUtils.MirrorControl( this.loginTableLayoutPanel );
                LayoutUtils.MirrorControl( this.selectDatabaseRadioButton );
                LayoutUtils.MirrorControl( this.selectDatabaseComboBox );
                LayoutUtils.MirrorControl( this.attachDatabaseRadioButton );
                LayoutUtils.MirrorControl( this.attachDatabaseTableLayoutPanel );
                LayoutUtils.MirrorControl( this.logicalDatabaseNameLabel );
                LayoutUtils.MirrorControl( this.logicalDatabaseNameTextBox );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.logicalDatabaseNameTextBox );
                LayoutUtils.UnmirrorControl( this.logicalDatabaseNameLabel );
                LayoutUtils.UnmirrorControl( this.attachDatabaseTableLayoutPanel );
                LayoutUtils.UnmirrorControl( this.attachDatabaseRadioButton );
                LayoutUtils.UnmirrorControl( this.selectDatabaseComboBox );
                LayoutUtils.UnmirrorControl( this.selectDatabaseRadioButton );
                LayoutUtils.UnmirrorControl( this.loginTableLayoutPanel );
                LayoutUtils.UnmirrorControl( this.sqlAuthenticationRadioButton );
                LayoutUtils.UnmirrorControl( this.windowsAuthenticationRadioButton );
                LayoutUtils.UnmirrorControl( this.serverLabel, this.serverTableLayoutPanel );
            }
        }

        /// <summary>   Scales a control's location, size, padding and margin. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="factor">       The factor by which the height and width of the control will be
        ///                             scaled. </param>
        /// <param name="specified">    A <see cref="T:System.Windows.Forms.BoundsSpecified" /> value
        ///                             that specifies the bounds of the control to use when defining its
        ///                             size and position. </param>
        protected override void ScaleControl( SizeF factor, BoundsSpecified specified )
        {
            Size baseSize = this.Size;
            this.MinimumSize = Size.Empty;
            base.ScaleControl( factor, specified );
            this.MinimumSize = new Size(
                ( int ) Math.Round( ( float ) baseSize.Width * factor.Width ),
                ( int ) Math.Round( ( float ) baseSize.Height * factor.Height ) );
        }

        /// <summary>   Processes a dialog key. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="keyData">  One of the <see cref="T:System.Windows.Forms.Keys" /> values that
        ///                         represents the key to process. </param>
        /// <returns>
        /// <see langword="true" /> if the key was processed by the control; otherwise,
        /// <see langword="false" />.
        /// </returns>
        protected override bool ProcessDialogKey( Keys keyData )
        {
            if ( this.ActiveControl == this.selectDatabaseRadioButton &&
                (keyData & Keys.KeyCode) == Keys.Down )
            {
                _ = this.attachDatabaseRadioButton.Focus();
                return true;
            }
            if ( this.ActiveControl == this.attachDatabaseRadioButton &&
                (keyData & Keys.KeyCode) == Keys.Down )
            {
                _ = this.selectDatabaseRadioButton.Focus();
                return true;
            }
            return base.ProcessDialogKey( keyData );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            if ( this.Parent == null )
            {
                this.OnFontChanged( e );
            }
        }

        /// <summary>   Handles the combo box down key. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Key event information. </param>
        private void HandleComboBoxDownKey( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Down )
            {
                if ( sender == this.serverComboBox )
                {
                    this.EnumerateServers( sender, e );
                }
                if ( sender == this.selectDatabaseComboBox )
                {
                    this.EnumerateDatabases( sender, e );
                }
            }
        }

        /// <summary>   Enumerate servers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void EnumerateServers( object sender, System.EventArgs e )
        {
            if ( this.serverComboBox.Items.Count == 0 )
            {
                Cursor currentCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    if ( this._ServerEnumerationThread == null ||
                        this._ServerEnumerationThread.ThreadState == ThreadState.Stopped )
                    {
                        this.EnumerateServers();
                    }
                    else if ( this._ServerEnumerationThread.ThreadState == ThreadState.Running )
                    {
                        // Wait for the asynchronous enumeration to finish
                        this._ServerEnumerationThread.Join();

                        // Populate the combo box now, rather than waiting for
                        // the asynchronous call to be marshaled back to the UI
                        // thread
                        this.PopulateServerComboBox();
                    }
                }
                finally
                {
                    Cursor.Current = currentCursor;
                }
            }
        }

        /// <summary>   Sets a server. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetServer( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.ServerName = this.serverComboBox.Text;
                if ( this.serverComboBox.Items.Count == 0 && this._ServerEnumerationThread == null )
                {
                    // Start an enumeration of servers
                    this._ServerEnumerationThread = new Thread( new ThreadStart( this.EnumerateServers ) );
                    this._ServerEnumerationThread.Start();
                }
            }
            this.SetDatabaseGroupBoxStatus( sender, e );
            this.selectDatabaseComboBox.Items.Clear(); // a server change requires a refresh here
        }

        /// <summary>   Refresh servers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void RefreshServers( object sender, System.EventArgs e )
        {
            this.serverComboBox.Items.Clear();
            this.EnumerateServers( sender, e );
        }

        /// <summary>   Sets authentication option. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetAuthenticationOption( object sender, EventArgs e )
        {
            if ( this.windowsAuthenticationRadioButton.Checked )
            {
                if ( !this._Loading )
                {
                    this.Properties.UseWindowsAuthentication = true;
                    this.Properties.UserName = null;
                    this.Properties.Password = null;
                    this.Properties.SavePassword = false;
                }
                this.loginTableLayoutPanel.Enabled = false;
            }
            else /* if (sqlAuthenticationRadioButton.Checked) */
            {
                if ( !this._Loading )
                {
                    this.Properties.UseWindowsAuthentication = false;
                    this.SetUserName( sender, e );
                    this.SetPassword( sender, e );
                    this.SetSavePassword( sender, e );
                }
                this.loginTableLayoutPanel.Enabled = true;
            }
            this.SetDatabaseGroupBoxStatus( sender, e );
            this.selectDatabaseComboBox.Items.Clear(); // an authentication change requires a refresh here
        }

        /// <summary>   Sets user name. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetUserName( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.UserName = this.userNameTextBox.Text;
            }
            this.SetDatabaseGroupBoxStatus( sender, e );
            this.selectDatabaseComboBox.Items.Clear(); // a user name change requires a refresh here
        }

        /// <summary>   Sets a password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetPassword( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.Password = this.passwordTextBox.Text;
                this.passwordTextBox.Text = this.passwordTextBox.Text; // forces reselection of all text
            }
            this.selectDatabaseComboBox.Items.Clear(); // a password change requires a refresh here
        }

        /// <summary>   Sets save password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetSavePassword( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.SavePassword = this.savePasswordCheckBox.Checked;
            }
        }

        /// <summary>   Sets database group box status. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetDatabaseGroupBoxStatus( object sender, System.EventArgs e )
        {
            this.databaseGroupBox.Enabled = this.serverComboBox.Text.Trim().Length > 0 &&
                (this.windowsAuthenticationRadioButton.Checked ||
                this.userNameTextBox.Text.Trim().Length > 0);
        }

        /// <summary>   Sets database option. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetDatabaseOption( object sender, System.EventArgs e )
        {
            if ( this.selectDatabaseRadioButton.Checked )
            {
                this.SetDatabase( sender, e );
                this.SetAttachDatabase( sender, e );
                this.selectDatabaseComboBox.Enabled = true;
                this.attachDatabaseTableLayoutPanel.Enabled = false;
                this.logicalDatabaseNameLabel.Enabled = false;
                this.logicalDatabaseNameTextBox.Enabled = false;
            }
            else /* if (attachDatabaseRadioButton.Checked) */
            {
                this.SetAttachDatabase( sender, e );
                this.SetLogicalFilename( sender, e );
                this.selectDatabaseComboBox.Enabled = false;
                this.attachDatabaseTableLayoutPanel.Enabled = true;
                this.logicalDatabaseNameLabel.Enabled = true;
                this.logicalDatabaseNameTextBox.Enabled = true;
            }
        }

        /// <summary>   Sets a database. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetDatabase( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.DatabaseName = this.selectDatabaseComboBox.Text;
                if ( this.selectDatabaseComboBox.Items.Count == 0 && this._DatabaseEnumerationThread == null )
                {
                    // Start an enumeration of databases
                    this._DatabaseEnumerationThread = new Thread( new ThreadStart( this.EnumerateDatabases ) );
                    this._DatabaseEnumerationThread.Start();
                }
            }
        }

        /// <summary>   Enumerate databases. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void EnumerateDatabases( object sender, System.EventArgs e )
        {
            if ( this.selectDatabaseComboBox.Items.Count == 0 )
            {
                Cursor currentCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    if ( this._DatabaseEnumerationThread == null ||
                        this._DatabaseEnumerationThread.ThreadState == ThreadState.Stopped )
                    {
                        this.EnumerateDatabases();
                    }
                    else if ( this._DatabaseEnumerationThread.ThreadState == ThreadState.Running )
                    {
                        // Wait for the asynchronous enumeration to finish
                        this._DatabaseEnumerationThread.Join();

                        // Populate the combo box now, rather than waiting for
                        // the asynchronous call to be marshaled back to the UI
                        // thread
                        this.PopulateDatabaseComboBox();
                    }
                }
                finally
                {
                    Cursor.Current = currentCursor;
                }
            }
        }

        /// <summary>   Sets attach database. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetAttachDatabase( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.DatabaseFile = this.selectDatabaseRadioButton.Checked ? null : this.attachDatabaseTextBox.Text;
            }
        }

        /// <summary>   Sets logical filename. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetLogicalFilename( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties.LogicalDatabaseName = this.selectDatabaseRadioButton.Checked ? null : this.logicalDatabaseNameTextBox.Text;
            }
        }

        /// <summary>   Browses. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Browse( object sender, System.EventArgs e )
        {
            OpenFileDialog fileDialog = new() {
                Title = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileTitle,
                Multiselect = false,
                RestoreDirectory = true,
                Filter = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileFilter,
                DefaultExt = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileDefaultExt
            };
            if ( this.Container != null )
            {
                this.Container.Add( fileDialog );
            }
            try
            {
                DialogResult result = fileDialog.ShowDialog( this.ParentForm );
                if ( result == DialogResult.OK )
                {
                    this.attachDatabaseTextBox.Text = fileDialog.FileName.Trim();
                }
            }
            finally
            {
                if ( this.Container != null )
                {
                    this.Container.Remove( fileDialog );
                }
                fileDialog.Dispose();
            }
        }

        /// <summary>   Trim control text. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TrimControlText( object sender, System.EventArgs e )
        {
            Control c = sender as Control;
            c.Text = c.Text.Trim();
        }

        /// <summary>   Enumerate servers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void EnumerateServers()
        {
            // Perform the enumeration
            DataTable dataTable = null;
            try
            {
                // dataTable = System.Data.Sql.SqlDataSourceEnumerator.Instance.GetDataSources();
                dataTable = new DataTable {
                    Locale = System.Globalization.CultureInfo.InvariantCulture
                };
            }
            catch
            {
                dataTable = new DataTable {
                    Locale = System.Globalization.CultureInfo.InvariantCulture
                };
            }

            // Create the object array of server names (with instances appended)
            this._Servers = new object[dataTable.Rows.Count];
            for ( int i = 0; i < this._Servers.Length; i++ )
            {
                string name = dataTable.Rows[i]["ServerName"].ToString();
                string instance = dataTable.Rows[i]["InstanceName"].ToString();
                this._Servers[i] = instance.Length == 0 ? name : name + "\\" + instance;
            }

            // Sort the list
            Array.Sort( this._Servers );

            // Populate the server combo box items (must occur on the UI thread)
            if ( Thread.CurrentThread == this._UiThread )
            {
                this.PopulateServerComboBox();
            }
            else if ( this.IsHandleCreated )
            {
                _ = this.BeginInvoke( new ThreadStart( this.PopulateServerComboBox ) );
            }
        }

        /// <summary>   Populates the server combo box. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void PopulateServerComboBox()
        {
            if ( this.serverComboBox.Items.Count == 0 )
            {
                if ( this._Servers.Length > 0 )
                {
                    this.serverComboBox.Items.AddRange( this._Servers );
                }
                else
                {
                    _ = this.serverComboBox.Items.Add( String.Empty );
                }
            }
        }

        /// <summary>   Enumerate databases. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void EnumerateDatabases()
        {
            // Perform the enumeration
            DataTable dataTable = null;
            IDbConnection connection = null;
            IDataReader reader = null;
            try
            {
                // Get a basic connection
                connection = this.Properties.GetBasicConnection();

                // Create a command to check if the database is on SQL AZure.
                IDbCommand command = connection.CreateCommand();
                command.CommandText = "SELECT CASE WHEN SERVERPROPERTY(N'EDITION') = 'SQL Data Services' OR SERVERPROPERTY(N'EDITION') = 'SQL Azure' THEN 1 ELSE 0 END";

                // Open the connection
                connection.Open();

                // SQL AZure doesn't support HAS_DBACCESS at this moment.
                // Change the command text to get database names accordingly
                command.CommandText = ( Int32 ) (command.ExecuteScalar()) == 1
                    ? "SELECT name FROM master.dbo.sysdatabases ORDER BY name"
                    : "SELECT name FROM master.dbo.sysdatabases WHERE HAS_DBACCESS(name) = 1 ORDER BY name";

                // Execute the command
                reader = command.ExecuteReader();

                // Read into the data table
                dataTable = new DataTable {
                    Locale = System.Globalization.CultureInfo.CurrentCulture
                };
                dataTable.Load( reader );
            }
            catch
            {
                dataTable = new DataTable {
                    Locale = System.Globalization.CultureInfo.InvariantCulture
                };
            }
            finally
            {
                if ( reader != null )
                {
                    reader.Dispose();
                }
                if ( connection != null )
                {
                    connection.Dispose();
                }
            }

            // Create the object array of database names
            this._Databases = new object[dataTable.Rows.Count];
            for ( int i = 0; i < this._Databases.Length; i++ )
            {
                this._Databases[i] = dataTable.Rows[i]["name"];
            }

            // Populate the database combo box items (must occur on the UI thread)
            if ( Thread.CurrentThread == this._UiThread )
            {
                this.PopulateDatabaseComboBox();
            }
            else if ( this.IsHandleCreated )
            {
                _ = this.BeginInvoke( new ThreadStart( this.PopulateDatabaseComboBox ) );
            }
        }

        /// <summary>   Populates the database combo box. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void PopulateDatabaseComboBox()
        {
            if ( this.selectDatabaseComboBox.Items.Count == 0 )
            {
                if ( this._Databases.Length > 0 )
                {
                    this.selectDatabaseComboBox.Items.AddRange( this._Databases );
                }
                else
                {
                    _ = this.selectDatabaseComboBox.Items.Add( String.Empty );
                }
            }
            this._DatabaseEnumerationThread = null;
        }

        /// <summary>   Text without mnemonics. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="text"> The text. </param>
        /// <returns>   A string. </returns>
        private static string TextWithoutMnemonics( string text )
        {
            if ( text == null )
            {
                return null;
            }

            int index = text.IndexOf( '&' );
            if ( index == -1 )
            {
                return text;
            }

            System.Text.StringBuilder str = new( text.Substring( 0, index ) );
            for ( ; index < text.Length; ++index )
            {
                if ( text[index] == '&' )
                {
                    // Skip this & and copy the next character instead
                    index++;
                }
                if ( index < text.Length )
                {
                    _ = str.Append( text[index] );
                }
            }

            return str.ToString();
        }

        /// <summary>   Gets or sets the properties. </summary>
        /// <value> The properties. </value>
        private ControlProperties Properties { get; set; }

        /// <summary>   A control properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class ControlProperties
        {
            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="properties">   (Immutable) the properties. </param>
            public ControlProperties( IDataConnectionProperties properties )
            {
                this._Properties = properties;
            }

            /// <summary>   Gets the provider. </summary>
            /// <value> The provider. </value>
            public string Provider => this._Properties is OleDBSqlConnectionProperties ? this._Properties[nameof( this.Provider )] as string : null;

            /// <summary>   Gets or sets the name of the server. </summary>
            /// <value> The name of the server. </value>
            public string ServerName
            {
                get => this._Properties[this.ServerNameProperty] as string;
                set {
                    if ( value != null && value.Trim().Length > 0 )
                    {
                        this._Properties[this.ServerNameProperty] = value.Trim();
                    }
                    else
                    {
                        this._Properties.Reset( this.ServerNameProperty );
                    }
                }
            }

            /// <summary>   Gets a value indicating whether the user instance. </summary>
            /// <value> True if user instance, false if not. </value>
            public bool UserInstance => this._Properties is SqlConnectionProperties && ( bool ) this._Properties["User Instance"];

            /// <summary>
            /// Gets or sets a value indicating whether this object use windows authentication.
            /// </summary>
            /// <value> True if use windows authentication, false if not. </value>
            public bool UseWindowsAuthentication
            {
                get => this._Properties is SqlConnectionProperties
                        ? ( bool ) this._Properties["Integrated Security"]
                        : this._Properties is OleDBConnectionProperties
                            ? this._Properties.Contains( "Integrated Security" ) &&
                                this._Properties["Integrated Security"] is string &&
                                (this._Properties["Integrated Security"] as string).Equals( "SSPI", StringComparison.OrdinalIgnoreCase )
                            : this._Properties is OdbcConnectionProperties
                                    && this._Properties.Contains( "Trusted_Connection" ) &&
                                        this._Properties["Trusted_Connection"] is string &&
                                        (this._Properties["Trusted_Connection"] as string).Equals( "Yes", StringComparison.OrdinalIgnoreCase );
                set {
                    if ( this._Properties is SqlConnectionProperties )
                    {
                        if ( value )
                        {
                            this._Properties["Integrated Security"] = value;
                        }
                        else
                        {
                            this._Properties.Reset( "Integrated Security" );
                        }
                    }
                    if ( this._Properties is OleDBConnectionProperties )
                    {
                        if ( value )
                        {
                            this._Properties["Integrated Security"] = "SSPI";
                        }
                        else
                        {
                            this._Properties.Reset( "Integrated Security" );
                        }
                    }
                    if ( this._Properties is OdbcConnectionProperties )
                    {
                        if ( value )
                        {
                            this._Properties["Trusted_Connection"] = "Yes";
                        }
                        else
                        {
                            this._Properties.Remove( "Trusted_Connection" );
                        }
                    }
                }
            }

            /// <summary>   Gets or sets the name of the user. </summary>
            /// <value> The name of the user. </value>
            public string UserName
            {
                get => this._Properties[this.UserNameProperty] as string;
                set {
                    if ( value != null && value.Trim().Length > 0 )
                    {
                        this._Properties[this.UserNameProperty] = value.Trim();
                    }
                    else
                    {
                        this._Properties.Reset( this.UserNameProperty );
                    }
                }
            }

            /// <summary>   Gets or sets the password. </summary>
            /// <value> The password. </value>
            public string Password
            {
                get => this._Properties[this.PasswordProperty] as string;
                set {
                    if ( value != null && value.Length > 0 )
                    {
                        this._Properties[this.PasswordProperty] = value;
                    }
                    else
                    {
                        this._Properties.Reset( this.PasswordProperty );
                    }
                }
            }

            /// <summary>   Gets or sets a value indicating whether the password should be saveed. </summary>
            /// <value> True if save password, false if not. </value>
            public bool SavePassword
            {
                get => this._Properties is not OdbcConnectionProperties && ( bool ) this._Properties["Persist Security Info"];
                set {
                    Debug.Assert( !(this._Properties is OdbcConnectionProperties) );
                    if ( value )
                    {
                        this._Properties["Persist Security Info"] = value;
                    }
                    else
                    {
                        this._Properties.Reset( "Persist Security Info" );
                    }
                }
            }

            /// <summary>   Gets or sets the name of the database. </summary>
            /// <value> The name of the database. </value>
            public string DatabaseName
            {
                get => this._Properties[this.DatabaseNameProperty] as string;
                set {
                    if ( value != null && value.Trim().Length > 0 )
                    {
                        this._Properties[this.DatabaseNameProperty] = value.Trim();
                    }
                    else
                    {
                        this._Properties.Reset( this.DatabaseNameProperty );
                    }
                }
            }

            /// <summary>   Gets or sets the database file. </summary>
            /// <value> The database file. </value>
            public string DatabaseFile
            {
                get => this._Properties[this.DatabaseFileProperty] as string;
                set {
                    if ( value != null && value.Trim().Length > 0 )
                    {
                        this._Properties[this.DatabaseFileProperty] = value.Trim();
                    }
                    else
                    {
                        this._Properties.Reset( this.DatabaseFileProperty );
                    }
                }
            }

            /// <summary>   Gets or sets the name of the logical database. </summary>
            /// <value> The name of the logical database. </value>
            public string LogicalDatabaseName
            {
                get => this.DatabaseName;
                set => this.DatabaseName = value;
            }

            /// <summary>   Gets basic connection. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>   The basic connection. </returns>
            public IDbConnection GetBasicConnection()
            {
                IDbConnection connection = null;

                string connectionString = String.Empty;
                if ( this._Properties is SqlConnectionProperties || this._Properties is OleDBConnectionProperties )
                {
                    if ( this._Properties is OleDBConnectionProperties )
                    {
                        connectionString += "Provider=" + this._Properties[nameof( this.Provider )].ToString() + ";";
                    }
                    connectionString += "Data Source='" + this.ServerName.Replace( "'", "''" ) + "';";
                    if ( this.UserInstance )
                    {
                        connectionString += "User Instance=true;";
                    }
                    if ( this.UseWindowsAuthentication )
                    {
                        connectionString += "Integrated Security=" + this._Properties["Integrated Security"].ToString() + ";";
                    }
                    else
                    {
                        connectionString += "User ID='" + this.UserName.Replace( "'", "''" ) + "';";
                        connectionString += "Password='" + this.Password.Replace( "'", "''" ) + "';";
                    }
                    if ( this._Properties is SqlConnectionProperties )
                    {
                        connectionString += "Pooling=False;";
                    }
                }
                if ( this._Properties is OdbcConnectionProperties )
                {
                    connectionString += "DRIVER={SQL Server};";
                    connectionString += "SERVER={" + this.ServerName.Replace( "}", "}}" ) + "};";
                    if ( this.UseWindowsAuthentication )
                    {
                        connectionString += "Trusted_Connection=Yes;";
                    }
                    else
                    {
                        connectionString += "UID={" + this.UserName.Replace( "}", "}}" ) + "};";
                        connectionString += "PWD={" + this.Password.Replace( "}", "}}" ) + "};";
                    }
                }

                if ( this._Properties is SqlConnectionProperties )
                {
                    connection = new SqlConnection( connectionString );
                }
                if ( this._Properties is OleDBConnectionProperties )
                {
                    connection = new OleDbConnection( connectionString );
                }
                if ( this._Properties is OdbcConnectionProperties )
                {
                    connection = new OdbcConnection( connectionString );
                }

                return connection;
            }

            /// <summary>   Gets the server name property. </summary>
            /// <value> The server name property. </value>
            private string ServerNameProperty => (this._Properties is SqlConnectionProperties) ? "Data Source" :
                        (this._Properties is OleDBConnectionProperties) ? "Data Source" :
                        (this._Properties is OdbcConnectionProperties) ? "SERVER" : null;

            /// <summary>   Gets the user name property. </summary>
            /// <value> The user name property. </value>
            private string UserNameProperty => (this._Properties is SqlConnectionProperties) ? "User ID" :
                        (this._Properties is OleDBConnectionProperties) ? "User ID" :
                        (this._Properties is OdbcConnectionProperties) ? "UID" : null;

            /// <summary>   Gets the password property. </summary>
            /// <value> The password property. </value>
            private string PasswordProperty => (this._Properties is SqlConnectionProperties) ? "Password" :
                        (this._Properties is OleDBConnectionProperties) ? "Password" :
                        (this._Properties is OdbcConnectionProperties) ? "PWD" : null;

            /// <summary>   Gets the database name property. </summary>
            /// <value> The database name property. </value>
            private string DatabaseNameProperty => (this._Properties is SqlConnectionProperties) ? "Initial Catalog" :
                        (this._Properties is OleDBConnectionProperties) ? "Initial Catalog" :
                        (this._Properties is OdbcConnectionProperties) ? "DATABASE" : null;

            /// <summary>   Gets the database file property. </summary>
            /// <value> The database file property. </value>
            private string DatabaseFileProperty => (this._Properties is SqlConnectionProperties) ? "AttachDbFilename" :
                        (this._Properties is OleDBConnectionProperties) ? "Initial File Name" :
                        (this._Properties is OdbcConnectionProperties) ? "AttachDBFileName" : null;

            /// <summary>   (Immutable) the properties. </summary>
            private readonly IDataConnectionProperties _Properties;
        }

        /// <summary>   True to loading. </summary>
        private bool _Loading;
        /// <summary>   The servers. </summary>
        private object[] _Servers;
        /// <summary>   The databases. </summary>
        private object[] _Databases;
        /// <summary>   (Immutable) the thread. </summary>
        private readonly Thread _UiThread;
        /// <summary>   The server enumeration thread. </summary>
        private Thread _ServerEnumerationThread;
        /// <summary>   The database enumeration thread. </summary>
        private Thread _DatabaseEnumerationThread;
        /// <summary>   The current OLE database provider. </summary>
        private string _CurrentOleDBProvider;
        /// <summary>   True to current user instance setting. </summary>
        private bool _CurrentUserInstanceSetting;
    }
}
