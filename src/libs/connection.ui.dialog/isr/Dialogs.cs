
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI.Dialog
{
    /// <summary>   A dialogs. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public static class Dialogs
    {

        /// <summary>   Shows the SQL Server data source connection dialog. </summary>
        /// <remarks>   David, 2020-04-01. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, string ConnectionString) ShowSqlDataSourceConnectionDialog( string connectionString )
        {
            (bool Success, string ConnectionString) result = (true, string.Empty);
            Microsoft.Data.ConnectionUI.DataConnectionDialog dialog = new();
            Microsoft.Data.ConnectionUI.DataSource.AddStandardDataSources( dialog );
            dialog.SelectedDataSource = Microsoft.Data.ConnectionUI.DataSource.SqlDataSource;
            dialog.SelectedDataProvider = Microsoft.Data.ConnectionUI.DataProvider.SqlDataProvider;
            dialog.ConnectionString = connectionString;
            result.Success = DialogResult.OK == Microsoft.Data.ConnectionUI.DataConnectionDialog.Show( dialog );
            result.ConnectionString = dialog.ConnectionString;
            _ = dialog.DisplayConnectionString;
            return result;
        }
    }
}
