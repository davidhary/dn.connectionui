//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A SQL file connection user interface control. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public partial class SqlFileConnectionUIControl : UserControl, IDataConnectionUIControl
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public SqlFileConnectionUIControl()
        {
            this.InitializeComponent();
            this.RightToLeft = RightToLeft.Inherit;

            int requiredHeight = LayoutUtils.GetPreferredCheckBoxHeight( this.savePasswordCheckBox );
            if ( this.savePasswordCheckBox.Height < requiredHeight )
            {
                this.savePasswordCheckBox.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
                this.loginTableLayoutPanel.Height += this.loginTableLayoutPanel.Margin.Bottom;
                this.loginTableLayoutPanel.Margin = new Padding( this.loginTableLayoutPanel.Margin.Left, this.loginTableLayoutPanel.Margin.Top, this.loginTableLayoutPanel.Margin.Right, 0 );
            }
        }

        /// <summary>   Initializes this object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <param name="connectionProperties"> The connection properties. </param>
        public void Initialize( IDataConnectionProperties connectionProperties )
        {
            if ( !(connectionProperties is SqlFileConnectionProperties) )
            {
                throw new ArgumentException( Dialog.Resources.Strings.SqlFileConnectionUIControl_InvalidConnectionProperties );
            }

            this.Properties = connectionProperties;
        }

        /// <summary>   Loads the properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public void LoadProperties()
        {
            this._Loading = true;

            this.databaseFileTextBox.Text = this.Properties["AttachDbFilename"] as string;
            string myDocumentsDir = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments );
            if ( this.databaseFileTextBox.Text.StartsWith( myDocumentsDir, StringComparison.OrdinalIgnoreCase ) )
            {
                this.databaseFileTextBox.Text = this.databaseFileTextBox.Text.Substring( myDocumentsDir.Length + 1 );
            }
            if ( ( bool ) this.Properties["Integrated Security"] )
            {
                this.windowsAuthenticationRadioButton.Checked = true;
            }
            else
            {
                this.sqlAuthenticationRadioButton.Checked = true;
            }
            this.userNameTextBox.Text = this.Properties["User ID"] as string;
            this.passwordTextBox.Text = this.Properties["Password"] as string;
            this.savePasswordCheckBox.Checked = ( bool ) this.Properties["Persist Security Info"];

            this._Loading = false;
        }

        /// <summary>   Simulate RTL mirroring. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRightToLeftChanged( EventArgs e )
        {
            base.OnRightToLeftChanged( e );
            if ( this.ParentForm != null &&
                this.ParentForm.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.databaseFileLabel, this.databaseFileTableLayoutPanel );
                LayoutUtils.MirrorControl( this.windowsAuthenticationRadioButton );
                LayoutUtils.MirrorControl( this.sqlAuthenticationRadioButton );
                LayoutUtils.MirrorControl( this.loginTableLayoutPanel );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.loginTableLayoutPanel );
                LayoutUtils.UnmirrorControl( this.sqlAuthenticationRadioButton );
                LayoutUtils.UnmirrorControl( this.windowsAuthenticationRadioButton );
                LayoutUtils.UnmirrorControl( this.databaseFileLabel, this.databaseFileTableLayoutPanel );
            }
        }

        /// <summary>   Scales a control's location, size, padding and margin. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="factor">       The factor by which the height and width of the control will be
        ///                             scaled. </param>
        /// <param name="specified">    A <see cref="T:System.Windows.Forms.BoundsSpecified" /> value
        ///                             that specifies the bounds of the control to use when defining its
        ///                             size and position. </param>
        protected override void ScaleControl( SizeF factor, BoundsSpecified specified )
        {
            Size baseSize = this.Size;
            this.MinimumSize = Size.Empty;
            base.ScaleControl( factor, specified );
            this.MinimumSize = new Size(
                ( int ) Math.Round( ( float ) baseSize.Width * factor.Width ),
                ( int ) Math.Round( ( float ) baseSize.Height * factor.Height ) );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.ParentChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            if ( this.Parent == null )
            {
                this.OnFontChanged( e );
            }
        }

        /// <summary>   Sets database file. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetDatabaseFile( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["AttachDbFilename"] = (this.databaseFileTextBox.Text.Trim().Length > 0) ? this.databaseFileTextBox.Text.Trim() : null;
            }
        }

        /// <summary>   Updates the database file. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void UpdateDatabaseFile( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                string attachDbFilename = (this.databaseFileTextBox.Text.Trim().Length > 0) ? this.databaseFileTextBox.Text.Trim() : null;
                if ( attachDbFilename != null )
                {
                    if ( !attachDbFilename.EndsWith( ".mdf", StringComparison.OrdinalIgnoreCase ) )
                    {
                        attachDbFilename += ".mdf";
                    }
                    try
                    {
                        if ( !System.IO.Path.IsPathRooted( attachDbFilename ) )
                        {
                            // Simulate a default directory as My Documents by appending this to the front
                            attachDbFilename = System.IO.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ), attachDbFilename );
                        }
                    }
                    catch { }
                }
                this.Properties["AttachDbFilename"] = attachDbFilename;
            }
        }

        /// <summary>   Browses. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Browse( object sender, System.EventArgs e )
        {
            OpenFileDialog fileDialog = new() {
                Title = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileTitle,
                Multiselect = false,
                CheckFileExists = false,
                RestoreDirectory = true,
                Filter = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileFilter,
                DefaultExt = Dialog.Resources.Strings.SqlConnectionUIControl_BrowseFileDefaultExt,
                FileName = this.Properties["AttachDbFilename"] as string
            };
            if ( this.Container != null )
            {
                this.Container.Add( fileDialog );
            }
            try
            {
                DialogResult result = fileDialog.ShowDialog( this.ParentForm );
                if ( result == DialogResult.OK )
                {
                    this.databaseFileTextBox.Text = fileDialog.FileName.Trim();
                }
            }
            finally
            {
                if ( this.Container != null )
                {
                    this.Container.Remove( fileDialog );
                }
                fileDialog.Dispose();
            }
        }

        /// <summary>   Sets authentication option. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetAuthenticationOption( object sender, EventArgs e )
        {
            if ( this.windowsAuthenticationRadioButton.Checked )
            {
                if ( !this._Loading )
                {
                    this.Properties["Integrated Security"] = true;
                    this.Properties.Reset( "User ID" );
                    this.Properties.Reset( "Password" );
                    this.Properties.Reset( "Persist Security Info" );
                }
                this.loginTableLayoutPanel.Enabled = false;
            }
            else /* if (sqlAuthenticationRadioButton.Checked) */
            {
                if ( !this._Loading )
                {
                    this.Properties["Integrated Security"] = false;
                    this.SetUserName( sender, e );
                    this.SetPassword( sender, e );
                    this.SetSavePassword( sender, e );
                }
                this.loginTableLayoutPanel.Enabled = true;
            }
        }

        /// <summary>   Sets user name. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetUserName( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["User ID"] = (this.userNameTextBox.Text.Trim().Length > 0) ? this.userNameTextBox.Text.Trim() : null;
            }
        }

        /// <summary>   Sets a password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetPassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["Password"] = (this.passwordTextBox.Text.Length > 0) ? this.passwordTextBox.Text : null;
                this.passwordTextBox.Text = this.passwordTextBox.Text; // forces reselection of all text
            }
        }

        /// <summary>   Sets save password. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetSavePassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["Persist Security Info"] = this.savePasswordCheckBox.Checked;
            }
        }

        /// <summary>   Trim control text. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void TrimControlText( object sender, EventArgs e )
        {
            Control c = sender as Control;
            c.Text = c.Text.Trim();
        }

        /// <summary>   Gets or sets the properties. </summary>
        /// <value> The properties. </value>
        private IDataConnectionProperties Properties { get; set; }

        /// <summary>   True to loading. </summary>
        private bool _Loading;
    }
}
