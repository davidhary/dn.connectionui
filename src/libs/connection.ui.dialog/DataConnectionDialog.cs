//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Security;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms.Design;
using System.Security.Permissions;
using System.Runtime.InteropServices;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Dialog for setting the data connection. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public partial class DataConnectionDialog : Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public DataConnectionDialog()
        {
            this.InitializeComponent();
            this.dataSourceTextBox.Width = 0;

            // Make sure we handle a user preference change
            this.components.Add( new UserPreferenceChangedHandler( this ) );

            // Configure initial label values
            ComponentResourceManager resources = new( typeof( DataConnectionSourceDialog ) );
            this._ChooseDataSourceTitle = resources.GetString( "$this.Text" );
            this._ChooseDataSourceAcceptText = resources.GetString( "okButton.Text" );
            this._ChangeDataSourceTitle = Dialog.Resources.Strings.DataConnectionDialog_ChangeDataSourceTitle;

            this.DataSources = new DataSourceCollection( this );
        }

        /// <summary>   Shows. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dialog">   The dialog. </param>
        /// <returns>   A DialogResult. </returns>
        public static DialogResult Show( DataConnectionDialog dialog )
        {

            return Show( dialog, null );
        }

        /// <summary>   Shows. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="dialog">   The dialog. </param>
        /// <param name="owner">    The owner. </param>
        /// <returns>   A DialogResult. </returns>
        public static DialogResult Show( DataConnectionDialog dialog, IWin32Window owner )
        {
            if ( dialog == null )
            {
                throw new ArgumentNullException( nameof( dialog ) );
            }
            if ( dialog.DataSources.Count == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataSourcesAvailable );
            }
            foreach ( DataSource dataSource in dialog.DataSources )
            {
                if ( dataSource.Providers.Count == 0 )
                {
                    throw new InvalidOperationException( String.Format( Dialog.Resources.Strings.DataConnectionDialog_NoDataProvidersForDataSource + dataSource.DisplayName.Replace( "'", "''" ) ) );
                }
            }

            Application.ThreadException += new ThreadExceptionEventHandler( dialog.HandleDialogException );
            dialog._ShowingDialog = true;
            try
            {
                // If there is no selected data source or provider, show the data connection source dialog
                if ( dialog.SelectedDataSource == null || dialog.SelectedDataProvider == null )
                {
                    DataConnectionSourceDialog sourceDialog = new( dialog ) {
                        Title = dialog.ChooseDataSourceTitle,
                        HeaderLabel = dialog.ChooseDataSourceHeaderLabel
                    };
                    (sourceDialog.AcceptButton as Button).Text = dialog.ChooseDataSourceAcceptText;
                    if ( dialog.Container != null )
                    {
                        dialog.Container.Add( sourceDialog );
                    }
                    try
                    {
                        if ( owner == null )
                        {
                            sourceDialog.StartPosition = FormStartPosition.CenterScreen;
                        }
                        _ = sourceDialog.ShowDialog( owner );
                        if ( dialog.SelectedDataSource == null || dialog.SelectedDataProvider == null )
                        {
                            return DialogResult.Cancel;
                        }
                    }
                    finally
                    {
                        if ( dialog.Container != null )
                        {
                            dialog.Container.Remove( sourceDialog );
                        }
                        sourceDialog.Dispose();
                    }
                }
                else
                {
                    dialog.SaveSelection = false;
                }
                if ( owner == null )
                {
                    dialog.StartPosition = FormStartPosition.CenterScreen;
                }
                for (; ; )
                {
                    DialogResult result = dialog.ShowDialog( owner );
                    if ( result == DialogResult.Ignore )
                    {
                        DataConnectionSourceDialog sourceDialog = new( dialog ) {
                            Title = dialog.ChangeDataSourceTitle,
                            HeaderLabel = dialog.ChangeDataSourceHeaderLabel
                        };
                        if ( dialog.Container != null )
                        {
                            dialog.Container.Add( sourceDialog );
                        }
                        try
                        {
                            if ( owner == null )
                            {
                                sourceDialog.StartPosition = FormStartPosition.CenterScreen;
                            }
                            result = sourceDialog.ShowDialog( owner );
                        }
                        finally
                        {
                            if ( dialog.Container != null )
                            {
                                dialog.Container.Remove( sourceDialog );
                            }
                            sourceDialog.Dispose();
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
            }
            finally
            {
                dialog._ShowingDialog = false;
                Application.ThreadException -= new ThreadExceptionEventHandler( dialog.HandleDialogException );
            }
        }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title
        {
            get => this.Text;
            set => this.Text = value;
        }

        /// <summary>   Gets or sets the header label. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The header label. </value>
        public string HeaderLabel
        {
            get => (this._HeaderLabel != null) ? this._HeaderLabel.Text : String.Empty;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( this._HeaderLabel == null && (value == null || value.Length == 0) )
                {
                    return;
                }
                if ( this._HeaderLabel != null && value == this._HeaderLabel.Text )
                {
                    return;
                }
                if ( value != null && value.Length > 0 )
                {
                    if ( this._HeaderLabel == null )
                    {
                        this._HeaderLabel = new Label {
                            Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                            FlatStyle = FlatStyle.System,
                            Location = new Point( 12, 12 ),
                            Margin = new Padding( 3 ),
                            Name = "dataSourceLabel",
                            Width = this.dataSourceTableLayoutPanel.Width,
                            TabIndex = 100
                        };
                        this.Controls.Add( this._HeaderLabel );
                    }
                    this._HeaderLabel.Text = value;
                    this.MinimumSize = Size.Empty;
                    this._HeaderLabel.Height = LayoutUtils.GetPreferredLabelHeight( this._HeaderLabel );
                    int dy =
                        this._HeaderLabel.Bottom +
                        this._HeaderLabel.Margin.Bottom +
                        this.dataSourceLabel.Margin.Top -
                        this.dataSourceLabel.Top;
                    this.containerControl.Anchor &= ~AnchorStyles.Bottom;
                    this.Height += dy;
                    this.containerControl.Anchor |= AnchorStyles.Bottom;
                    this.containerControl.Top += dy;
                    this.dataSourceTableLayoutPanel.Top += dy;
                    this.dataSourceLabel.Top += dy;
                    this.MinimumSize = this.Size;
                }
                else
                {
                    if ( this._HeaderLabel != null )
                    {
                        int dy = this._HeaderLabel.Height;
                        try
                        {
                            this.Controls.Remove( this._HeaderLabel );
                        }
                        finally
                        {
                            this._HeaderLabel.Dispose();
                            this._HeaderLabel = null;
                        }
                        this.MinimumSize = Size.Empty;
                        this.dataSourceLabel.Top -= dy;
                        this.dataSourceTableLayoutPanel.Top -= dy;
                        this.containerControl.Top -= dy;
                        this.containerControl.Anchor &= ~AnchorStyles.Bottom;
                        this.Height -= dy;
                        this.containerControl.Anchor |= AnchorStyles.Bottom;
                        this.MinimumSize = this.Size;
                    }
                }
            }
        }

        /// <summary>   Gets or sets a value indicating whether the translate help button. </summary>
        /// <value> True if translate help button, false if not. </value>
        public bool TranslateHelpButton { get; set; } = true;

        /// <summary>   Gets or sets the choose data source title. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The choose data source title. </value>
        public string ChooseDataSourceTitle
        {
            get => this._ChooseDataSourceTitle;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceTitle )
                {
                    return;
                }
                this._ChooseDataSourceTitle = value;
            }
        }

        /// <summary>   Gets or sets the choose data source header label. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The choose data source header label. </value>
        public string ChooseDataSourceHeaderLabel
        {
            get => this._ChooseDataSourceHeaderLabel;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceHeaderLabel )
                {
                    return;
                }
                this._ChooseDataSourceHeaderLabel = value;
            }
        }

        /// <summary>   Gets or sets the choose data source accept text. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The choose data source accept text. </value>
        public string ChooseDataSourceAcceptText
        {
            get => this._ChooseDataSourceAcceptText;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceAcceptText )
                {
                    return;
                }
                this._ChooseDataSourceAcceptText = value;
            }
        }

        /// <summary>   Gets or sets the change data source title. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The change data source title. </value>
        public string ChangeDataSourceTitle
        {
            get => this._ChangeDataSourceTitle;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChangeDataSourceTitle )
                {
                    return;
                }
                this._ChangeDataSourceTitle = value;
            }
        }

        /// <summary>   Gets or sets the change data source header label. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The change data source header label. </value>
        public string ChangeDataSourceHeaderLabel
        {
            get => this._ChangeDataSourceHeaderLabel;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChangeDataSourceHeaderLabel )
                {
                    return;
                }
                this._ChangeDataSourceHeaderLabel = value;
            }
        }

        /// <summary>   Gets the data sources. </summary>
        /// <value> The data sources. </value>
        public ICollection<DataSource> DataSources { get; }

        /// <summary>   Gets the unspecified data source. </summary>
        /// <value> The unspecified data source. </value>
        public DataSource UnspecifiedDataSource { get; } = DataSource.CreateUnspecified();

        /// <summary>   Gets or sets the selected data source. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The selected data source. </value>
        public DataSource SelectedDataSource
        {
            get {
                if ( this.DataSources == null )
                {
                    return null;
                }
                switch ( this.DataSources.Count )
                {
                    case 0:
                        Debug.Assert( this._SelectedDataSource == null );
                        return null;
                    case 1:
                        // If there is only one data source, it must be selected
                        IEnumerator<DataSource> e = this.DataSources.GetEnumerator();
                        _ = e.MoveNext();
                        return e.Current;
                    default:
                        return this._SelectedDataSource;
                }
            }
            set {
                if ( this.SelectedDataSource != value )
                {
                    if ( this._ShowingDialog )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                    }
                    this.SetSelectedDataSource( value, false );
                }
            }
        }

        /// <summary>   Gets or sets the selected data provider. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The selected data provider. </value>
        public DataProvider SelectedDataProvider
        {
            get => this.GetSelectedDataProvider( this.SelectedDataSource );
            set {
                if ( this.SelectedDataProvider != value )
                {
                    if ( this.SelectedDataSource == null )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataSourceSelected );
                    }
                    this.SetSelectedDataProvider( this.SelectedDataSource, value );
                }
            }
        }

        /// <summary>   Gets selected data provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dataSource">   The data source. </param>
        /// <returns>   The selected data provider. </returns>
        public DataProvider GetSelectedDataProvider( DataSource dataSource )
        {
            if ( dataSource == null )
            {
                return null;
            }
            switch ( dataSource.Providers.Count )
            {
                case 0:
                    return null;
                case 1:
                    // If there is only one data provider, it must be selected
                    IEnumerator<DataProvider> e = dataSource.Providers.GetEnumerator();
                    _ = e.MoveNext();
                    return e.Current;
                default:
                    return (this._DataProviderSelections.ContainsKey( dataSource )) ? this._DataProviderSelections[dataSource] : dataSource.DefaultProvider;
            }
        }

        /// <summary>   Sets selected data provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="dataSource">   The data source. </param>
        /// <param name="dataProvider"> The data provider. </param>
        public void SetSelectedDataProvider( DataSource dataSource, DataProvider dataProvider )
        {
            if ( this.GetSelectedDataProvider( dataSource ) != dataProvider )
            {
                if ( dataSource == null )
                {
                    throw new ArgumentNullException( nameof( dataSource ) );
                }
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                this.SetSelectedDataProvider( dataSource, dataProvider, false );
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selection should be saveed.
        /// </summary>
        /// <value> True if save selection, false if not. </value>
        public bool SaveSelection { get; set; } = true;

        /// <summary>   Gets the display connection string. </summary>
        /// <value> The display connection string. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string DisplayConnectionString
        {
            get {
                string s = null;
                if ( this.ConnectionProperties != null )
                {
                    try
                    {
                        s = this.ConnectionProperties.ToDisplayString();
                    }
                    catch { }
                }
                return s ?? String.Empty;
            }
        }

        /// <summary>   Gets or sets the connection string. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The connection string. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string ConnectionString
        {
            get {
                string s = null;
                if ( this.ConnectionProperties != null )
                {
                    try
                    {
                        s = this.ConnectionProperties.ToString();
                    }
                    catch { }
                }
                return s ?? String.Empty;
            }
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( this.SelectedDataProvider == null )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataProviderSelected );
                }
                Debug.Assert( this.ConnectionProperties != null );
                if ( this.ConnectionProperties != null )
                {
                    this.ConnectionProperties.Parse( value );
                }
            }
        }

        /// <summary>   Gets or sets the accept button text. </summary>
        /// <value> The accept button text. </value>
        public string AcceptButtonText
        {
            get => this.acceptButton.Text;
            set => this.acceptButton.Text = value;
        }

        /// <summary>   Event queue for all listeners interested in VerifySettings events. </summary>
        public event EventHandler VerifySettings;

        /// <summary>   Event queue for all listeners interested in ContextHelpRequested events. </summary>
        public event EventHandler<ContextHelpEventArgs> ContextHelpRequested;

        /// <summary>   Event queue for all listeners interested in DialogException events. </summary>
        public event ThreadExceptionEventHandler DialogException;

        /// <summary>   Gets the connection user interface control. </summary>
        /// <value> The connection user interface control. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        internal UserControl ConnectionUIControl
        {
            get {
                if ( this.SelectedDataProvider == null )
                {
                    return null;
                }
                if ( !this._ConnectionUIControlTable.ContainsKey( this.SelectedDataSource ) )
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource] = new Dictionary<DataProvider, IDataConnectionUIControl>();
                }
                if ( !this._ConnectionUIControlTable[this.SelectedDataSource].ContainsKey( this.SelectedDataProvider ) )
                {
                    IDataConnectionUIControl uiControl = null;
                    UserControl control = null;
                    try
                    {
                        uiControl = this.SelectedDataSource == this.UnspecifiedDataSource
                            ? this.SelectedDataProvider.CreateConnectionUIControl()
                            : this.SelectedDataProvider.CreateConnectionUIControl( this.SelectedDataSource );
                        control = uiControl as UserControl;
                        if ( control == null )
                        {
                            if ( uiControl is IContainerControl ctControl )
                            {
                                control = ctControl.ActiveControl as UserControl;
                            }
                        }
                    }
                    catch { }
                    if ( uiControl == null || control == null )
                    {
                        uiControl = new PropertyGridUIControl();
                        control = uiControl as UserControl;
                    }
                    control.Location = Point.Empty;
                    control.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    control.AutoSize = false;
                    try
                    {
                        uiControl.Initialize( this.ConnectionProperties );
                    }
                    catch { }
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] = uiControl;
                    this.components.Add( control ); // so that it is disposed when the form is disposed
                }
                if ( this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] is not UserControl result )
                {
                    result = (this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] as IContainerControl).ActiveControl as UserControl;
                }
                return result;
            }
        }

        /// <summary>   Gets the connection properties. </summary>
        /// <value> The connection properties. </value>
        internal IDataConnectionProperties ConnectionProperties
        {
            get {
                if ( this.SelectedDataProvider == null )
                {
                    return null;
                }
                if ( !this._ConnectionPropertiesTable.ContainsKey( this.SelectedDataSource ) )
                {
                    this._ConnectionPropertiesTable[this.SelectedDataSource] = new Dictionary<DataProvider, IDataConnectionProperties>();
                }
                if ( !this._ConnectionPropertiesTable[this.SelectedDataSource].ContainsKey( this.SelectedDataProvider ) )
                {
                    IDataConnectionProperties properties = this.SelectedDataSource == this.UnspecifiedDataSource
                        ? this.SelectedDataProvider.CreateConnectionProperties()
                        : this.SelectedDataProvider.CreateConnectionProperties( this.SelectedDataSource );
                    if ( properties == null )
                    {
                        properties = new BasicConnectionProperties();
                    }
                    properties.PropertyChanged += new EventHandler( this.ConfigureAcceptButton );
                    this._ConnectionPropertiesTable[this.SelectedDataSource][this.SelectedDataProvider] = properties;
                }
                return this._ConnectionPropertiesTable[this.SelectedDataSource][this.SelectedDataProvider];
            }
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="NotSupportedException">    Thrown when the requested operation is not
        ///                                             supported. </exception>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            if ( !this._ShowingDialog )
            {
                throw new NotSupportedException( Dialog.Resources.Strings.DataConnectionDialog_ShowDialogNotSupported );
            }
            this.ConfigureDataSourceTextBox();
            this.ConfigureChangeDataSourceButton();
            this.ConfigureContainerControl();
            this.ConfigureAcceptButton( this, EventArgs.Empty );
            base.OnLoad( e );
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );

            // Set focus to the connection UI control (if any)
            if ( this.ConnectionUIControl != null )
            {
                _ = this.ConnectionUIControl.Focus();
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.FontChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );

            this.dataSourceTableLayoutPanel.Anchor &= ~AnchorStyles.Right;
            this.containerControl.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.advancedButton.Anchor |= AnchorStyles.Top | AnchorStyles.Left;
            this.advancedButton.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.separatorPanel.Anchor |= AnchorStyles.Top;
            this.separatorPanel.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.testConnectionButton.Anchor |= AnchorStyles.Top;
            this.testConnectionButton.Anchor &= ~AnchorStyles.Bottom;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Top | AnchorStyles.Left;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            Size properSize = new(
                this.containerControl.Right +
                this.containerControl.Margin.Right +
                this.Padding.Right,
                this.buttonsTableLayoutPanel.Bottom +
                this.buttonsTableLayoutPanel.Margin.Bottom +
                this.Padding.Bottom );
            properSize = this.SizeFromClientSize( properSize );
            Size dsize = this.Size - properSize;
            this.MinimumSize -= dsize;
            this.Size -= dsize;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Top & ~AnchorStyles.Left;
            this.testConnectionButton.Anchor |= AnchorStyles.Bottom;
            this.testConnectionButton.Anchor &= ~AnchorStyles.Top;
            this.separatorPanel.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.separatorPanel.Anchor &= ~AnchorStyles.Top;
            this.advancedButton.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.advancedButton.Anchor &= ~AnchorStyles.Top & ~AnchorStyles.Left;
            this.containerControl.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.dataSourceTableLayoutPanel.Anchor |= AnchorStyles.Right;
        }

        /// <summary>   Raises the verify settings event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnVerifySettings( EventArgs e )
        {
            if ( VerifySettings != null )
            {
                VerifySettings( this, e );
            }
        }

        /// <summary>   Raises the context help event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected internal virtual void OnContextHelpRequested( ContextHelpEventArgs e )
        {
            if ( ContextHelpRequested != null )
            {
                ContextHelpRequested( this, e );
            }
            if ( e.Handled == false )
            {
                this.ShowError( null, Dialog.Resources.Strings.DataConnectionDialog_NoHelpAvailable );
                e.Handled = true;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.HelpRequested" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hevent">   A <see cref="T:System.Windows.Forms.HelpEventArgs" /> that contains
        ///                         the event data. </param>
        protected override void OnHelpRequested( HelpEventArgs hevent )
        {
            // Get the active control
            Control activeControl = this;
            while ( activeControl is ContainerControl containrControl &&
                containrControl != this.ConnectionUIControl &&
                containrControl.ActiveControl != null )
            {
                activeControl = containrControl.ActiveControl;
            }

            // Figure out the context
            DataConnectionDialogContext context = DataConnectionDialogContext.Main;
            if ( activeControl == this.dataSourceTextBox )
            {
                context = DataConnectionDialogContext.MainDataSourceTextBox;
            }
            if ( activeControl == this.changeDataSourceButton )
            {
                context = DataConnectionDialogContext.MainChangeDataSourceButton;
            }
            if ( activeControl == this.ConnectionUIControl )
            {
                context = DataConnectionDialogContext.MainConnectionUIControl;
                if ( this.ConnectionUIControl is SqlConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainSqlConnectionUIControl;
                }
                if ( this.ConnectionUIControl is SqlFileConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainSqlFileConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OracleConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOracleConnectionUIControl;
                }
                if ( this.ConnectionUIControl is AccessConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainAccessConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OleDBConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOleDBConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OdbcConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOdbcConnectionUIControl;
                }
                if ( this.ConnectionUIControl is PropertyGridUIControl )
                {
                    context = DataConnectionDialogContext.MainGenericConnectionUIControl;
                }
            }
            if ( activeControl == this.advancedButton )
            {
                context = DataConnectionDialogContext.MainAdvancedButton;
            }
            if ( activeControl == this.testConnectionButton )
            {
                context = DataConnectionDialogContext.MainTestConnectionButton;
            }
            if ( activeControl == this.acceptButton )
            {
                context = DataConnectionDialogContext.MainAcceptButton;
            }
            if ( activeControl == this.cancelButton )
            {
                context = DataConnectionDialogContext.MainCancelButton;
            }

            // Call OnContextHelpRequested
            ContextHelpEventArgs e = new( context, hevent.MousePos );
            this.OnContextHelpRequested( e );
            hevent.Handled = e.Handled;
            if ( !e.Handled )
            {
                base.OnHelpRequested( hevent );
            }
        }

        /// <summary>   Raises the thread exception event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnDialogException( ThreadExceptionEventArgs e )
        {
            if ( DialogException != null )
            {
                DialogException( this, e );
            }
            else
            {
                this.ShowError( null, e.Exception );
            }
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that
        ///                     contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        protected override void OnFormClosing( FormClosingEventArgs e )
        {
            if ( this.DialogResult == DialogResult.OK )
            {
                try
                {
                    this.OnVerifySettings( EventArgs.Empty );
                }
                catch ( Exception ex )
                {
                    if ( ex is not ExternalException exex || exex.ErrorCode != NativeMethods.DB_E_CANCELED )
                    {
                        this.ShowError( null, ex );
                    }
                    e.Cancel = true;
                }
            }

            base.OnFormClosing( e );
        }

        /// <summary>   Processes Windows messages. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="m">    [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        ///                     process. </param>
        protected override void WndProc( ref Message m )
        {
            if ( this.TranslateHelpButton && HelpUtils.IsContextHelpMessage( ref m ) )
            {
                // Force the ? in the title bar to invoke the help topic
                HelpUtils.TranslateContextHelpMessage( this, ref m );
                base.DefWndProc( ref m ); // pass to the active control
                return;
            }
            base.WndProc( ref m );
        }

        /// <summary>   Sets selected data source internal. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The value. </param>
        internal void SetSelectedDataSourceInternal( DataSource value )
        {
            this.SetSelectedDataSource( value, false );
        }

        /// <summary>   Sets selected data provider internal. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dataSource">   The data source. </param>
        /// <param name="value">        The value. </param>
        internal void SetSelectedDataProviderInternal( DataSource dataSource, DataProvider value )
        {
            this.SetSelectedDataProvider( dataSource, value, false );
        }

        /// <summary>   Sets selected data source. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="value">                The value. </param>
        /// <param name="noSingleItemCheck">    True to no single item check. </param>
        private void SetSelectedDataSource( DataSource value, bool noSingleItemCheck )
        {
            if ( !noSingleItemCheck && this.DataSources.Count == 1 && this._SelectedDataSource != value )
            {
                IEnumerator<DataSource> e = this.DataSources.GetEnumerator();
                _ = e.MoveNext();
                if ( value != e.Current )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotChangeSingleDataSource );
                }
            }
            if ( this._SelectedDataSource != value )
            {
                if ( value != null )
                {
                    if ( !this.DataSources.Contains( value ) )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_DataSourceNotFound );
                    }
                    this._SelectedDataSource = value;
                    switch ( this._SelectedDataSource.Providers.Count )
                    {
                        case 0:
                            this.SetSelectedDataProvider( this._SelectedDataSource, null, noSingleItemCheck );
                            break;
                        case 1:
                            IEnumerator<DataProvider> e = this._SelectedDataSource.Providers.GetEnumerator();
                            _ = e.MoveNext();
                            this.SetSelectedDataProvider( this._SelectedDataSource, e.Current, true );
                            break;
                        default:
                            DataProvider defaultProvider = this._SelectedDataSource.DefaultProvider;
                            if ( this._DataProviderSelections.ContainsKey( this._SelectedDataSource ) )
                            {
                                defaultProvider = this._DataProviderSelections[this._SelectedDataSource];
                            }
                            this.SetSelectedDataProvider( this._SelectedDataSource, defaultProvider, noSingleItemCheck );
                            break;
                    }
                }
                else
                {
                    this._SelectedDataSource = null;
                }

                if ( this._ShowingDialog )
                {
                    this.ConfigureDataSourceTextBox();
                }
            }
        }

        /// <summary>   Sets selected data provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="dataSource">           The data source. </param>
        /// <param name="value">                The value. </param>
        /// <param name="noSingleItemCheck">    True to no single item check. </param>
        private void SetSelectedDataProvider( DataSource dataSource, DataProvider value, bool noSingleItemCheck )
        {
            Debug.Assert( dataSource != null );
            if ( !noSingleItemCheck && dataSource.Providers.Count == 1 &&
                ((this._DataProviderSelections.ContainsKey( dataSource ) && this._DataProviderSelections[dataSource] != value) ||
                (!this._DataProviderSelections.ContainsKey( dataSource ) && value != null)) )
            {
                IEnumerator<DataProvider> e = dataSource.Providers.GetEnumerator();
                _ = e.MoveNext();
                if ( value != e.Current )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotChangeSingleDataProvider );
                }
            }
            if ( (this._DataProviderSelections.ContainsKey( dataSource ) && this._DataProviderSelections[dataSource] != value) ||
                (!this._DataProviderSelections.ContainsKey( dataSource ) && value != null) )
            {
                if ( value != null )
                {
                    if ( !dataSource.Providers.Contains( value ) )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_DataSourceNoAssociation );
                    }
                    this._DataProviderSelections[dataSource] = value;
                }
                else if ( this._DataProviderSelections.ContainsKey( dataSource ) )
                {
                    _ = this._DataProviderSelections.Remove( dataSource );
                }

                if ( this._ShowingDialog )
                {
                    this.ConfigureContainerControl();
                }
            }
        }

        /// <summary>   Configure data source text box. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void ConfigureDataSourceTextBox()
        {
            if ( this.SelectedDataSource != null )
            {
                if ( this.SelectedDataSource == this.UnspecifiedDataSource )
                {
                    this.dataSourceTextBox.Text = this.SelectedDataProvider?.DisplayName;
                    this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
                }
                else
                {
                    this.dataSourceTextBox.Text = this.SelectedDataSource.DisplayName;
                    if ( this.SelectedDataProvider != null )
                    {
                        if ( this.SelectedDataProvider.ShortDisplayName != null )
                        {
                            this.dataSourceTextBox.Text = String.Format( Dialog.Resources.Strings.DataConnectionDialog_DataSourceWithShortProvider, this.dataSourceTextBox.Text, this.SelectedDataProvider.ShortDisplayName );
                        }
                        this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, this.SelectedDataProvider.DisplayName );
                    }
                    else
                    {
                        this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
                    }
                }
            }
            else
            {
                this.dataSourceTextBox.Text = null;
                this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
            }
            this.dataSourceTextBox.Select( 0, 0 );
        }

        /// <summary>   Configure change data source button. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void ConfigureChangeDataSourceButton()
        {
            this.changeDataSourceButton.Enabled = (this.DataSources.Count > 1 || this.SelectedDataSource.Providers.Count > 1);
        }

        /// <summary>   Change data source. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ChangeDataSource( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        /// <summary>   Configure container control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ConfigureContainerControl()
        {
            if ( this.containerControl.Controls.Count == 0 )
            {
                this._InitialContainerControlSize = this.containerControl.Size;
            }
            if ( (this.containerControl.Controls.Count == 0 && this.ConnectionUIControl != null) ||
                (this.containerControl.Controls.Count > 0 && this.ConnectionUIControl != this.containerControl.Controls[0]) )
            {
                this.containerControl.Controls.Clear();
                if ( this.ConnectionUIControl != null && this.ConnectionUIControl.PreferredSize.Width > 0 && this.ConnectionUIControl.PreferredSize.Height > 0 )
                {
                    // Add it to the container control
                    this.containerControl.Controls.Add( this.ConnectionUIControl );

                    // Size dialog appropriately
                    this.MinimumSize = Size.Empty;
                    Size currentSize = this.containerControl.Size;
                    this.containerControl.Size = this._InitialContainerControlSize;
                    Size preferredSize = this.ConnectionUIControl.PreferredSize;
                    this.containerControl.Size = currentSize;
                    int minimumWidth =
                        this._InitialContainerControlSize.Width - (this.Width - this.ClientSize.Width) -
                        this.Padding.Left -
                        this.containerControl.Margin.Left -
                        this.containerControl.Margin.Right -
                        this.Padding.Right;
                    minimumWidth = Math.Max( minimumWidth,
                        this.testConnectionButton.Width +
                        this.testConnectionButton.Margin.Right +
                        this.buttonsTableLayoutPanel.Margin.Left +
                        this.buttonsTableLayoutPanel.Width +
                        this.buttonsTableLayoutPanel.Margin.Right );
                    preferredSize.Width = Math.Max( preferredSize.Width, minimumWidth );
                    this.Size += preferredSize - this.containerControl.Size;
                    if ( this.containerControl.Bottom == this.advancedButton.Top )
                    {
                        this.containerControl.Margin = new Padding(
                                this.containerControl.Margin.Left,
                                this.dataSourceTableLayoutPanel.Margin.Bottom,
                                this.containerControl.Margin.Right,
                                this.advancedButton.Margin.Top );
                        this.Height += this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.containerControl.Height -= this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                    }
                    Size maximumSize =
                        SystemInformation.PrimaryMonitorMaximizedWindowSize -
                        SystemInformation.FrameBorderSize -
                        SystemInformation.FrameBorderSize;
                    if ( this.Width > maximumSize.Width )
                    {
                        this.Width = maximumSize.Width;
                        if ( this.Height + SystemInformation.HorizontalScrollBarHeight <= maximumSize.Height )
                        {
                            this.Height += SystemInformation.HorizontalScrollBarHeight;
                        }
                    }
                    if ( this.Height > maximumSize.Height )
                    {
                        if ( this.Width + SystemInformation.VerticalScrollBarWidth <= maximumSize.Width )
                        {
                            this.Width += SystemInformation.VerticalScrollBarWidth;
                        }
                        this.Height = maximumSize.Height;
                    }
                    this.MinimumSize = this.Size;

                    // The advanced button is only enabled for actual UI controls
                    this.advancedButton.Enabled = !(this.ConnectionUIControl is PropertyGridUIControl);
                }
                else
                {
                    // Size dialog appropriately
                    this.MinimumSize = Size.Empty;
                    if ( this.containerControl.Bottom != this.advancedButton.Top )
                    {
                        this.containerControl.Height += this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.Height -= this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.containerControl.Margin = new Padding(
                                this.containerControl.Margin.Left,
                                0,
                                this.containerControl.Margin.Right,
                                0 );
                    }
                    this.Size -= this.containerControl.Size - new Size( 300, 0 );
                    this.MinimumSize = this.Size;

                    // The advanced button is always enabled for no UI control
                    this.advancedButton.Enabled = true;
                }
            }
            if ( this.ConnectionUIControl != null )
            {
                // Load properties into the connection UI control
                try
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider].LoadProperties();
                }
                catch { }
            }
        }
        /// <summary>   Initial size of the container control. </summary>
        private Size _InitialContainerControlSize;

        /// <summary>   Sets connection user interface control dock style. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetConnectionUIControlDockStyle( object sender, EventArgs e )
        {
            if ( this.containerControl.Controls.Count > 0 )
            {
                DockStyle dockStyle = DockStyle.None;
                Size containerControlSize = this.containerControl.Size;
                Size connectionUIControlMinimumSize = this.containerControl.Controls[0].MinimumSize;
                if ( containerControlSize.Width >= connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height >= connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Fill;
                }
                if ( containerControlSize.Width - SystemInformation.VerticalScrollBarWidth >= connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height < connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Top;
                }
                if ( containerControlSize.Width < connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height - SystemInformation.HorizontalScrollBarHeight >= connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Left;
                }
                this.containerControl.Controls[0].Dock = dockStyle;
            }
        }

        /// <summary>   Shows the advanced. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ShowAdvanced( object sender, EventArgs e )
        {
            DataConnectionAdvancedDialog advancedDialog = new( this.ConnectionProperties, this );
            DialogResult dialogResult = DialogResult.None;
            try
            {
                if ( this.Container != null )
                {
                    this.Container.Add( advancedDialog );
                }
                dialogResult = advancedDialog.ShowDialog( this );
            }
            finally
            {
                if ( this.Container != null )
                {
                    this.Container.Remove( advancedDialog );
                }
                advancedDialog.Dispose();
            }
            if ( dialogResult == DialogResult.OK && this.ConnectionUIControl != null )
            {
                try
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider].LoadProperties();
                }
                catch { }
                this.ConfigureAcceptButton( this, EventArgs.Empty );
            }
        }

        /// <summary>   Tests connection. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void TestConnection( object sender, EventArgs e )
        {
            Cursor currentCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.ConnectionProperties.Test();
            }
            catch ( Exception ex )
            {
                Cursor.Current = currentCursor;
                this.ShowError( Dialog.Resources.Strings.DataConnectionDialog_TestResults, ex );
                return;
            }
            Cursor.Current = currentCursor;
            this.ShowMessage( Dialog.Resources.Strings.DataConnectionDialog_TestResults, Dialog.Resources.Strings.DataConnectionDialog_TestConnectionSucceeded );
        }

        /// <summary>   Configure accept button. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ConfigureAcceptButton( object sender, EventArgs e )
        {
            try
            {
                this.acceptButton.Enabled = (this.ConnectionProperties != null) && this.ConnectionProperties.IsComplete;
            }
            catch
            {
                this.acceptButton.Enabled = true;
            }
        }

        /// <summary>   Handles the accept. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void HandleAccept( object sender, EventArgs e )
        {
            _ = this.acceptButton.Focus(); // ensures connection properties are up to date
        }

        /// <summary>   Paints the separator. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Paint event information. </param>
        private void PaintSeparator( object sender, PaintEventArgs e )
        {
            Graphics graphics = e.Graphics;
            Pen dark = new( ControlPaint.Dark( this.BackColor, 0f ) );
            Pen light = new( ControlPaint.Light( this.BackColor, 1f ) );
            int width = this.separatorPanel.Width;

            graphics.DrawLine( dark, 0, 0, width, 0 );
            graphics.DrawLine( light, 0, 1, width, 1 );
        }

        /// <summary>   Handles the dialog exception. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Thread exception event information. </param>
        private void HandleDialogException( object sender, ThreadExceptionEventArgs e )
        {
            this.OnDialogException( e );
        }

        /// <summary>   Shows the message. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="title">    The title. </param>
        /// <param name="message">  The message. </param>
        private void ShowMessage( string title, string message )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowMessage( message );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, message, MessageBoxIcon.Information );
            }
        }

        /// <summary>   Shows the error. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="title">    The title. </param>
        /// <param name="ex">       The exception. </param>
        private void ShowError( string title, Exception ex )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowError( ex );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, ex.Message, MessageBoxIcon.Exclamation );
            }
        }

        /// <summary>   Shows the error. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="title">    The title. </param>
        /// <param name="message">  The message. </param>
        private void ShowError( string title, string message )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowError( message );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, message, MessageBoxIcon.Exclamation );
            }
        }

        /// <summary>   Collection of data sources. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class DataSourceCollection : ICollection<DataSource>
        {
            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="dialog">   (Immutable) the dialog. </param>
            public DataSourceCollection( DataConnectionDialog dialog )
            {
                Debug.Assert( dialog != null );

                this._Dialog = dialog;
            }

            /// <summary>
            /// Gets the number of elements contained in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <value>
            /// The number of elements contained in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </value>
            public int Count => this._List.Count;

            /// <summary>
            /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
            /// is read-only.
            /// </summary>
            /// <value>
            /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
            /// read-only; otherwise, <see langword="false" />.
            /// </value>
            public bool IsReadOnly => this._Dialog._ShowingDialog;

            /// <summary>
            /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
            ///                                                 are null. </exception>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <param name="item"> The object to add to the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            public void Add( DataSource item )
            {
                if ( item == null )
                {
                    throw new ArgumentNullException( nameof( item ) );
                }
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( !this._List.Contains( item ) )
                {
                    this._List.Add( item );
                }
            }

            /// <summary>
            /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
            /// specific value.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="item"> The object to locate in the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            /// <returns>
            /// <see langword="true" /> if <paramref name="item" /> is found in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
            /// <see langword="false" />.
            /// </returns>
            public bool Contains( DataSource item )
            {
                return this._List.Contains( item );
            }

            /// <summary>
            /// Removes the first occurrence of a specific object from the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <param name="item"> The object to remove from the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            /// <returns>
            /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
            /// <see langword="false" />. This method also returns <see langword="false" /> if
            /// <paramref name="item" /> is not found in the original
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </returns>
            public bool Remove( DataSource item )
            {
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                bool result = this._List.Remove( item );
                if ( item == this._Dialog.SelectedDataSource )
                {
                    this._Dialog.SetSelectedDataSource( null, true );
                }
                return result;
            }

            /// <summary>
            /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            public void Clear()
            {
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                this._List.Clear();
                this._Dialog.SetSelectedDataSource( null, true );
            }

            /// <summary>
            /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
            /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
            ///                             destination of the elements copied from
            ///                             <see cref="T:System.Collections.Generic.ICollection`1" />. The
            ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
            /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
            ///                             copying begins. </param>
            public void CopyTo( DataSource[] array, int arrayIndex )
            {
                this._List.CopyTo( array, arrayIndex );
            }

            /// <summary>   Returns an enumerator that iterates through the collection. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
            public IEnumerator<DataSource> GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            /// <summary>   Returns an enumerator that iterates through a collection. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through
            /// the collection.
            /// </returns>
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            /// <summary>   (Immutable) the list. </summary>
            private readonly List<DataSource> _List = new();
            /// <summary>   (Immutable) the dialog. </summary>
            private readonly DataConnectionDialog _Dialog;
        }

        /// <summary>   A property grid user interface control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class PropertyGridUIControl : UserControl, IDataConnectionUIControl
        {
            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public PropertyGridUIControl()
            {
                this._PropertyGrid = new DataConnectionAdvancedDialog.SpecializedPropertyGrid();
                this.SuspendLayout();
                // 
                // propertyGrid
                // 
                this._PropertyGrid.CommandsVisibleIfAvailable = true;
                this._PropertyGrid.Dock = DockStyle.Fill;
                this._PropertyGrid.Location = Point.Empty;
                this._PropertyGrid.Margin = new Padding( 0 );
                this._PropertyGrid.Name = "propertyGrid";
                this._PropertyGrid.TabIndex = 0;
                // 
                // DataConnectionDialog
                // 
                this.Controls.Add( this._PropertyGrid );
                this.Name = "PropertyGridUIControl";
                this.ResumeLayout( false );
                this.PerformLayout();
            }

            /// <summary>   Initializes this object. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="dataConnectionProperties"> The connection properties. </param>
            public void Initialize( IDataConnectionProperties dataConnectionProperties )
            {
                this._ConnectionProperties = dataConnectionProperties;
            }

            /// <summary>   Loads the properties. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public void LoadProperties()
            {
                this._PropertyGrid.SelectedObject = this._ConnectionProperties;
            }

            /// <summary>
            /// Retrieves the size of a rectangular area into which a control can be fitted.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="proposedSize"> The custom-sized area for a control. </param>
            /// <returns>
            /// An ordered pair of type <see cref="T:System.Drawing.Size" /> representing the width and
            /// height of a rectangle.
            /// </returns>
            public override Size GetPreferredSize( Size proposedSize )
            {
                return this._PropertyGrid.GetPreferredSize( proposedSize );
            }

            /// <summary>   The connection properties. </summary>
            private IDataConnectionProperties _ConnectionProperties;
            /// <summary>   (Immutable) the property grid. </summary>
            private readonly DataConnectionAdvancedDialog.SpecializedPropertyGrid _PropertyGrid;
        }

        /// <summary>   A basic connection properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class BasicConnectionProperties : IDataConnectionProperties
        {
            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public BasicConnectionProperties()
            {
            }

            /// <summary>   Resets the given propertyName. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public void Reset()
            {
                this._S = String.Empty;
            }

            /// <summary>   Parses. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="s">    The string. </param>
            public void Parse( string s )
            {
                this._S = s;
                if ( PropertyChanged != null )
                {
                    PropertyChanged( this, EventArgs.Empty );
                }
            }

            /// <summary>   Gets a value indicating whether this object is extensible. </summary>
            /// <value> True if this object is extensible, false if not. </value>
            [Browsable( false )]
            public bool IsExtensible => false;

            /// <summary>   Adds propertyName. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="propertyName"> Name of the property. </param>
            public void Add( string propertyName )
            {
                throw new NotImplementedException();
            }

            /// <summary>   Query if this object contains the given propertyName. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="propertyName"> Name of the property. </param>
            /// <returns>   True if the object is in this collection, false if not. </returns>
            public bool Contains( string propertyName )
            {
                return (propertyName == "ConnectionString");
            }

            /// <summary>
            /// Indexer to get or set items within this collection using array index syntax.
            /// </summary>
            /// <param name="propertyName"> Name of the property. </param>
            /// <returns>   The indexed item. </returns>
            public object this[string propertyName]
            {
                get => propertyName == "ConnectionString" ? this.ConnectionString : null;
                set {
                    if ( propertyName == "ConnectionString" )
                    {
                        this.ConnectionString = value as string;
                    }
                }
            }

            /// <summary>   Removes the given propertyName. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="propertyName"> Name of the property. </param>
            public void Remove( string propertyName )
            {
                throw new NotImplementedException();
            }

            /// <summary>   Event queue for all listeners interested in PropertyChanged events. </summary>
            public event EventHandler PropertyChanged;

            /// <summary>   Resets the given propertyName. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="propertyName"> Name of the property. </param>
            public void Reset( string propertyName )
            {
                Debug.Assert( propertyName == "ConnectionString" );
                this._S = String.Empty;
            }

            /// <summary>   Gets a value indicating whether this object is complete. </summary>
            /// <value> True if this object is complete, false if not. </value>
            [Browsable( false )]
            public bool IsComplete => true;

            /// <summary>   Gets or sets the connection string. </summary>
            /// <value> The connection string. </value>
            public string ConnectionString
            {
                get => this.ToFullString();
                set => this.Parse( value );
            }

            /// <summary>   Tests this object. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public void Test()
            {
            }

            /// <summary>   Converts this object to a full string. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>   This object as a string. </returns>
            public string ToFullString()
            {
                return this._S;
            }

            /// <summary>   Converts this object to a display string. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>   This object as a string. </returns>
            public string ToDisplayString()
            {
                return this._S;
            }

            /// <summary>   The s. </summary>
            private string _S;
        }

        /// <summary>   True to showing dialog. </summary>
        private bool _ShowingDialog;
        /// <summary>   The header label. </summary>
        private Label _HeaderLabel;
        /// <summary>   The choose data source title. </summary>
        private string _ChooseDataSourceTitle;
        /// <summary>   The choose data source header label. </summary>
        private string _ChooseDataSourceHeaderLabel = String.Empty;
        /// <summary>   The choose data source accept text. </summary>
        private string _ChooseDataSourceAcceptText;
        /// <summary>   The change data source title. </summary>
        private string _ChangeDataSourceTitle;
        /// <summary>   The change data source header label. </summary>
        private string _ChangeDataSourceHeaderLabel = String.Empty;
        /// <summary>   The selected data source. </summary>
        private DataSource _SelectedDataSource;
        /// <summary>   (Immutable) the data provider selections. </summary>
        private readonly IDictionary<DataSource, DataProvider> _DataProviderSelections = new Dictionary<DataSource, DataProvider>();
        /// <summary>   (Immutable) the connection user interface control table. </summary>
        private readonly IDictionary<DataSource, IDictionary<DataProvider, IDataConnectionUIControl>> _ConnectionUIControlTable = new Dictionary<DataSource, IDictionary<DataProvider, IDataConnectionUIControl>>();
        /// <summary>   (Immutable) the connection properties table. </summary>
        private readonly IDictionary<DataSource, IDictionary<DataProvider, IDataConnectionProperties>> _ConnectionPropertiesTable = new Dictionary<DataSource, IDictionary<DataProvider, IDataConnectionProperties>>();
    }
}
