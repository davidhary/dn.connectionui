//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Dialog for setting the data connection source. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal partial class DataConnectionSourceDialog : Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public DataConnectionSourceDialog()
        {
            this.InitializeComponent();

            // Make sure we handle a user preference change
            if ( this.components == null )
            {
                this.components = new System.ComponentModel.Container();
            }
            this.components.Add( new UserPreferenceChangedHandler( this ) );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="mainDialog">   The main dialog. </param>
        public DataConnectionSourceDialog( DataConnectionDialog mainDialog )
            : this()
        {
            Debug.Assert( mainDialog != null );

            this._MainDialog = mainDialog;
        }

        /// <summary>   Gets or sets the title. </summary>
        /// <value> The title. </value>
        public string Title
        {
            get => this.Text;
            set => this.Text = value;
        }

        /// <summary>   Gets or sets the header label. </summary>
        /// <value> The header label. </value>
        public string HeaderLabel
        {
            get => (this._HeaderLabel != null) ? this._HeaderLabel.Text : String.Empty;
            set {
                if ( this._HeaderLabel == null && (value == null || value.Length == 0) )
                {
                    return;
                }
                if ( this._HeaderLabel != null && value == this._HeaderLabel.Text )
                {
                    return;
                }
                if ( value != null )
                {
                    if ( this._HeaderLabel == null )
                    {
                        this._HeaderLabel = new Label {
                            Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                            FlatStyle = FlatStyle.System,
                            Location = new Point( 12, 12 ),
                            Margin = new Padding( 3 ),
                            Name = "dataSourceLabel",
                            Width = this.mainTableLayoutPanel.Width,
                            TabIndex = 100
                        };
                        this.Controls.Add( this._HeaderLabel );
                    }
                    this._HeaderLabel.Text = value;
                    this.MinimumSize = Size.Empty;
                    this._HeaderLabel.Height = LayoutUtils.GetPreferredLabelHeight( this._HeaderLabel );
                    int dy =
                        this._HeaderLabel.Bottom +
                        this._HeaderLabel.Margin.Bottom +
                        this.mainTableLayoutPanel.Margin.Top -
                        this.mainTableLayoutPanel.Top;
                    this.mainTableLayoutPanel.Anchor &= ~AnchorStyles.Bottom;
                    this.Height += dy;
                    this.mainTableLayoutPanel.Anchor |= AnchorStyles.Bottom;
                    this.mainTableLayoutPanel.Top += dy;
                    this.MinimumSize = this.Size;
                }
                else
                {
                    if ( this._HeaderLabel != null )
                    {
                        int dy = this._HeaderLabel.Height;
                        try
                        {
                            this.Controls.Remove( this._HeaderLabel );
                        }
                        finally
                        {
                            this._HeaderLabel.Dispose();
                            this._HeaderLabel = null;
                        }
                        this.MinimumSize = Size.Empty;
                        this.mainTableLayoutPanel.Top -= dy;
                        this.mainTableLayoutPanel.Anchor &= ~AnchorStyles.Bottom;
                        this.Height -= dy;
                        this.mainTableLayoutPanel.Anchor |= AnchorStyles.Bottom;
                        this.MinimumSize = this.Size;
                    }
                }
            }
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            // If a main dialog was associated with this dialog, get its data sources
            if ( this._MainDialog != null )
            {
                foreach ( DataSource dataSource in this._MainDialog.DataSources )
                {
                    if ( dataSource == this._MainDialog.UnspecifiedDataSource )
                    {
                        continue;
                    }
                    _ = this.dataSourceListBox.Items.Add( dataSource );
                }
                if ( this._MainDialog.DataSources.Contains( this._MainDialog.UnspecifiedDataSource ) )
                {
                    // We want to put the unspecified data source at the end of the list
                    this.dataSourceListBox.Sorted = false;
                    _ = this.dataSourceListBox.Items.Add( this._MainDialog.UnspecifiedDataSource );
                }

                // Figure out the correct width for the data source list box and size dialog
                int dataSourceListBoxWidth = this.dataSourceListBox.Width - (this.dataSourceListBox.Width - this.dataSourceListBox.ClientSize.Width);
                foreach ( object item in this.dataSourceListBox.Items )
                {
                    Size size = TextRenderer.MeasureText( (item as DataSource).DisplayName, this.dataSourceListBox.Font );
                    size.Width += 3; // otherwise text is crammed up against right edge
                    dataSourceListBoxWidth = Math.Max( dataSourceListBoxWidth, size.Width );
                }
                dataSourceListBoxWidth += (this.dataSourceListBox.Width - this.dataSourceListBox.ClientSize.Width);
                dataSourceListBoxWidth = Math.Max( dataSourceListBoxWidth, this.dataSourceListBox.MinimumSize.Width );
                int dx = dataSourceListBoxWidth - this.dataSourceListBox.Size.Width;
                this.Width += dx * 2; // * 2 because the description group box resizes as well
                this.MinimumSize = this.Size;

                if ( this._MainDialog.SelectedDataSource != null )
                {
                    this.dataSourceListBox.SelectedItem = this._MainDialog.SelectedDataSource;
                    if ( this._MainDialog.SelectedDataProvider != null )
                    {
                        this.dataProviderComboBox.SelectedItem = this._MainDialog.SelectedDataProvider;
                    }
                }

                // Configure the initial data provider selections
                foreach ( DataSource dataSource in this.dataSourceListBox.Items )
                {
                    DataProvider selectedProvider = this._MainDialog.GetSelectedDataProvider( dataSource );
                    if ( selectedProvider != null )
                    {
                        this._ProviderSelections[dataSource] = selectedProvider;
                    }
                }
            }

            // Set the save selection check box
            this.saveSelectionCheckBox.Checked = this._MainDialog.SaveSelection;

            this.SetOkButtonStatus();

            base.OnLoad( e );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.FontChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );

            this.dataProviderComboBox.Top =
                this.leftPanel.Height -
                this.leftPanel.Padding.Bottom -
                this.dataProviderComboBox.Margin.Bottom -
                this.dataProviderComboBox.Height;
            this.dataProviderLabel.Top =
                this.dataProviderComboBox.Top -
                this.dataProviderComboBox.Margin.Top -
                this.dataProviderLabel.Margin.Bottom -
                this.dataProviderLabel.Height;

            int dx =
                (this.saveSelectionCheckBox.Right + this.saveSelectionCheckBox.Margin.Right) -
                (this.buttonsTableLayoutPanel.Left - this.buttonsTableLayoutPanel.Margin.Left);
            if ( dx > 0 )
            {
                this.Width += dx;
                this.MinimumSize = new Size( this.MinimumSize.Width + dx, this.MinimumSize.Height );
            }
            this.mainTableLayoutPanel.Anchor &= ~AnchorStyles.Bottom;
            this.saveSelectionCheckBox.Anchor &= ~AnchorStyles.Bottom;
            this.saveSelectionCheckBox.Anchor |= AnchorStyles.Top;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Bottom;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Top;
            int height =
                this.buttonsTableLayoutPanel.Top +
                this.buttonsTableLayoutPanel.Height +
                this.buttonsTableLayoutPanel.Margin.Bottom +
                this.Padding.Bottom;
            int dy = this.Height - this.SizeFromClientSize( new Size( 0, height ) ).Height;
            this.MinimumSize = new Size( this.MinimumSize.Width, this.MinimumSize.Height - dy );
            this.Height -= dy;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Top;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Bottom;
            this.saveSelectionCheckBox.Anchor &= ~AnchorStyles.Top;
            this.saveSelectionCheckBox.Anchor |= AnchorStyles.Bottom;
            this.mainTableLayoutPanel.Anchor |= AnchorStyles.Bottom;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.RightToLeftLayoutChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRightToLeftLayoutChanged( EventArgs e )
        {
            base.OnRightToLeftLayoutChanged( e );
            if ( this.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.dataSourceLabel, this.dataSourceListBox );
                LayoutUtils.MirrorControl( this.dataProviderLabel, this.dataProviderComboBox );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.dataProviderLabel, this.dataProviderComboBox );
                LayoutUtils.UnmirrorControl( this.dataSourceLabel, this.dataSourceListBox );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.RightToLeftChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnRightToLeftChanged( EventArgs e )
        {
            base.OnRightToLeftChanged( e );
            if ( this.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.dataSourceLabel, this.dataSourceListBox );
                LayoutUtils.MirrorControl( this.dataProviderLabel, this.dataProviderComboBox );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.dataProviderLabel, this.dataProviderComboBox );
                LayoutUtils.UnmirrorControl( this.dataSourceLabel, this.dataSourceListBox );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.HelpRequested" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hevent">   A <see cref="T:System.Windows.Forms.HelpEventArgs" /> that contains
        ///                         the event data. </param>
        protected override void OnHelpRequested( HelpEventArgs hevent )
        {
            // Get the active control
            Control activeControl = HelpUtils.GetActiveControl( this );

            // Figure out the context
            DataConnectionDialogContext context = DataConnectionDialogContext.Source;
            if ( activeControl == this.dataSourceListBox )
            {
                context = DataConnectionDialogContext.SourceListBox;
            }
            if ( activeControl == this.dataProviderComboBox )
            {
                context = DataConnectionDialogContext.SourceProviderComboBox;
            }
            if ( activeControl == this.okButton )
            {
                context = DataConnectionDialogContext.SourceOkButton;
            }
            if ( activeControl == this.cancelButton )
            {
                context = DataConnectionDialogContext.SourceCancelButton;
            }

            // Call OnContextHelpRequested
            ContextHelpEventArgs e = new( context, hevent.MousePos );
            this._MainDialog.OnContextHelpRequested( e );
            hevent.Handled = e.Handled;
            if ( !e.Handled )
            {
                base.OnHelpRequested( hevent );
            }
        }

        /// <summary>   Processes Windows messages. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="m">    [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        ///                     process. </param>
        protected override void WndProc( ref Message m )
        {
            if ( this._MainDialog.TranslateHelpButton && HelpUtils.IsContextHelpMessage( ref m ) )
            {
                // Force the ? in the title bar to invoke the help topic
                HelpUtils.TranslateContextHelpMessage( this, ref m );
            }
            base.WndProc( ref m );
        }

        /// <summary>   Format data source. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        List control convert event information. </param>
        private void FormatDataSource( object sender, ListControlConvertEventArgs e )
        {
            if ( e.DesiredType == typeof( string ) )
            {
                e.Value = (e.ListItem as DataSource).DisplayName;
            }
        }

        /// <summary>   Change data source. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ChangeDataSource( object sender, EventArgs e )
        {
            this.dataProviderComboBox.Items.Clear();
            if ( this.dataSourceListBox.SelectedItem is DataSource newDataSource )
            {
                foreach ( DataProvider dataProvider in newDataSource.Providers )
                {
                    _ = this.dataProviderComboBox.Items.Add( dataProvider );
                }
                if ( !this._ProviderSelections.ContainsKey( newDataSource ) )
                {
                    this._ProviderSelections.Add( newDataSource, newDataSource.DefaultProvider );
                }
                this.dataProviderComboBox.SelectedItem = this._ProviderSelections[newDataSource];
            }
            else
            {
                _ = this.dataProviderComboBox.Items.Add( String.Empty );
            }
            this.ConfigureDescription();
            this.SetOkButtonStatus();
        }

        /// <summary>   Select data source. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SelectDataSource( object sender, EventArgs e )
        {
            if ( this.okButton.Enabled )
            {
                this.DialogResult = DialogResult.OK;
                this.DoOk( sender, e );
                this.Close();
            }
        }

        /// <summary>   Format data provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        List control convert event information. </param>
        private void FormatDataProvider( object sender, ListControlConvertEventArgs e )
        {
            if ( e.DesiredType == typeof( string ) )
            {
                e.Value = (e.ListItem is DataProvider) ? (e.ListItem as DataProvider).DisplayName : e.ListItem.ToString();
            }
        }

        /// <summary>   Sets data provider drop down width. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetDataProviderDropDownWidth( object sender, EventArgs e )
        {
            if ( this.dataProviderComboBox.Items.Count > 0 &&
                !(this.dataProviderComboBox.Items[0] is string) )
            {
                int largestWidth = 0;
                using ( Graphics g = Graphics.FromHwnd( this.dataProviderComboBox.Handle ) )
                {
                    foreach ( DataProvider dataProvider in this.dataProviderComboBox.Items )
                    {
                        int width = TextRenderer.MeasureText(
                            g,
                            dataProvider.DisplayName,
                            this.dataProviderComboBox.Font,
                            new Size( Int32.MaxValue, Int32.MaxValue ),
                            TextFormatFlags.WordBreak
                        ).Width;
                        if ( width > largestWidth )
                        {
                            largestWidth = width;
                        }
                    }
                }
                this.dataProviderComboBox.DropDownWidth = largestWidth + 3; // give a little extra margin
                if ( this.dataProviderComboBox.Items.Count > this.dataProviderComboBox.MaxDropDownItems )
                {
                    this.dataProviderComboBox.DropDownWidth += SystemInformation.VerticalScrollBarWidth;
                }
            }
            else
            {
                this.dataProviderComboBox.DropDownWidth = this.dataProviderComboBox.Width;
            }
        }

        /// <summary>   Change data provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ChangeDataProvider( object sender, EventArgs e )
        {
            if ( this.dataSourceListBox.SelectedItem != null )
            {
                this._ProviderSelections[this.dataSourceListBox.SelectedItem as DataSource] = this.dataProviderComboBox.SelectedItem as DataProvider;
            }
            this.ConfigureDescription();
            this.SetOkButtonStatus();
        }

        /// <summary>   Configure description. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void ConfigureDescription()
        {
            this.descriptionLabel.Text = this.dataProviderComboBox.SelectedItem is DataProvider
                ? this.dataSourceListBox.SelectedItem == this._MainDialog.UnspecifiedDataSource
                    ? (this.dataProviderComboBox.SelectedItem as DataProvider).Description
                    : (this.dataProviderComboBox.SelectedItem as DataProvider).GetDescription( this.dataSourceListBox.SelectedItem as DataSource )
                : null;
        }

        /// <summary>   Sets save selection. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetSaveSelection( object sender, EventArgs e )
        {
            this._MainDialog.SaveSelection = this.saveSelectionCheckBox.Checked;
        }

        /// <summary>   Sets ok button status. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void SetOkButtonStatus()
        {
            this.okButton.Enabled =
                this.dataSourceListBox.SelectedItem is DataSource &&
                this.dataProviderComboBox.SelectedItem is DataProvider;
        }

        /// <summary>   Executes the ok operation. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void DoOk( object sender, EventArgs e )
        {
            this._MainDialog.SetSelectedDataSourceInternal( this.dataSourceListBox.SelectedItem as DataSource );
            foreach ( DataSource dataSource in this.dataSourceListBox.Items )
            {
                DataProvider selectedProvider = (this._ProviderSelections.ContainsKey( dataSource )) ? this._ProviderSelections[dataSource] : null;
                this._MainDialog.SetSelectedDataProviderInternal( dataSource, selectedProvider );
            }
        }

        /// <summary>   The header label. </summary>
        private Label _HeaderLabel;

        /// <summary>   The provider selections. </summary>
        private readonly Dictionary<DataSource, DataProvider> _ProviderSelections = new();

        /// <summary>   The main dialog. </summary>
        private readonly DataConnectionDialog _MainDialog;
    }
}
