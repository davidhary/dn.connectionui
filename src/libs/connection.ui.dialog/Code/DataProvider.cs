//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A data provider. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class DataProvider
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">             The name. </param>
        /// <param name="displayName">      The name of the display. </param>
        /// <param name="shortDisplayName"> The name of the short display. </param>
        public DataProvider( string name, string displayName, string shortDisplayName )
            : this( name, displayName, shortDisplayName, null, null )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">             The name. </param>
        /// <param name="displayName">      The name of the display. </param>
        /// <param name="shortDisplayName"> The name of the short display. </param>
        /// <param name="description">      The description. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description )
            : this( name, displayName, shortDisplayName, description, null )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">                 The name. </param>
        /// <param name="displayName">          The name of the display. </param>
        /// <param name="shortDisplayName">     The name of the short display. </param>
        /// <param name="description">          The description. </param>
        /// <param name="targetConnectionType"> The type of the target connection. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType )
        {
            this.Name = name ?? throw new ArgumentNullException( nameof( name ) );
            this._DisplayName = displayName;
            this.ShortDisplayName = shortDisplayName;
            this._Description = description;
            this.TargetConnectionType = targetConnectionType;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">                     The name. </param>
        /// <param name="displayName">              The name of the display. </param>
        /// <param name="shortDisplayName">         The name of the short display. </param>
        /// <param name="description">              The description. </param>
        /// <param name="targetConnectionType">     The type of the target connection. </param>
        /// <param name="connectionPropertiesType"> Type of the connection properties. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType )
        {
            if ( connectionPropertiesType == null )
            {
                throw new ArgumentNullException( nameof( connectionPropertiesType ) );
            }

            this._ConnectionPropertiesTypes = new Dictionary<string, Type> {
                { String.Empty, connectionPropertiesType }
            };
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">                     The name. </param>
        /// <param name="displayName">              The name of the display. </param>
        /// <param name="shortDisplayName">         The name of the short display. </param>
        /// <param name="description">              The description. </param>
        /// <param name="targetConnectionType">     The type of the target connection. </param>
        /// <param name="connectionUIControlType">  Type of the connection user interface control. </param>
        /// <param name="connectionPropertiesType"> Type of the connection properties. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, Type connectionUIControlType, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionPropertiesType )
        {
            if ( connectionUIControlType == null )
            {
                throw new ArgumentNullException( nameof( connectionUIControlType ) );
            }

            this._ConnectionUIControlTypes = new Dictionary<string, Type> {
                { String.Empty, connectionUIControlType }
            };
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">                     The name. </param>
        /// <param name="displayName">              The name of the display. </param>
        /// <param name="shortDisplayName">         The name of the short display. </param>
        /// <param name="description">              The description. </param>
        /// <param name="targetConnectionType">     The type of the target connection. </param>
        /// <param name="connectionUIControlTypes"> (Immutable) list of types of the connection user
        ///                                         interface controls. </param>
        /// <param name="connectionPropertiesType"> Type of the connection properties. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, Type> connectionUIControlTypes, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionPropertiesType )
        {
            this._ConnectionUIControlTypes = connectionUIControlTypes;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">                     The name. </param>
        /// <param name="displayName">              The name of the display. </param>
        /// <param name="shortDisplayName">         The name of the short display. </param>
        /// <param name="description">              The description. </param>
        /// <param name="targetConnectionType">     The type of the target connection. </param>
        /// <param name="dataSourceDescriptions">   (Immutable) the data source descriptions. </param>
        /// <param name="connectionUIControlTypes"> (Immutable) list of types of the connection user
        ///                                         interface controls. </param>
        /// <param name="connectionPropertiesType"> Type of the connection properties. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, string> dataSourceDescriptions, IDictionary<string, Type> connectionUIControlTypes, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionUIControlTypes, connectionPropertiesType )
        {
            this._DataSourceDescriptions = dataSourceDescriptions;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">                         The name. </param>
        /// <param name="displayName">                  The name of the display. </param>
        /// <param name="shortDisplayName">             The name of the short display. </param>
        /// <param name="description">                  The description. </param>
        /// <param name="targetConnectionType">         The type of the target connection. </param>
        /// <param name="dataSourceDescriptions">       (Immutable) the data source descriptions. </param>
        /// <param name="connectionUIControlTypes">     (Immutable) list of types of the connection user
        ///                                             interface controls. </param>
        /// <param name="connectionPropertiesTypes">    (Immutable) list of types of the connection
        ///                                             properties. </param>
        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, string> dataSourceDescriptions, IDictionary<string, Type> connectionUIControlTypes, IDictionary<string, Type> connectionPropertiesTypes )
            : this( name, displayName, shortDisplayName, description, targetConnectionType )
        {
            this._DataSourceDescriptions = dataSourceDescriptions;
            this._ConnectionUIControlTypes = connectionUIControlTypes;
            this._ConnectionPropertiesTypes = connectionPropertiesTypes;
        }

        /// <summary>   Gets the SQL data provider. </summary>
        /// <value> The SQL data provider. </value>
        public static DataProvider SqlDataProvider
        {
            get {
                if ( _SqlDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new() {
                        { DataSource.SqlDataSource.Name, Dialog.Resources.Strings.DataProvider_Sql_DataSource_Description },
                        { DataSource.MicrosoftSqlServerFileName, Dialog.Resources.Strings.DataProvider_Sql_FileDataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new() {
                        { DataSource.SqlDataSource.Name, typeof( SqlConnectionUIControl ) },
                        { DataSource.MicrosoftSqlServerFileName, typeof( SqlFileConnectionUIControl ) },
                        { String.Empty, typeof( SqlConnectionUIControl ) }
                    };

                    Dictionary<string, Type> properties = new() {
                        { DataSource.MicrosoftSqlServerFileName, typeof( SqlFileConnectionProperties ) },
                        { String.Empty, typeof( SqlConnectionProperties ) }
                    };

                    _SqlDataProvider = new DataProvider(
                        "System.Data.SqlClient",
                        Dialog.Resources.Strings.DataProvider_Sql,
                        Dialog.Resources.Strings.DataProvider_Sql_Short,
                        Dialog.Resources.Strings.DataProvider_Sql_Description,
                        typeof( System.Data.SqlClient.SqlConnection ),
                        descriptions,
                        uiControls,
                        properties );
                }
                return _SqlDataProvider;
            }
        }
        /// <summary>   The SQL data provider. </summary>
        private static DataProvider _SqlDataProvider;

        /// <summary>   Gets the oracle data provider. </summary>
        /// <value> The oracle data provider. </value>
        public static DataProvider OracleDataProvider
        {
            get {
                if ( _OracleDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new() {
                        { DataSource.OracleDataSource.Name, Dialog.Resources.Strings.DataProvider_Oracle_DataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new() {
                        { String.Empty, typeof( OracleConnectionUIControl ) }
                    };

                    _OracleDataProvider = new DataProvider(
                        "System.Data.OracleClient",
                        Dialog.Resources.Strings.DataProvider_Oracle,
                        Dialog.Resources.Strings.DataProvider_Oracle_Short,
                        Dialog.Resources.Strings.DataProvider_Oracle_Description,
                        // Disable OracleClient deprecation warnings
#pragma warning disable CS0618 // Type or member is obsolete
                        typeof( System.Data.OracleClient.OracleConnection ), descriptions, uiControls,
#pragma warning restore CS0618 // Type or member is obsolete
                        typeof( OracleConnectionProperties ) );
                }
                return _OracleDataProvider;
            }
        }
        /// <summary>   The oracle data provider. </summary>
        private static DataProvider _OracleDataProvider;

        /// <summary>   Gets the OLE database data provider. </summary>
        /// <value> The OLE database data provider. </value>
        public static DataProvider OleDBDataProvider
        {
            get {
                if ( _OleDBDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new() {
                        { DataSource.SqlDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_SqlDataSource_Description },
                        { DataSource.OracleDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_OracleDataSource_Description },
                        { DataSource.AccessDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_AccessDataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new() {
                        { DataSource.SqlDataSource.Name, typeof( SqlConnectionUIControl ) },
                        { DataSource.OracleDataSource.Name, typeof( OracleConnectionUIControl ) },
                        { DataSource.AccessDataSource.Name, typeof( AccessConnectionUIControl ) },
                        { String.Empty, typeof( OleDBConnectionUIControl ) }
                    };

                    Dictionary<string, Type> properties = new() {
                        { DataSource.SqlDataSource.Name, typeof( OleDBSqlConnectionProperties ) },
                        { DataSource.OracleDataSource.Name, typeof( OleDBOracleConnectionProperties ) },
                        { DataSource.AccessDataSource.Name, typeof( OleDBAccessConnectionProperties ) },
                        { String.Empty, typeof( OleDBConnectionProperties ) }
                    };

                    _OleDBDataProvider = new DataProvider(
                        "System.Data.OleDb",
                        Dialog.Resources.Strings.DataProvider_OleDB,
                        Dialog.Resources.Strings.DataProvider_OleDB_Short,
                        Dialog.Resources.Strings.DataProvider_OleDB_Description,
                        typeof( System.Data.OleDb.OleDbConnection ),
                        descriptions,
                        uiControls,
                        properties );
                }
                return _OleDBDataProvider;
            }
        }
        /// <summary>   The OLE database data provider. </summary>
        private static DataProvider _OleDBDataProvider;

        /// <summary>   Gets the ODBC data provider. </summary>
        /// <value> The ODBC data provider. </value>
        public static DataProvider OdbcDataProvider
        {
            get {
                if ( _OdbcDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new() {
                        { DataSource.OdbcDataSource.Name, Dialog.Resources.Strings.DataProvider_Odbc_DataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new() {
                        { String.Empty, typeof( OdbcConnectionUIControl ) }
                    };

                    _OdbcDataProvider = new DataProvider(
                        "System.Data.Odbc",
                        Dialog.Resources.Strings.DataProvider_Odbc,
                        Dialog.Resources.Strings.DataProvider_Odbc_Short,
                        Dialog.Resources.Strings.DataProvider_Odbc_Description,
                        typeof( System.Data.Odbc.OdbcConnection ),
                        descriptions,
                        uiControls,
                        typeof( OdbcConnectionProperties ) );
                }
                return _OdbcDataProvider;
            }
        }
        /// <summary>   The ODBC data provider. </summary>
        private static DataProvider _OdbcDataProvider;

        /// <summary>   Gets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; }

        /// <summary>   Gets the name of the display. </summary>
        /// <value> The name of the display. </value>
        public string DisplayName => this._DisplayName ?? this.Name;

        /// <summary>   Gets the name of the short display. </summary>
        /// <value> The name of the short display. </value>
        public string ShortDisplayName { get; }

        /// <summary>   Gets the description. </summary>
        /// <value> The description. </value>
        public string Description => this.GetDescription( null );

        /// <summary>   Gets the type of the target connection. </summary>
        /// <value> The type of the target connection. </value>
        public Type TargetConnectionType { get; }

        /// <summary>   Gets a description. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dataSource">   The data source. </param>
        /// <returns>   The description. </returns>
        public virtual string GetDescription( DataSource dataSource )
        {
            return this._DataSourceDescriptions != null && dataSource != null &&
                this._DataSourceDescriptions.ContainsKey( dataSource.Name )
                ? this._DataSourceDescriptions[dataSource.Name]
                : this._Description;
        }

        /// <summary>   Creates connection user interface control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   The new connection user interface control. </returns>
        public IDataConnectionUIControl CreateConnectionUIControl()
        {
            return this.CreateConnectionUIControl( null );
        }

        /// <summary>   Creates connection user interface control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dataSource">   The data source. </param>
        /// <returns>   The new connection user interface control. </returns>
        public virtual IDataConnectionUIControl CreateConnectionUIControl( DataSource dataSource )
        {
            string key;
            return this._ConnectionUIControlTypes != null &&
                (dataSource != null && this._ConnectionUIControlTypes.ContainsKey( key = dataSource.Name )) ||
                this._ConnectionUIControlTypes.ContainsKey( key = String.Empty )
                ? Activator.CreateInstance( this._ConnectionUIControlTypes[key] ) as IDataConnectionUIControl
                : null;
        }

        /// <summary>   Creates connection properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   The new connection properties. </returns>
        public IDataConnectionProperties CreateConnectionProperties()
        {
            return this.CreateConnectionProperties( null );
        }

        /// <summary>   Creates connection properties. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dataSource">   The data source. </param>
        /// <returns>   The new connection properties. </returns>
        public virtual IDataConnectionProperties CreateConnectionProperties( DataSource dataSource )
        {
            string key;
            return this._ConnectionPropertiesTypes != null &&
                ((dataSource != null && this._ConnectionPropertiesTypes.ContainsKey( key = dataSource.Name )) ||
                this._ConnectionPropertiesTypes.ContainsKey( key = String.Empty ))
                ? Activator.CreateInstance( this._ConnectionPropertiesTypes[key] ) as IDataConnectionProperties
                : null;
        }

        /// <summary>   (Immutable) name of the display. </summary>
        private readonly string _DisplayName;
        /// <summary>   (Immutable) the description. </summary>
        private readonly string _Description;
        /// <summary>   (Immutable) the data source descriptions. </summary>
        private readonly IDictionary<string, string> _DataSourceDescriptions;
        /// <summary>   (Immutable) list of types of the connection user interface controls. </summary>
        private readonly IDictionary<string, Type> _ConnectionUIControlTypes;
        /// <summary>   (Immutable) list of types of the connection properties. </summary>
        private readonly IDictionary<string, Type> _ConnectionPropertiesTypes;
    }
}
