﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Additional information for context help events. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class ContextHelpEventArgs : HelpEventArgs
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="context">  The context. </param>
        /// <param name="mousePos"> The mouse position. </param>
        public ContextHelpEventArgs( DataConnectionDialogContext context, Point mousePos ) : base( mousePos )
        {
            this.Context = context;
        }

        /// <summary>   Gets the context. </summary>
        /// <value> The context. </value>
        public DataConnectionDialogContext Context { get; }
    }
}
