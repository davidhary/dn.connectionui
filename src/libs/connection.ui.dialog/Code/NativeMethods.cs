//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A native methods. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal sealed class NativeMethods
    {
        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private NativeMethods()
        {
        }

        #region Macros

        /// <summary>   SQL succeeded. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="rc">   The rectangle. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        internal static bool SQL_SUCCEEDED( short rc )
        {
            return (((rc) & (~1)) == 0);
        }

        /// <summary>   Low Words. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dwValue">  The value. </param>
        /// <returns>   A short. </returns>
        internal static short LOWORD( int dwValue )
        {
            return ( short ) (dwValue & 0xffff);
        }

        /// <summary>   Hi Words. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dwValue">  The value. </param>
        /// <returns>   A short. </returns>
        internal static short HIWORD( int dwValue )
        {
            return ( short ) ((dwValue >> 16) & 0xffff);
        }

        #endregion

        #region Interfaces

        /// <summary>   Interface for data initialize. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [ComImport]
        [Guid( "2206CCB1-19C1-11D1-89E0-00C04FD7A829" )]
        [InterfaceType( ComInterfaceType.InterfaceIsIUnknown )]
        internal interface IDataInitialize
        {
            /// <summary>   Gets data source. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="pUnkOuter">                The unk outer. </param>
            /// <param name="dwClsCtx">                 Context for the cls. </param>
            /// <param name="pwszInitializationString"> The pwsz initialization string. </param>
            /// <param name="riid">                     [in,out] The riid. </param>
            /// <param name="ppDataSource">             [in,out] The data source. </param>
            void GetDataSource(
                [In, MarshalAs( UnmanagedType.IUnknown )] object pUnkOuter,
                [In, MarshalAs( UnmanagedType.U4 )] int dwClsCtx,
                [In, MarshalAs( UnmanagedType.LPWStr )] string pwszInitializationString,
                [In] ref Guid riid,
                [In, Out, MarshalAs( UnmanagedType.IUnknown )] ref object ppDataSource );

            /// <summary>   Gets initialization string. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="pDataSource">      The data source. </param>
            /// <param name="fIncludePassword"> True to include, false to exclude the password. </param>
            /// <param name="ppwszInitString">  [out] The ppwsz initialize string. </param>
            void GetInitializationString(
                [In, MarshalAs( UnmanagedType.IUnknown )] object pDataSource,
                [In, MarshalAs( UnmanagedType.I1 )] bool fIncludePassword,
                [Out, MarshalAs( UnmanagedType.LPWStr )] out string ppwszInitString );

            /// <summary>   Unused create database instance. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            void Unused_CreateDBInstance();
            /// <summary>   Unused create database instance exception. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            void Unused_CreateDBInstanceEx();
            /// <summary>   Unused load string from storage. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            void Unused_LoadStringFromStorage();
            /// <summary>   Unused write string to storage. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            void Unused_WriteStringToStorage();
        }

        /// <summary>   Interface for idb prompt initialize. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [ComImport]
        [Guid( "2206CCB0-19C1-11D1-89E0-00C04FD7A829" )]
        [InterfaceType( ComInterfaceType.InterfaceIsIUnknown )]
        internal interface IDBPromptInitialize
        {
            /// <summary>   Prompt data source. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="pUnkOuter">                The unk outer. </param>
            /// <param name="hwndParent">               The parent. </param>
            /// <param name="dwPromptOptions">          Options for controlling the prompt. </param>
            /// <param name="cSourceTypeFilter">        A filter specifying the source type. </param>
            /// <param name="rgSourceTypeFilter">       A filter specifying the source type. </param>
            /// <param name="pwszszzProviderFilter">    A filter specifying the pwszszz provider. </param>
            /// <param name="riid">                     [in,out] The riid. </param>
            /// <param name="ppDataSource">             [in,out] The data source. </param>
            void PromptDataSource(
                [In, MarshalAs( UnmanagedType.IUnknown )] object pUnkOuter,
                [In] IntPtr hwndParent,
                [In, MarshalAs( UnmanagedType.U4 )] int dwPromptOptions,
                [In, MarshalAs( UnmanagedType.U4 )] int cSourceTypeFilter,
                [In] IntPtr rgSourceTypeFilter,
                [In, MarshalAs( UnmanagedType.LPWStr )] string pwszszzProviderFilter,
                [In] ref Guid riid,
                [In, Out, MarshalAs( UnmanagedType.IUnknown )] ref object ppDataSource );

            /// <summary>   Unused prompt file name. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            void Unused_PromptFileName();
        }

        #endregion

        #region Structures

        /// <summary>   A helpinfo. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        [StructLayout( LayoutKind.Sequential )]
        internal class HELPINFO
        {
            /// <summary>   The size. </summary>
            public int cbSize = Marshal.SizeOf( typeof( HELPINFO ) );
            /// <summary>   Type of the context. </summary>
            public int iContextType;
            /// <summary>   Identifier for the control. </summary>
            public int iCtrlId;
            /// <summary>   Handle of the item. </summary>
            public IntPtr hItemHandle;
            /// <summary>   Identifier for the context. </summary>
            public int dwContextId;
            /// <summary>   The mouse position. </summary>
            public POINT MousePos;
        }

        /// <summary>   A point. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        [StructLayout( LayoutKind.Sequential )]
        internal struct POINT
        {
            /// <summary>   The x coordinate. </summary>
            public int x;
            /// <summary>   The y coordinate. </summary>
            public int y;
        }

        #endregion

        #region Functions

        /// <summary>   SQL allocate environment. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="environmentHandle">    [out] Handle of the environment. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll" )]
        internal static extern short SQLAllocEnv( out IntPtr environmentHandle );

        /// <summary>   SQL allocate connect. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="environmentHandle">    Handle of the environment. </param>
        /// <param name="connectionHandle">     [out] Handle of the connection. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll" )]
        internal static extern short SQLAllocConnect( IntPtr environmentHandle, out IntPtr connectionHandle );

        /// <summary>   SQL driver connect. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hdbc">                 The hdbc. </param>
        /// <param name="hwnd">                 The hwnd. </param>
        /// <param name="szConnStrIn">          The connection string in. </param>
        /// <param name="cbConnStrIn">          The connection string in. </param>
        /// <param name="szConnStrOut">         The connection string out. </param>
        /// <param name="cbConnStrOutMax">      The connection string out maximum. </param>
        /// <param name="pcbConnStrOut">        [out] The pcb connection string out. </param>
        /// <param name="fDriverCompletion">    The driver completion. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll", EntryPoint = "SQLDriverConnectW", CharSet = CharSet.Unicode )]
        internal static extern short SQLDriverConnect( IntPtr hdbc, IntPtr hwnd, string szConnStrIn, short cbConnStrIn, System.Text.StringBuilder szConnStrOut, short cbConnStrOutMax, out short pcbConnStrOut, ushort fDriverCompletion );

        /// <summary>   SQL disconnect. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="connectionHandle"> Handle of the connection. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll" )]
        internal static extern short SQLDisconnect( IntPtr connectionHandle );

        /// <summary>   SQL free connect. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="connectionHandle"> Handle of the connection. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll" )]
        internal static extern short SQLFreeConnect( IntPtr connectionHandle );

        /// <summary>   SQL free environment. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="environmentHandle">    Handle of the environment. </param>
        /// <returns>   A short. </returns>
        [DllImport( "odbc32.dll" )]
        internal static extern short SQLFreeEnv( IntPtr environmentHandle );

        /// <summary>   SQL get installed drivers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="lpszBuf">      The buffer. </param>
        /// <param name="cbBufMax">     The buffer maximum. </param>
        /// <param name="pcbBufOut">    [in,out] The pcb buffer out. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [DllImport( "odbccp32.dll", CharSet = CharSet.Unicode )]
        internal static extern bool SQLGetInstalledDrivers( char[] lpszBuf, int cbBufMax, ref int pcbBufOut );

        /// <summary>   SQL get private profile string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="lpszSection">  The section. </param>
        /// <param name="lpszEntry">    The entry. </param>
        /// <param name="lpszDefault">  The default. </param>
        /// <param name="retBuffer">    Buffer for ret data. </param>
        /// <param name="cbRetBuffer">  Buffer for ret data. </param>
        /// <param name="lpszFilename"> Filename of the file. </param>
        /// <returns>   An int. </returns>
        [DllImport( "odbccp32.dll", CharSet = CharSet.Unicode )]
        internal static extern int SQLGetPrivateProfileString( string lpszSection, string lpszEntry, string lpszDefault, StringBuilder retBuffer, int cbRetBuffer, string lpszFilename );

        /// <summary>   Used to check if OS is 64 bits. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hProcess"> The process. </param>
        /// <param name="pIsWow64"> [out] True if is wow 64, false if not. </param>
        /// <returns>   True if wow 64 process, false if not. </returns>
        [DllImport( "kernel32" )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool IsWow64Process( IntPtr hProcess, out bool pIsWow64 );

        /// <summary>   Used to access 64 bit registry section from 32 bits application. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hKey">         The key. </param>
        /// <param name="lpSubKey">     The sub key. </param>
        /// <param name="ulOptions">    Options for controlling the ul. </param>
        /// <param name="samDesired">   The sam desired. </param>
        /// <param name="phkResult">    [out] The phk result. </param>
        /// <returns>   An int. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        [DllImport( "advapi32" )]
        internal static extern int RegOpenKeyEx( UIntPtr hKey, string lpSubKey, int ulOptions, int samDesired, out UIntPtr phkResult );

        /// <summary>   Registers the query value exception. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hKey">         The key. </param>
        /// <param name="lpValueName">  Name of the value. </param>
        /// <param name="lpReserved">   The reserved. </param>
        /// <param name="lpType">       [in,out] The type. </param>
        /// <param name="lpData">       The data. </param>
        /// <param name="lpchData">     [in,out] Information describing the lpch. </param>
        /// <returns>   An int. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        [DllImport( "advapi32" )]
        internal static extern int RegQueryValueEx( UIntPtr hKey, string lpValueName, uint lpReserved, ref uint lpType, IntPtr lpData, ref int lpchData );

        /// <summary>   Registers the query information key. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hkey">                     The hkey. </param>
        /// <param name="lpClass">                  The class. </param>
        /// <param name="lpcbClass">                The lpcb class. </param>
        /// <param name="lpReserved">               The reserved. </param>
        /// <param name="lpcSubKeys">               [out] The lpc sub keys. </param>
        /// <param name="lpcbMaxSubKeyLen">         Length of the lpcb maximum sub key. </param>
        /// <param name="lpcbMaxClassLen">          Length of the lpcb maximum class. </param>
        /// <param name="lpcValues">                [out] The lpc values. </param>
        /// <param name="lpcbMaxValueNameLen">      Length of the lpcb maximum value name. </param>
        /// <param name="lpcbMaxValueLen">          Length of the lpcb maximum value. </param>
        /// <param name="lpcbSecurityDescriptor">   Information describing the lpcb security. </param>
        /// <param name="lpftLastWriteTime">        The lpft last write time. </param>
        /// <returns>   An int. </returns>
        [DllImport( "advapi32.dll" )]
        internal static extern int RegQueryInfoKey( UIntPtr hkey, byte[] lpClass, IntPtr lpcbClass, IntPtr lpReserved, out uint lpcSubKeys, IntPtr lpcbMaxSubKeyLen, IntPtr lpcbMaxClassLen, out uint lpcValues, IntPtr lpcbMaxValueNameLen, IntPtr lpcbMaxValueLen, IntPtr lpcbSecurityDescriptor, IntPtr lpftLastWriteTime );

        /// <summary>   Registers the enum value. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hkey">             The hkey. </param>
        /// <param name="index">            Zero-based index of the. </param>
        /// <param name="lpValueName">      Name of the value. </param>
        /// <param name="lpcbValueName">    [in,out] Name of the lpcb value. </param>
        /// <param name="reserved">         The reserved. </param>
        /// <param name="lpType">           The type. </param>
        /// <param name="lpData">           The data. </param>
        /// <param name="lpcbData">         Information describing the lpcb. </param>
        /// <returns>   An int. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        [DllImport( "advapi32.dll" )]
        internal static extern int RegEnumValue( UIntPtr hkey, uint index, StringBuilder lpValueName, ref uint lpcbValueName, IntPtr reserved, IntPtr lpType, IntPtr lpData, IntPtr lpcbData );

        /// <summary>   Registers the close key described by hKey. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hKey"> The key. </param>
        /// <returns>   An uint. </returns>
        [DllImport( "advapi32" )]
        internal static extern uint RegCloseKey( UIntPtr hKey );

        /// <summary>   (Immutable) the hkey local machine. </summary>
        internal static readonly UIntPtr HKEY_LOCAL_MACHINE = new( (( uint ) 0x80000002) );
        /// <summary>   (Immutable) the key wow 64 key. </summary>
        internal const int KEY_WOW64_64KEY = 0x0100;
        /// <summary>   (Immutable) the key wow 64 32 key. </summary>
        internal const int KEY_WOW64_32KEY = 0x0200;
        /// <summary>   (Immutable) the key query value. </summary>
        internal const int KEY_QUERY_VALUE = 0x1;

        #endregion

        #region Guids

        /// <summary>   The iid i unknown. </summary>
        internal static Guid IID_IUnknown = new( "00000000-0000-0000-c000-000000000046" );
        /// <summary>   The clsid data links. </summary>
        internal static Guid CLSID_DataLinks = new( "2206CDB2-19C1-11d1-89E0-00C04FD7A829" );
        /// <summary>   The clsid oledb enumerator. </summary>
        internal static Guid CLSID_OLEDB_ENUMERATOR = new( "C8B522D0-5CF3-11ce-ADE5-00AA0044773D" );
        /// <summary>   The clsid msdasql enumerator. </summary>
        internal static Guid CLSID_MSDASQL_ENUMERATOR = new( "C8B522CD-5CF3-11ce-ADE5-00AA0044773D" );

        #endregion

        #region Constants

        /// <summary>   (Immutable) HRESULT codes. </summary>
        internal const int
            DB_E_CANCELED = unchecked(( int ) 0x80040E4E);

        /// <summary>   (Immutable) COM class contexts. </summary>
        internal const int
            CLSCTX_INPROC_SERVER = 1;

        /// <summary>   (Immutable) Window messages. </summary>
        internal const int
            WM_SETFOCUS = 0x0007,
            WM_HELP = 0x0053,
            WM_CONTEXTMENU = 0x007B,
            WM_SYSCOMMAND = 0x0112;

        /// <summary>   (Immutable) Window system commands. </summary>
        internal const int
            SC_CONTEXTHELP = 0xF180;

        /// <summary>   (Immutable) HELPINFO constants. </summary>
        internal const int
            HELPINFO_WINDOW = 0x0001;

        /// <summary>   (Immutable) OLE DB database source types. </summary>
        internal const int
            DBSOURCETYPE_DATASOURCE_TDP = 1,
            DBSOURCETYPE_DATASOURCE_MDP = 3;

        /// <summary>   (Immutable) OLE DB Data Links dialog prompt options. </summary>
        internal const int
            DBPROMPTOPTIONS_PROPERTYSHEET = 0x02,
            DBPROMPTOPTIONS_DISABLE_PROVIDER_SELECTION = 0x10;

        /// <summary>   (Immutable) ODBC Driver prompt options. </summary>
        internal const ushort
            SQL_DRIVER_PROMPT = 2;

        /// <summary>   (Immutable) ODBC return values. </summary>
        internal const short
            SQL_NO_DATA = 100;

        #endregion
    }
}
