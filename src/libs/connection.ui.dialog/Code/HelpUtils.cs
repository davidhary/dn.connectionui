//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A help utilities. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal sealed class HelpUtils
    {
        /// <summary>   (Immutable) 1024 should be enough for registry key value name. </summary>
        private const int _KeyValueNameLength = 1024;

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private HelpUtils()
        {
        }

        /// <summary>   Query if 'm' is context help message. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="m">    [in,out] A Message to process. </param>
        /// <returns>   True if context help message, false if not. </returns>
        public static bool IsContextHelpMessage( ref Message m )
        {
            return (m.Msg == NativeMethods.WM_SYSCOMMAND && (( int ) m.WParam & 0xFFF0) == NativeMethods.SC_CONTEXTHELP);
        }


        /// <summary>   This function checks if the OS is 64 bits. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   True if wow 64, false if not. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static bool IsWow64()
        {
            bool isWow64 = false;
            if ( Environment.OSVersion.Version.Major >= 5 )
            {
                System.Diagnostics.Process curProcess = System.Diagnostics.Process.GetCurrentProcess();
                try
                {
                    _ = NativeMethods.IsWow64Process( curProcess.Handle, out isWow64 );
                }
                catch ( Exception e )
                {
                    isWow64 = false;
                    Debug.Fail( "Failed in calling IsWow64Process: " + e.Message );
                }
            }

            return isWow64;
        }

        /// <summary>
        /// Get ValueNames from registry for WoW64 machine. Corresponding to
        /// Microsoft.Win32.RegistryKey.GetValueNames().  
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="registryKey">  Registry key string value. </param>
        /// <param name="ulOptions">    Access key value options. </param>
        /// <returns>   An array of string. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static string[] GetValueNamesWow64( string registryKey, int ulOptions )
        {
            UIntPtr nameKey = UIntPtr.Zero;
            int lResult = 0;
            string[] valueNames = null;

            try
            {
                lResult = NativeMethods.RegOpenKeyEx( NativeMethods.HKEY_LOCAL_MACHINE, registryKey, 0, ulOptions, out nameKey );
            }
            catch
            {
                // Ignore native exceptions. 
            }
            if ( lResult == 0 && UIntPtr.Equals( nameKey, UIntPtr.Zero ) == false )
            {
                uint numValues = 0;
                try
                {
                    lResult = NativeMethods.RegQueryInfoKey( nameKey, null, IntPtr.Zero, IntPtr.Zero, out uint numSubKeys, IntPtr.Zero, IntPtr.Zero, out numValues, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero );
                }
                catch
                {
                    // Ignore native exceptions. 
                }

                if ( lResult == 0 )
                {
                    valueNames = new string[numValues];

                    for ( uint index = 0; index < numValues; index++ )
                    {
                        StringBuilder builder = new( _KeyValueNameLength );
                        uint size = _KeyValueNameLength;

                        try
                        {
                            lResult = NativeMethods.RegEnumValue( nameKey, index, builder, ref size, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero );
                        }
                        catch
                        {
                            // Ignore native exceptions. 
                        }

                        if ( lResult == 0 )
                        {
                            valueNames[index] = builder.ToString();
                        }
                    }
                }
            }
            return valueNames ?? Array.Empty<string>();
        }

        /// <summary>   Translate context help message. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="f">    A Form to process. </param>
        /// <param name="m">    [in,out] A Message to process. </param>
        public static void TranslateContextHelpMessage( Form f, ref Message m )
        {
            Debug.Assert( f != null );

            Control activeControl = GetActiveControl( f );
            if ( activeControl != null )
            {
                // Turn this message into a WM_HELP message
                m.HWnd = activeControl.Handle;
                m.Msg = NativeMethods.WM_HELP;
                m.WParam = IntPtr.Zero;
                NativeMethods.HELPINFO helpInfo = new() {
                    iContextType = NativeMethods.HELPINFO_WINDOW,
                    iCtrlId = f.Handle.ToInt32(),
                    hItemHandle = activeControl.Handle,
                    dwContextId = 0
                };
                helpInfo.MousePos.x = ( int ) NativeMethods.LOWORD( ( int ) m.LParam );
                helpInfo.MousePos.y = ( int ) NativeMethods.HIWORD( ( int ) m.LParam );
                m.LParam = Marshal.AllocHGlobal( Marshal.SizeOf( helpInfo ) );
                Marshal.StructureToPtr( helpInfo, m.LParam, false );
            }
        }

        /// <summary>   Gets active control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="f">    A Form to process. </param>
        /// <returns>   The active control. </returns>
        public static Control GetActiveControl( Form f )
        {
            Control activeControl = f;
            while ( activeControl is ContainerControl containerControl &&
                containerControl.ActiveControl != null )
            {
                activeControl = containerControl.ActiveControl;
            }
            return activeControl;
        }
    }
}
