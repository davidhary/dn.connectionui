//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A dynamic property descriptor. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal class DynamicPropertyDescriptor : PropertyDescriptor
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name"> The name of the member. </param>
        public DynamicPropertyDescriptor( string name )
            : base( name, null )
        {
        }

#if NOT_USED
		public DynamicPropertyDescriptor(string name, string displayName) : this(name, displayName, null, null, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category) : this(name, displayName, category, null, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category, string description) : this(name, displayName, category, description, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category, string description, bool isReadOnly) : base(name, BuildAttributes(displayName, category, description, isReadOnly))
		{
		}
#endif

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="name">         The name of the member. </param>
        /// <param name="attributes">   The attributes. </param>
        public DynamicPropertyDescriptor( string name, params Attribute[] attributes )
            : base( name, FilterAttributes( attributes ) )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="baseDescriptor">   Information describing the base. </param>
        public DynamicPropertyDescriptor( PropertyDescriptor baseDescriptor )
            : this( baseDescriptor, null )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="baseDescriptor">   Information describing the base. </param>
        /// <param name="newAttributes">    A variable-length parameters list containing new attributes. </param>
        public DynamicPropertyDescriptor( PropertyDescriptor baseDescriptor, params Attribute[] newAttributes )
            : base( baseDescriptor, newAttributes )
        {
            this.AttributeArray = FilterAttributes( this.AttributeArray );
            this._BaseDescriptor = baseDescriptor;
        }

        /// <summary>   Gets the name of the member. </summary>
        /// <value> The name of the member. </value>
        public override string Name => this._Name ?? base.Name;

        /// <summary>
        /// Gets the name of the category to which the member belongs, as specified in the
        /// <see cref="T:System.ComponentModel.CategoryAttribute" />.
        /// </summary>
        /// <value>
        /// The name of the category to which the member belongs. If there is no
        /// <see cref="T:System.ComponentModel.CategoryAttribute" />, the category name is set to the
        /// default category, <see langword="Misc" />.
        /// </value>
        public override string Category => this._Category ?? base.Category;

        /// <summary>
        /// Gets the description of the member, as specified in the
        /// <see cref="T:System.ComponentModel.DescriptionAttribute" />.
        /// </summary>
        /// <value>
        /// The description of the member. If there is no
        /// <see cref="T:System.ComponentModel.DescriptionAttribute" />, the property value is set to the
        /// default, which is an empty string ("").
        /// </value>
        public override string Description => this._Description ?? base.Description;

        /// <summary>   When overridden in a derived class, gets the type of the property. </summary>
        /// <value> A <see cref="T:System.Type" /> that represents the type of the property. </value>
        public override Type PropertyType => this._PropertyType ?? (this._BaseDescriptor?.PropertyType);

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether this property is read-
        /// only.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the property is read-only; otherwise, <see langword="false" />.
        /// </value>
        public override bool IsReadOnly => (ReadOnlyAttribute.Yes.Equals( this.Attributes[typeof( ReadOnlyAttribute )] ));

        /// <summary>   Gets the type converter for this property. </summary>
        /// <value>
        /// A <see cref="T:System.ComponentModel.TypeConverter" /> that is used to convert the
        /// <see cref="T:System.Type" /> of this property.
        /// </value>
        public override TypeConverter Converter
        {
            get {
                if ( this._ConverterTypeName != null )
                {
                    if ( this._Converter == null )
                    {
                        Type converterType = this.GetTypeFromName( this._ConverterTypeName );
                        if ( typeof( TypeConverter ).IsAssignableFrom( converterType ) )
                        {
                            this._Converter = ( TypeConverter ) this.CreateInstance( converterType );
                        }
                    }
                    if ( this._Converter != null )
                    {
                        return this._Converter;
                    }
                }
                return base.Converter;
            }
        }

        /// <summary>   Gets the collection of attributes for this member. </summary>
        /// <value>
        /// An <see cref="T:System.ComponentModel.AttributeCollection" /> that provides the attributes
        /// for this member, or an empty collection if there are no attributes in the
        /// <see cref="P:System.ComponentModel.MemberDescriptor.AttributeArray" />.
        /// </value>
        public override AttributeCollection Attributes
        {
            get {
                if ( this._Attributes != null )
                {
                    Dictionary<object, Attribute> attributes = new();
                    foreach ( Attribute attr in this.AttributeArray )
                    {
                        attributes[attr.TypeId] = attr;
                    }
                    foreach ( Attribute attr in this._Attributes )
                    {
                        if ( !attr.IsDefaultAttribute() )
                        {
                            attributes[attr.TypeId] = attr;
                        }
                        else if ( attributes.ContainsKey( attr.TypeId ) )
                        {
                            _ = attributes.Remove( attr.TypeId );
                        }
                        if ( attr is CategoryAttribute categoryAttr )
                        {
                            this._Category = categoryAttr.Category;
                        }
                        if ( attr is DescriptionAttribute descriptionAttr )
                        {
                            this._Description = descriptionAttr.Description;
                        }
                        if ( attr is TypeConverterAttribute typeConverterAttr )
                        {
                            this._ConverterTypeName = typeConverterAttr.ConverterTypeName;
                            this._Converter = null;
                        }
                    }
                    Attribute[] newAttributes = new Attribute[attributes.Values.Count];
                    attributes.Values.CopyTo( newAttributes, 0 );
                    this.AttributeArray = newAttributes;
                    this._Attributes = null;
                }
                return base.Attributes;
            }
        }

        /// <summary>   Gets or sets the get value handler. </summary>
        /// <value> The get value handler. </value>
        public GetValueHandler GetValueHandler { get; set; }

        /// <summary>   Gets or sets the set value handler. </summary>
        /// <value> The set value handler. </value>
        public SetValueHandler SetValueHandler { get; set; }

        /// <summary>   Gets or sets the can reset value handler. </summary>
        /// <value> The can reset value handler. </value>
        public CanResetValueHandler CanResetValueHandler { get; set; }

        /// <summary>   Gets or sets the reset value handler. </summary>
        /// <value> The reset value handler. </value>
        public ResetValueHandler ResetValueHandler { get; set; }

        /// <summary>   Gets or sets the should serialize value handler. </summary>
        /// <value> The should serialize value handler. </value>
        public ShouldSerializeValueHandler ShouldSerializeValueHandler { get; set; }

        /// <summary>   Gets or sets the get child properties handler. </summary>
        /// <value> The get child properties handler. </value>
        public GetChildPropertiesHandler GetChildPropertiesHandler { get; set; }

        /// <summary>
        /// When overridden in a derived class, gets the type of the component this property is bound to.
        /// </summary>
        /// <value>
        /// A <see cref="T:System.Type" /> that represents the type of component this property is bound
        /// to. When the
        /// <see cref="M:System.ComponentModel.PropertyDescriptor.GetValue(System.Object)" /> or
        /// <see cref="M:System.ComponentModel.PropertyDescriptor.SetValue(System.Object,System.Object)" />
        /// methods are invoked, the object specified might be an instance of this type.
        /// </value>
        public override Type ComponentType => this._ComponentType ?? (this._BaseDescriptor?.ComponentType);

        /// <summary>   Sets a name. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetName( string value )
        {
            if ( value == null )
            {
                value = String.Empty;
            }
            this._Name = value;
        }

        /// <summary>   Sets display name. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetDisplayName( string value )
        {
            if ( value == null )
            {
                value = DisplayNameAttribute.Default.DisplayName;
            }
            this.SetAttribute( new DisplayNameAttribute( value ) );
        }

        /// <summary>   Sets a category. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetCategory( string value )
        {
            if ( value == null )
            {
                value = CategoryAttribute.Default.Category;
            }
            this._Category = value;
            this.SetAttribute( new CategoryAttribute( value ) );
        }

        /// <summary>   Sets a description. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetDescription( string value )
        {
            if ( value == null )
            {
                value = DescriptionAttribute.Default.Description;
            }
            this._Description = value;
            this.SetAttribute( new DescriptionAttribute( value ) );
        }

        /// <summary>   Sets property type. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="value">    The new value. </param>
        public void SetPropertyType( Type value )
        {
            this._PropertyType = value ?? throw new ArgumentNullException( nameof( value ) );
        }

        /// <summary>   Sets design time only. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetDesignTimeOnly( bool value )
        {
            this.SetAttribute( new DesignOnlyAttribute( value ) );
        }

        /// <summary>   Sets is browsable. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetIsBrowsable( bool value )
        {
            this.SetAttribute( new BrowsableAttribute( value ) );
        }

        /// <summary>   Sets is localizable. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetIsLocalizable( bool value )
        {
            this.SetAttribute( new LocalizableAttribute( value ) );
        }

        /// <summary>   Sets is read only. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetIsReadOnly( bool value )
        {
            this.SetAttribute( new ReadOnlyAttribute( value ) );
        }

        /// <summary>   Sets converter type. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetConverterType( Type value )
        {
            this._ConverterTypeName = value?.AssemblyQualifiedName;
            if ( this._ConverterTypeName != null )
            {
                this.SetAttribute( new TypeConverterAttribute( value ) );
            }
            else
            {
                this.SetAttribute( TypeConverterAttribute.Default );
            }
            this._Converter = null;
        }

        /// <summary>   Sets an attribute. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="value">    The new value. </param>
        public void SetAttribute( Attribute value )
        {
            if ( value == null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }
            if ( this._Attributes == null )
            {
                this._Attributes = new List<Attribute>();
            }
            this._Attributes.Add( value );
        }

        /// <summary>   Sets the attributes. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="values">   A variable-length parameters list containing values. </param>
        public void SetAttributes( params Attribute[] values )
        {
            foreach ( Attribute value in values )
            {
                this.SetAttribute( value );
            }
        }

        /// <summary>   Sets component type. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="value">    The new value. </param>
        public void SetComponentType( Type value )
        {
            this._ComponentType = value;
        }

        /// <summary>
        /// When overridden in a derived class, gets the current value of the property on a component.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component with the property for which to retrieve the value. </param>
        /// <returns>   The value of a property for a given component. </returns>
        public override object GetValue( object component )
        {
            return this.GetValueHandler != null
                ? this.GetValueHandler( component )
                : this._BaseDescriptor?.GetValue( component );
        }

        /// <summary>
        /// When overridden in a derived class, sets the value of the component to a different value.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component with the property value that is to be set. </param>
        /// <param name="value">        The new value. </param>
        public override void SetValue( object component, object value )
        {
            if ( this.SetValueHandler != null )
            {
                this.SetValueHandler( component, value );
                this.OnValueChanged( component, EventArgs.Empty );
            }
            else if ( this._BaseDescriptor != null )
            {
                this._BaseDescriptor.SetValue( component, value );
                this.OnValueChanged( component, EventArgs.Empty );
            }
        }

        /// <summary>
        /// When overridden in a derived class, returns whether resetting an object changes its value.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component to test for reset capability. </param>
        /// <returns>
        /// <see langword="true" /> if resetting the component changes its value; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool CanResetValue( object component )
        {
            return this.CanResetValueHandler != null
                ? this.CanResetValueHandler( component )
                : this._BaseDescriptor != null
                ? this._BaseDescriptor.CanResetValue( component )
                : this.Attributes[typeof( DefaultValueAttribute )] != null;
        }

        /// <summary>
        /// When overridden in a derived class, resets the value for this property of the component to
        /// the default value.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component with the property value that is to be reset to the
        ///                             default value. </param>
        public override void ResetValue( object component )
        {
            if ( this.ResetValueHandler != null )
            {
                this.ResetValueHandler( component );
            }
            else if ( this._BaseDescriptor != null )
            {
                this._BaseDescriptor.ResetValue( component );
            }
            else
            {
                if ( this.Attributes[typeof( DefaultValueAttribute )] is DefaultValueAttribute attribute )
                {
                    this.SetValue( component, attribute.Value );
                }
            }
        }

        /// <summary>
        /// When overridden in a derived class, determines a value indicating whether the value of this
        /// property needs to be persisted.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component with the property to be examined for persistence. </param>
        /// <returns>
        /// <see langword="true" /> if the property should be persisted; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool ShouldSerializeValue( object component )
        {
            return this.ShouldSerializeValueHandler != null
                ? this.ShouldSerializeValueHandler( component )
                : this._BaseDescriptor != null
                ? this._BaseDescriptor.ShouldSerializeValue( component )
                : this.Attributes[typeof( DefaultValueAttribute )] is DefaultValueAttribute attribute && !Object.Equals( this.GetValue( component ), attribute.Value );
        }

        /// <summary>
        /// Returns a <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> for a given
        /// object using a specified array of attributes as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="instance"> A component to get the properties for. </param>
        /// <param name="filter">   An array of type <see cref="T:System.Attribute" /> to use as a
        ///                         filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> with the properties
        /// that match the specified attributes for the specified component.
        /// </returns>
        public override PropertyDescriptorCollection GetChildProperties( object instance, Attribute[] filter )
        {
            return this.GetChildPropertiesHandler != null
                ? this.GetChildPropertiesHandler( instance, filter )
                : this._BaseDescriptor != null
                ? this._BaseDescriptor.GetChildProperties( instance, filter )
                : base.GetChildProperties( instance, filter );
        }

        /// <summary>
        /// Gets the hash code for the name of the member, as specified in
        /// <see cref="M:System.String.GetHashCode" />.
        /// </summary>
        /// <value> The hash code for the name of the member. </value>
        protected override int NameHashCode => this._Name != null ? this._Name.GetHashCode() : base.NameHashCode;

#if NOT_USED
		private static Attribute[] BuildAttributes(string displayName, string category, string description, bool isReadOnly)
		{
			List<Attribute> attributes = new List<Attribute>();
			if (displayName != null && displayName != DisplayNameAttribute.Default.DisplayName)
			{
				attributes.Add(new DisplayNameAttribute(displayName));
			}
			if (category != null && category != CategoryAttribute.Default.Category)
			{
				attributes.Add(new CategoryAttribute(category));
			}
			if (description != null && description != DescriptionAttribute.Default.Description)
			{
				attributes.Add(new DescriptionAttribute(description));
			}
			if (isReadOnly != ReadOnlyAttribute.Default.IsReadOnly)
			{
				attributes.Add(new ReadOnlyAttribute(isReadOnly));
			}
			return attributes.ToArray();
		}
#endif

        /// <summary>   Filter attributes. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   The attributes. </param>
        /// <returns>   An Attribute[]. </returns>
        private static Attribute[] FilterAttributes( Attribute[] attributes )
        {
            Dictionary<object, Attribute> dictionary = new();
            foreach ( Attribute attribute in attributes )
            {
                if ( !attribute.IsDefaultAttribute() )
                {
                    dictionary.Add( attribute.TypeId, attribute );
                }
            }
            Attribute[] newAttributes = new Attribute[dictionary.Values.Count];
            dictionary.Values.CopyTo( newAttributes, 0 );
            return newAttributes;
        }

        /// <summary>   The name. </summary>
        private string _Name;

        /// <summary>   The category. </summary>
        private string _Category;

        /// <summary>   The description. </summary>
        private string _Description;

        /// <summary>   Type of the property. </summary>
        private Type _PropertyType;

        /// <summary>   Name of the converter type. </summary>
        private string _ConverterTypeName;

        /// <summary>   The converter. </summary>
        private TypeConverter _Converter;

        /// <summary>   The attributes. </summary>
        private List<Attribute> _Attributes;

        /// <summary>   Type of the component. </summary>
        private Type _ComponentType;

        /// <summary>   Information describing the base. </summary>
        private readonly PropertyDescriptor _BaseDescriptor;
    }

    /// <summary>   Handler, called when the get value. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="component">    The component. </param>
    /// <returns>   An object. </returns>
    internal delegate object GetValueHandler( object component );

    /// <summary>   Handler, called when the set value. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="component">    The component. </param>
    /// <param name="value">        The value. </param>
    internal delegate void SetValueHandler( object component, object value );

    /// <summary>   Determine if we can reset value handler. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="component">    The component. </param>
    /// <returns>   A bool. </returns>
    internal delegate bool CanResetValueHandler( object component );

    /// <summary>   Resets the value handler described by component. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="component">    The component. </param>
    internal delegate void ResetValueHandler( object component );

    /// <summary>   Determine if we should serialize value handler. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="component">    The component. </param>
    /// <returns>   A bool. </returns>
    internal delegate bool ShouldSerializeValueHandler( object component );

    /// <summary>   Gets a child properties handler. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    /// <param name="instance"> The instance. </param>
    /// <param name="filter">   Specifies the filter. </param>
    /// <returns>   A PropertyDescriptorCollection. </returns>
    internal delegate PropertyDescriptorCollection GetChildPropertiesHandler( object instance, Attribute[] filter );
}
