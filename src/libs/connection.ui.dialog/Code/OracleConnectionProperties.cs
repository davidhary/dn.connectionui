﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   An oracle connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OracleConnectionProperties : AdoDotNetConnectionProperties
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OracleConnectionProperties()
            : base( "System.Data.OracleClient" )
        {
            this.LocalReset();
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => !(this.ConnectionStringBuilder["Data Source"] is string) ||
                    (this.ConnectionStringBuilder["Data Source"] as string).Length == 0
                    ? false
                    : ( bool ) this.ConnectionStringBuilder["Integrated Security"] ||
                    this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0;

        /// <summary>   Converts this object to a test string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   This object as a string. </returns>
        protected override string ToTestString()
        {
            bool savedPooling = ( bool ) this.ConnectionStringBuilder["Pooling"];
            bool wasDefault = !this.ConnectionStringBuilder.ShouldSerialize( "Pooling" );
            this.ConnectionStringBuilder["Pooling"] = false;
            string testString = this.ConnectionStringBuilder.ConnectionString;
            this.ConnectionStringBuilder["Pooling"] = savedPooling;
            if ( wasDefault )
            {
                _ = this.ConnectionStringBuilder.Remove( "Pooling" );
            }
            return testString;
        }

        /// <summary>   Local reset. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void LocalReset()
        {
            // We always start with unicode turned on
            this["Unicode"] = true;
        }

    }
}
