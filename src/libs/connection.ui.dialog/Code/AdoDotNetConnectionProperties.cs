//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   An ADO dot net connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class AdoDotNetConnectionProperties : IDataConnectionProperties, ICustomTypeDescriptor
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="providerName"> (Immutable) name of the provider. </param>
        public AdoDotNetConnectionProperties( string providerName )
        {
            Debug.Assert( providerName != null );
            this._ProviderName = providerName;

            // Create an underlying connection string builder object
            DbProviderFactory factory = DbProviderFactories.GetFactory( providerName );
            Debug.Assert( factory != null );
            this.ConnectionStringBuilder = factory.CreateConnectionStringBuilder();
            Debug.Assert( this.ConnectionStringBuilder != null );
            this.ConnectionStringBuilder.BrowsableConnectionString = false;
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public virtual void Reset()
        {
            this.ConnectionStringBuilder.Clear();
            this.OnPropertyChanged( EventArgs.Empty );
        }

        /// <summary>   Parses. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="s">    The string. </param>
        public virtual void Parse( string s )
        {
            this.ConnectionStringBuilder.ConnectionString = s;
            this.OnPropertyChanged( EventArgs.Empty );
        }

        /// <summary>   Gets a value indicating whether this object is extensible. </summary>
        /// <value> True if this object is extensible, false if not. </value>
        public virtual bool IsExtensible => !this.ConnectionStringBuilder.IsFixedSize;

        /// <summary>   Adds propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public virtual void Add( string propertyName )
        {
            if ( !this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                this.ConnectionStringBuilder.Add( propertyName, String.Empty );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        /// <summary>   Query if this object contains the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        /// <returns>   True if the object is in this collection, false if not. </returns>
        public virtual bool Contains( string propertyName )
        {
            return this.ConnectionStringBuilder.ContainsKey( propertyName );
        }

        /// <summary>
        /// Indexer to get or set items within this collection using array index syntax.
        /// </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="propertyName"> Name of the property. </param>
        /// <returns>   The indexed item. </returns>
        public virtual object this[string propertyName]
        {
            get {
                // Property name must not be null
                if ( propertyName == null )
                {
                    throw new ArgumentNullException( nameof( propertyName ) );
                }

                // If property doesn't exist, just return null
                if ( !this.ConnectionStringBuilder.TryGetValue( propertyName, out _ ) )
                {
                    return null;
                }

                // If property value has been set, return this value
                if ( this.ConnectionStringBuilder.ShouldSerialize( propertyName ) )
                {
                    return this.ConnectionStringBuilder[propertyName];
                }

                // Get the property's default value (if any)
                object val = this.ConnectionStringBuilder[propertyName];

                // If a default value exists, return it
                if ( val != null )
                {
                    return val;
                }

                // No value has been set and no default value exists, so return DBNull.Value
                return DBNull.Value;
            }
            set {
                // Property name must not be null
                if ( propertyName == null )
                {
                    throw new ArgumentNullException( nameof( propertyName ) );
                }

                // Remove the value
                _ = this.ConnectionStringBuilder.Remove( propertyName );

                // Handle cases where value is DBNull.Value
                if ( value == DBNull.Value )
                {
                    // Leave the property in the reset state
                    this.OnPropertyChanged( EventArgs.Empty );
                    return;
                }

                // Get the property's default value (if any)
                _ = this.ConnectionStringBuilder.TryGetValue( propertyName, out object val );

                // Set the value
                this.ConnectionStringBuilder[propertyName] = value;

                // If the value is equal to the default, remove it again
                if ( Object.Equals( val, value ) )
                {
                    _ = this.ConnectionStringBuilder.Remove( propertyName );
                }

                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        /// <summary>   Removes the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public virtual void Remove( string propertyName )
        {
            if ( this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                _ = this.ConnectionStringBuilder.Remove( propertyName );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        /// <summary>   Event queue for all listeners interested in PropertyChanged events. </summary>
        public event EventHandler PropertyChanged;

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public virtual void Reset( string propertyName )
        {
            if ( this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                _ = this.ConnectionStringBuilder.Remove( propertyName );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public virtual bool IsComplete => true;

        /// <summary>   Tests this object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        public virtual void Test()
        {
            string testString = this.ToTestString();

            // If the connection string is empty, don't even bother testing
            if ( testString == null || testString.Length == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.AdoDotNetConnectionProperties_NoProperties );
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory( this._ProviderName );
            Debug.Assert( factory != null );
            // Create a connection object
            DbConnection connection = factory.CreateConnection();
            Debug.Assert( connection != null );

            // Try to open it
            try
            {
                connection.ConnectionString = testString;
                connection.Open();
                this.Inspect( connection );
            }
            finally
            {
                connection.Dispose();
            }
        }

        /// <summary>   Returns a string that represents the current object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.ToFullString();
        }

        /// <summary>   Converts this object to a full string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   This object as a string. </returns>
        public virtual string ToFullString()
        {
            return this.ConnectionStringBuilder.ConnectionString;
        }

        /// <summary>   Converts this object to a display string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   This object as a string. </returns>
        public virtual string ToDisplayString()
        {
            PropertyDescriptorCollection sensitiveProperties = this.GetProperties( new Attribute[] { PasswordPropertyTextAttribute.Yes } );
            List<KeyValuePair<string, object>> savedValues = new();
            foreach ( PropertyDescriptor sensitiveProperty in sensitiveProperties )
            {
                string propertyName = sensitiveProperty.DisplayName;
                if ( this.ConnectionStringBuilder.ShouldSerialize( propertyName ) )
                {
                    savedValues.Add( new KeyValuePair<string, object>( propertyName, this.ConnectionStringBuilder[propertyName] ) );
                    _ = this.ConnectionStringBuilder.Remove( propertyName );
                }
            }
            try
            {
                return this.ConnectionStringBuilder.ConnectionString;
            }
            finally
            {
                foreach ( KeyValuePair<string, object> savedValue in savedValues )
                {
                    if ( savedValue.Value != null )
                    {
                        this.ConnectionStringBuilder[savedValue.Key] = savedValue.Value;
                    }
                }
            }
        }

        /// <summary>   Gets the connection string builder. </summary>
        /// <value> The connection string builder. </value>
        public DbConnectionStringBuilder ConnectionStringBuilder { get; }

        /// <summary>   Gets the default property. </summary>
        /// <value> The default property. </value>
        protected virtual PropertyDescriptor DefaultProperty => TypeDescriptor.GetDefaultProperty( this.ConnectionStringBuilder, true );

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        protected virtual PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            return TypeDescriptor.GetProperties( this.ConnectionStringBuilder, attributes );
        }

        /// <summary>   Raises the property changed event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnPropertyChanged( EventArgs e )
        {
            if ( PropertyChanged != null )
            {
                PropertyChanged( this, e );
            }
        }

        /// <summary>   Converts this object to a test string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   This object as a string. </returns>
        protected virtual string ToTestString()
        {
            return this.ConnectionStringBuilder.ConnectionString;
        }

        /// <summary>   Inspects the given connection. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="connection">   The connection. </param>
        protected virtual void Inspect( DbConnection connection )
        {
        }

        #region ICustomTypeDescriptor implementation

        /// <summary>   Returns the class name of this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// The class name of the object, or <see langword="null" /> if the class does not have a name.
        /// </returns>
        string ICustomTypeDescriptor.GetClassName()
        {
            return TypeDescriptor.GetClassName( this.ConnectionStringBuilder, true );
        }

        /// <summary>   Returns the name of this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// The name of the object, or <see langword="null" /> if the object does not have a name.
        /// </returns>
        string ICustomTypeDescriptor.GetComponentName()
        {
            return TypeDescriptor.GetComponentName( this.ConnectionStringBuilder, true );
        }

        /// <summary>
        /// Returns a collection of custom attributes for this instance of a component.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.AttributeCollection" /> containing the attributes for
        /// this object.
        /// </returns>
        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return TypeDescriptor.GetAttributes( this.ConnectionStringBuilder, true );
        }

        /// <summary>   Returns an editor of the specified type for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="editorBaseType">   A <see cref="T:System.Type" /> that represents the editor for
        ///                                 this object. </param>
        /// <returns>
        /// An <see cref="T:System.Object" /> of the specified type that is the editor for this object,
        /// or <see langword="null" /> if the editor cannot be found.
        /// </returns>
        object ICustomTypeDescriptor.GetEditor( Type editorBaseType )
        {
            return TypeDescriptor.GetEditor( this.ConnectionStringBuilder, editorBaseType, true );
        }

        /// <summary>   Returns a type converter for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.TypeConverter" /> that is the converter for this object,
        /// or <see langword="null" /> if there is no
        /// <see cref="T:System.ComponentModel.TypeConverter" /> for this object.
        /// </returns>
        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return TypeDescriptor.GetConverter( this.ConnectionStringBuilder, true );
        }

        /// <summary>   Returns the default property for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that represents the default
        /// property for this object, or <see langword="null" /> if this object does not have properties.
        /// </returns>
        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return this.DefaultProperty;
        }

        /// <summary>   Returns the properties for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// properties for this component instance.
        /// </returns>
        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return this.GetProperties( Array.Empty<Attribute>() );
        }

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties( Attribute[] attributes )
        {
            return this.GetProperties( attributes );
        }

        /// <summary>   Returns the default event for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.EventDescriptor" /> that represents the default event
        /// for this object, or <see langword="null" /> if this object does not have events.
        /// </returns>
        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent( this.ConnectionStringBuilder, true );
        }

        /// <summary>   Returns the events for this instance of a component. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.EventDescriptorCollection" /> that represents the
        /// events for this component instance.
        /// </returns>
        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return TypeDescriptor.GetEvents( this.ConnectionStringBuilder, true );
        }

        /// <summary>
        /// Returns the events for this instance of a component using the specified attribute array as a
        /// filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// An <see cref="T:System.ComponentModel.EventDescriptorCollection" /> that represents the
        /// filtered events for this component instance.
        /// </returns>
        EventDescriptorCollection ICustomTypeDescriptor.GetEvents( Attribute[] attributes )
        {
            return TypeDescriptor.GetEvents( this.ConnectionStringBuilder, attributes, true );
        }

        /// <summary>
        /// Returns an object that contains the property described by the specified property descriptor.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="pd">   A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that
        ///                     represents the property whose owner is to be found. </param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that represents the owner of the specified property.
        /// </returns>
        object ICustomTypeDescriptor.GetPropertyOwner( PropertyDescriptor pd )
        {
            return this.ConnectionStringBuilder;
        }

        #endregion

        /// <summary>   (Immutable) name of the provider. </summary>
        private readonly string _ProviderName;
    }
}
