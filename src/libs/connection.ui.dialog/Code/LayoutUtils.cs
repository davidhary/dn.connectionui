//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A layout utilities. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal sealed class LayoutUtils
    {
        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private LayoutUtils()
        {
        }

        /// <summary>   Gets preferred label height. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="label">    The label. </param>
        /// <returns>   The preferred label height. </returns>
        public static int GetPreferredLabelHeight( Label label )
        {
            return GetPreferredLabelHeight( label, label.Width );
        }

        /// <summary>   Gets preferred label height. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="label">            The label. </param>
        /// <param name="requiredWidth">    Width of the required. </param>
        /// <returns>   The preferred label height. </returns>
        public static int GetPreferredLabelHeight( Label label, int requiredWidth )
        {
            return GetPreferredHeight( label, label.UseCompatibleTextRendering, requiredWidth );
        }

        /// <summary>   Gets preferred check box height. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="checkBox"> The check box control. </param>
        /// <returns>   The preferred check box height. </returns>
        public static int GetPreferredCheckBoxHeight( CheckBox checkBox )
        {
            return GetPreferredHeight( checkBox, checkBox.UseCompatibleTextRendering, checkBox.Width );
        }

        /// <summary>   Mirror control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="c">    A Control to process. </param>
        public static void MirrorControl( Control c )
        {
            c.Left =
                c.Parent.Right -
                c.Parent.Padding.Left -
                c.Margin.Left -
                c.Width;
            if ( (c.Anchor & AnchorStyles.Left) == 0 ||
                (c.Anchor & AnchorStyles.Right) == 0 )
            {
                c.Anchor &= ~AnchorStyles.Left;
                c.Anchor |= AnchorStyles.Right;
            }
        }

        /// <summary>   Mirror control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="c">        A Control to process. </param>
        /// <param name="pivot">    The pivot. </param>
        public static void MirrorControl( Control c, Control pivot )
        {
            c.Left = pivot.Right - c.Width;
            if ( (c.Anchor & AnchorStyles.Left) == 0 ||
                (c.Anchor & AnchorStyles.Right) == 0 )
            {
                c.Anchor &= ~AnchorStyles.Left;
                c.Anchor |= AnchorStyles.Right;
            }
        }

        /// <summary>   Unmirror control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="c">    A Control to process. </param>
        public static void UnmirrorControl( Control c )
        {
            c.Left =
                c.Parent.Left +
                c.Parent.Padding.Left +
                c.Margin.Left;
            if ( (c.Anchor & AnchorStyles.Left) == 0 ||
                (c.Anchor & AnchorStyles.Right) == 0 )
            {
                c.Anchor &= ~AnchorStyles.Right;
                c.Anchor |= AnchorStyles.Left;
            }
        }

        /// <summary>   Unmirror control. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="c">        A Control to process. </param>
        /// <param name="pivot">    The pivot. </param>
        public static void UnmirrorControl( Control c, Control pivot )
        {
            c.Left = pivot.Left;
            if ( (c.Anchor & AnchorStyles.Left) == 0 ||
                (c.Anchor & AnchorStyles.Right) == 0 )
            {
                c.Anchor &= ~AnchorStyles.Right;
                c.Anchor |= AnchorStyles.Left;
            }
        }

        /// <summary>   Gets preferred height. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="c">                            A Control to process. </param>
        /// <param name="useCompatibleTextRendering">   True to use compatible text rendering. </param>
        /// <param name="requiredWidth">                Width of the required. </param>
        /// <returns>   The preferred height. </returns>
        private static int GetPreferredHeight( Control c, bool useCompatibleTextRendering, int requiredWidth )
        {
            using ( Graphics g = Graphics.FromHwnd( c.Handle ) )
            {
                return useCompatibleTextRendering
                    ? g.MeasureString( c.Text, c.Font, c.Width ).ToSize().Height
                    : TextRenderer.MeasureText(
                        g,
                        c.Text,
                        c.Font,
                        new Size( requiredWidth, Int32.MaxValue ),
                        TextFormatFlags.WordBreak
                    ).Height;
            }
        }
    }
}
