//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A data source. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class DataSource
    {
        /// <summary>   (Immutable) filename of the microsoft SQL server file. </summary>
        public const string MicrosoftSqlServerFileName = "MicrosoftSqlServerFile";

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private DataSource()
        {
            this._DisplayName = Dialog.Resources.Strings.DataSource_UnspecifiedDisplayName;
            this.Providers = new DataProviderCollection( this );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">         The name. </param>
        /// <param name="displayName">  The name of the display. </param>
        public DataSource( string name, string displayName )
        {
            this.Name = name ?? throw new ArgumentNullException( nameof( name ) );
            this._DisplayName = displayName;
            this.Providers = new DataProviderCollection( this );
        }

        /// <summary>   Adds a standard data sources. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="dialog">   The dialog. </param>
        public static void AddStandardDataSources( DataConnectionDialog dialog )
        {
            dialog.DataSources.Add( DataSource.SqlDataSource );
            dialog.DataSources.Add( DataSource.SqlFileDataSource );
            dialog.DataSources.Add( DataSource.OracleDataSource );
            dialog.DataSources.Add( DataSource.AccessDataSource );
            dialog.DataSources.Add( DataSource.OdbcDataSource );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.SqlDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OracleDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OleDBDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OdbcDataProvider );
            dialog.DataSources.Add( dialog.UnspecifiedDataSource );
        }

        /// <summary>   Gets the SQL data source. </summary>
        /// <value> The SQL data source. </value>
        public static DataSource SqlDataSource
        {
            get {
                if ( _SqlDataSource == null )
                {
                    _SqlDataSource = new DataSource( "MicrosoftSqlServer", Dialog.Resources.Strings.DataSource_MicrosoftSqlServer );
                    _SqlDataSource.Providers.Add( DataProvider.SqlDataProvider );
                    _SqlDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                    _SqlDataSource.DefaultProvider = DataProvider.SqlDataProvider;
                }
                return _SqlDataSource;
            }
        }
        /// <summary>   The SQL data source. </summary>
        private static DataSource _SqlDataSource;

        /// <summary>   Gets the SQL file data source. </summary>
        /// <value> The SQL file data source. </value>
        public static DataSource SqlFileDataSource
        {
            get {
                if ( _SqlFileDataSource == null )
                {
                    _SqlFileDataSource = new DataSource( "MicrosoftSqlServerFile", Dialog.Resources.Strings.DataSource_MicrosoftSqlServerFile );
                    _SqlFileDataSource.Providers.Add( DataProvider.SqlDataProvider );
                }
                return _SqlFileDataSource;
            }
        }
        /// <summary>   The SQL file data source. </summary>
        private static DataSource _SqlFileDataSource;

        /// <summary>   Gets the oracle data source. </summary>
        /// <value> The oracle data source. </value>
        public static DataSource OracleDataSource
        {
            get {
                if ( _OracleDataSource == null )
                {
                    _OracleDataSource = new DataSource( "Oracle", Dialog.Resources.Strings.DataSource_Oracle );
                    _OracleDataSource.Providers.Add( DataProvider.OracleDataProvider );
                    _OracleDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                    _OracleDataSource.DefaultProvider = DataProvider.OracleDataProvider;
                }
                return _OracleDataSource;
            }
        }
        /// <summary>   The oracle data source. </summary>
        private static DataSource _OracleDataSource;

        /// <summary>   Gets the access data source. </summary>
        /// <value> The access data source. </value>
        public static DataSource AccessDataSource
        {
            get {
                if ( _AccessDataSource == null )
                {
                    _AccessDataSource = new DataSource( "MicrosoftAccess", Dialog.Resources.Strings.DataSource_MicrosoftAccess );
                    _AccessDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                }
                return _AccessDataSource;
            }
        }
        /// <summary>   The access data source. </summary>
        private static DataSource _AccessDataSource;

        /// <summary>   Gets the ODBC data source. </summary>
        /// <value> The ODBC data source. </value>
        public static DataSource OdbcDataSource
        {
            get {
                if ( _OdbcDataSource == null )
                {
                    _OdbcDataSource = new DataSource( "OdbcDsn", Dialog.Resources.Strings.DataSource_MicrosoftOdbcDsn );
                    _OdbcDataSource.Providers.Add( DataProvider.OdbcDataProvider );
                }
                return _OdbcDataSource;
            }
        }
        /// <summary>   The ODBC data source. </summary>
        private static DataSource _OdbcDataSource;

        /// <summary>   Gets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; }

        /// <summary>   Gets the name of the display. </summary>
        /// <value> The name of the display. </value>
        public string DisplayName => this._DisplayName ?? this.Name;

        /// <summary>   Gets or sets the default provider. </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The default provider. </value>
        public DataProvider DefaultProvider
        {
            get {
                switch ( this.Providers.Count )
                {
                    case 0:
                        Debug.Assert( this._DefaultProvider == null );
                        return null;
                    case 1:
                        // If there is only one data provider, it must be the default
                        IEnumerator<DataProvider> e = this.Providers.GetEnumerator();
                        _ = e.MoveNext();
                        return e.Current;
                    default:
                        return (this.Name != null) ? this._DefaultProvider : null;
                }
            }
            set {
                if ( this.Providers.Count == 1 && this._DefaultProvider != value )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataSource_CannotChangeSingleDataProvider );
                }
                if ( value != null && !this.Providers.Contains( value ) )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataSource_DataProviderNotFound );
                }
                this._DefaultProvider = value;
            }
        }

        /// <summary>   Gets the providers. </summary>
        /// <value> The providers. </value>
        public ICollection<DataProvider> Providers { get; }

        /// <summary>   Creates the unspecified. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   The new unspecified. </returns>
        internal static DataSource CreateUnspecified()
        {
            return new DataSource();
        }

        /// <summary>   Collection of data providers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class DataProviderCollection : ICollection<DataProvider>
        {
            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="source">   (Immutable) source for the. </param>
            public DataProviderCollection( DataSource source )
            {
                Debug.Assert( source != null );

                this._List = new List<DataProvider>();
                this._Source = source;
            }

            /// <summary>
            /// Gets the number of elements contained in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <value>
            /// The number of elements contained in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </value>
            public int Count => this._List.Count;

            /// <summary>
            /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
            /// is read-only.
            /// </summary>
            /// <value>
            /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
            /// read-only; otherwise, <see langword="false" />.
            /// </value>
            public bool IsReadOnly => false;

            /// <summary>
            /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
            ///                                             null. </exception>
            /// <param name="item"> The object to add to the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            public void Add( DataProvider item )
            {
                if ( item == null )
                {
                    throw new ArgumentNullException( nameof( item ) );
                }
                if ( !this._List.Contains( item ) )
                {
                    this._List.Add( item );
                }
            }

            /// <summary>
            /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
            /// specific value.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="item"> The object to locate in the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            /// <returns>
            /// <see langword="true" /> if <paramref name="item" /> is found in the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
            /// <see langword="false" />.
            /// </returns>
            public bool Contains( DataProvider item )
            {
                return this._List.Contains( item );
            }

            /// <summary>
            /// Removes the first occurrence of a specific object from the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="item"> The object to remove from the
            ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
            /// <returns>
            /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
            /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
            /// <see langword="false" />. This method also returns <see langword="false" /> if
            /// <paramref name="item" /> is not found in the original
            /// <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </returns>
            public bool Remove( DataProvider item )
            {
                bool result = this._List.Remove( item );
                if ( item == this._Source._DefaultProvider )
                {
                    this._Source._DefaultProvider = null;
                }
                return result;
            }

            /// <summary>
            /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public void Clear()
            {
                this._List.Clear();
                this._Source._DefaultProvider = null;
            }

            /// <summary>
            /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
            /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
            ///                             destination of the elements copied from
            ///                             <see cref="T:System.Collections.Generic.ICollection`1" />. The
            ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
            /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
            ///                             copying begins. </param>
            public void CopyTo( DataProvider[] array, int arrayIndex )
            {
                this._List.CopyTo( array, arrayIndex );
            }

            /// <summary>   Returns an enumerator that iterates through the collection. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
            public IEnumerator<DataProvider> GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            /// <summary>   Returns an enumerator that iterates through a collection. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through
            /// the collection.
            /// </returns>
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            /// <summary>   (Immutable) the list. </summary>
            private readonly ICollection<DataProvider> _List;
            /// <summary>   (Immutable) source for the. </summary>
            private readonly DataSource _Source;
        }

        /// <summary>   (Immutable) name of the display. </summary>
        private readonly string _DisplayName;
        /// <summary>   The default provider. </summary>
        private DataProvider _DefaultProvider;
    }
}
