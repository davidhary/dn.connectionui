﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Windows.Forms;
using System.Globalization;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A RTL aware message box. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal sealed class RTLAwareMessageBox
    {
        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private RTLAwareMessageBox()
        {
        }

        /// <summary>   Shows. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="text">     The text. </param>
        /// <param name="icon">     The icon. </param>
        /// <returns>   A DialogResult. </returns>
        public static DialogResult Show( string caption, string text, MessageBoxIcon icon )
        {
            MessageBoxOptions options = 0;
            if ( CultureInfo.CurrentUICulture.TextInfo.IsRightToLeft )
            {
                options = MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading;
            }
            return MessageBox.Show( text, caption, MessageBoxButtons.OK, icon, MessageBoxDefaultButton.Button1, options );
        }
    }
}
