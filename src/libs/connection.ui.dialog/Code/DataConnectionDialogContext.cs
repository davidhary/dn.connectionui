﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Values that represent data connection dialog contexts. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public enum DataConnectionDialogContext
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None = 0x00000000,

        /// <summary>   An enum constant representing the source option. </summary>
        Source = 0x01000000,
        /// <summary>   An enum constant representing the source list box option. </summary>
        SourceListBox = 0x01000001,
        /// <summary>   An enum constant representing the source provider combo box option. </summary>
        SourceProviderComboBox = 0x01000002,
        /// <summary>   An enum constant representing the source ok button option. </summary>
        SourceOkButton = 0x01000003,
        /// <summary>   An enum constant representing the source cancel button option. </summary>
        SourceCancelButton = 0x01000004,

        /// <summary>   An enum constant representing the main option. </summary>
        Main = 0x02000000,
        /// <summary>   An enum constant representing the main data source text box option. </summary>
        MainDataSourceTextBox = 0x02100001,
        /// <summary>   An enum constant representing the main change data source button option. </summary>
        MainChangeDataSourceButton = 0x02100002,
        /// <summary>
        /// An enum constant representing the main connection User interface control option.
        /// </summary>
        MainConnectionUIControl = 0x02200000,
        /// <summary>
        /// An enum constant representing the main SQL connection User interface control option.
        /// </summary>
        MainSqlConnectionUIControl = 0x02200001,
        /// <summary>
        /// An enum constant representing the main SQL file connection User interface control option.
        /// </summary>
        MainSqlFileConnectionUIControl = 0x02200002,
        /// <summary>
        /// An enum constant representing the main oracle connection User interface control option.
        /// </summary>
        MainOracleConnectionUIControl = 0x02200003,
        /// <summary>
        /// An enum constant representing the main access connection User interface control option.
        /// </summary>
        MainAccessConnectionUIControl = 0x02200004,
        /// <summary>
        /// An enum constant representing the main OLE Database connection User interface control option.
        /// </summary>
        MainOleDBConnectionUIControl = 0x02200005,
        /// <summary>
        /// An enum constant representing the main ODBC connection User interface control option.
        /// </summary>
        MainOdbcConnectionUIControl = 0x02200006,
        /// <summary>
        /// An enum constant representing the main generic connection User interface control option.
        /// </summary>
        MainGenericConnectionUIControl = 0x022FFFFF,
        /// <summary>   An enum constant representing the main advanced button option. </summary>
        MainAdvancedButton = 0x02400000,
        /// <summary>   An enum constant representing the main test connection button option. </summary>
        MainTestConnectionButton = 0x02800001,
        /// <summary>   An enum constant representing the main accept button option. </summary>
        MainAcceptButton = 0x0280000E,
        /// <summary>   An enum constant representing the main cancel button option. </summary>
        MainCancelButton = 0x0280000F,

        /// <summary>   An enum constant representing the advanced option. </summary>
        Advanced = 0x04000000,
        /// <summary>   An enum constant representing the advanced property grid option. </summary>
        AdvancedPropertyGrid = 0x04000001,
        /// <summary>   An enum constant representing the advanced text box option. </summary>
        AdvancedTextBox = 0x04000002,
        /// <summary>   An enum constant representing the advanced ok button option. </summary>
        AdvancedOkButton = 0x04000003,
        /// <summary>   An enum constant representing the advanced cancel button option. </summary>
        AdvancedCancelButton = 0x04000004,

        /// <summary>   An enum constant representing the add property option. </summary>
        AddProperty = 0x08000000,
        /// <summary>   An enum constant representing the add property text box option. </summary>
        AddPropertyTextBox = 0x08000001,
        /// <summary>   An enum constant representing the add property ok button option. </summary>
        AddPropertyOkButton = 0x0800000E,
        /// <summary>   An enum constant representing the add property cancel button option. </summary>
        AddPropertyCancelButton = 0x0800000F
    }
}
