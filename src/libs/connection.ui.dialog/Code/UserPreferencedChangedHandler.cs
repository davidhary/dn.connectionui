//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Windows.Forms.Design;
using Microsoft.Win32;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   A user preference changed handler. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal sealed class UserPreferenceChangedHandler : IComponent
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="form"> The form. </param>
        public UserPreferenceChangedHandler( Form form )
        {
            Debug.Assert( form != null );
            SystemEvents.UserPreferenceChanged += new UserPreferenceChangedEventHandler( this.HandleUserPreferenceChanged );
            this._form = form;
        }

        /// <summary>   Finalizer. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        ~UserPreferenceChangedHandler()
        {
            this.Dispose( false );
        }

        /// <summary>
        /// Gets or sets the <see cref="T:System.ComponentModel.ISite" /> associated with the
        /// <see cref="T:System.ComponentModel.IComponent" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.ComponentModel.ISite" /> object associated with the component; or
        /// <see langword="null" />, if the component does not have a site.
        /// </value>
        public ISite Site
        {
            get => this._form.Site;
            set {
                // This shouldn't be called
            }
        }

        /// <summary>
        /// Represents the method that handles the
        /// <see cref="E:System.ComponentModel.IComponent.Disposed" /> event of a component.
        /// </summary>
        public event EventHandler Disposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>   Handles the user preference changed. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        User preference changed event information. </param>
        private void HandleUserPreferenceChanged( object sender, UserPreferenceChangedEventArgs e )
        {
            // Need to update the font
            IUIService uiService = (this._form.Site != null) ? this._form.Site.GetService( typeof( IUIService ) ) as IUIService : null;
            if ( uiService != null )
            {
                if ( uiService.Styles["DialogFont"] is Font newFont )
                {
                    this._form.Font = newFont;
                }
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// Microsoft.Data.ConnectionUI.UserPreferenceChangedHandler and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            if ( disposing )
            {
                SystemEvents.UserPreferenceChanged -= new UserPreferenceChangedEventHandler( this.HandleUserPreferenceChanged );
                if ( Disposed != null )
                {
                    Disposed( this, EventArgs.Empty );
                }
            }
        }

        /// <summary>   The form. </summary>
        private Form _form;
    }
}
