//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   An OLE database connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OleDBConnectionProperties : AdoDotNetConnectionProperties
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OleDBConnectionProperties()
            : base( "System.Data.OleDb" )
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the provider selection is disabled.
        /// </summary>
        /// <value> True if disable provider selection, false if not. </value>
        public bool DisableProviderSelection { get; set; }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => this.ConnectionStringBuilder["Provider"] is string &&
                    (this.ConnectionStringBuilder["Provider"] as string).Length != 0;

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( this.DisableProviderSelection )
            {
                PropertyDescriptor providerDescriptor = descriptors.Find( "Provider", true );
                if ( providerDescriptor != null )
                {
                    int index = descriptors.IndexOf( providerDescriptor );
                    PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                    descriptors.CopyTo( descriptorArray, 0 );
                    descriptorArray[index] = new DynamicPropertyDescriptor( providerDescriptor, ReadOnlyAttribute.Yes );
                    (descriptorArray[index] as DynamicPropertyDescriptor).CanResetValueHandler = new CanResetValueHandler( this.CanResetProvider );
                    descriptors = new PropertyDescriptorCollection( descriptorArray, true );
                }
            }
            return descriptors;
        }

        /// <summary>   Gets the registered OLE DB providers as an array of ProgIDs. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   The registered providers. </returns>
        public static List<string> GetRegisteredProviders()
        {
            // Get the sources rowset for the root OLE DB enumerator
            Type rootEnumeratorType = Type.GetTypeFromCLSID( NativeMethods.CLSID_OLEDB_ENUMERATOR );
            OleDbDataReader dr = OleDbEnumerator.GetEnumerator( rootEnumeratorType );

            // Read the CLSIDs of each data source (not binders or enumerators)
            Dictionary<string, string> sources = new(); // avoids duplicate entries
            using ( dr )
            {
                while ( dr.Read() )
                {
                    int type = ( int ) dr["SOURCES_TYPE"];
                    if ( type == NativeMethods.DBSOURCETYPE_DATASOURCE_TDP ||
                        type == NativeMethods.DBSOURCETYPE_DATASOURCE_MDP )
                    {
                        sources[dr["SOURCES_CLSID"] as string] = null;
                    }
                }
            } // reader is disposed here

            // Get the ProgID for each data source
            List<string> sourceProgIds = new( sources.Count );
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey( "CLSID" );
            using ( key )
            {
                foreach ( KeyValuePair<string, string> source in sources )
                {
                    Microsoft.Win32.RegistryKey subKey = key.OpenSubKey( source.Key + "\\ProgID" );
                    // if this subkey does not exist, ignore it.
                    if ( subKey != null )
                    {
                        using ( subKey )
                        {
                            sourceProgIds.Add( subKey.GetValue( null ) as string );
                        } // subKey is disposed here
                    }
                }
            } // key is disposed here

            // Sort the prog ID array by name
            sourceProgIds.Sort();

            // The OLE DB provider for ODBC is not supported by the OLE DB .NET provider, so remove it
            while ( sourceProgIds.Contains( "MSDASQL.1" ) )
            {
                _ = sourceProgIds.Remove( "MSDASQL.1" );
            }

            return sourceProgIds;
        }

        /// <summary>   Determine if we can reset provider. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="component">    The component. </param>
        /// <returns>   True if we can reset provider, false if not. </returns>
        private bool CanResetProvider( object component )
        {
            return false;
        }
    }

    /// <summary>   An OLE database specialized connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OleDBSpecializedConnectionProperties : OleDBConnectionProperties
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="provider"> (Immutable) the provider. </param>
        public OleDBSpecializedConnectionProperties( string provider )
        {
            Debug.Assert( provider != null );
            this._Provider = provider;
            this.LocalReset();
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            bool disableProviderSelection = this.DisableProviderSelection;
            try
            {
                this.DisableProviderSelection = true;
                return base.GetProperties( attributes );
            }
            finally
            {
                this.DisableProviderSelection = disableProviderSelection;
            }
        }

        /// <summary>   Local reset. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void LocalReset()
        {
            // Initialize with the selected provider
            this["Provider"] = this._Provider;
        }

        /// <summary>   (Immutable) the provider. </summary>
        private readonly string _Provider;
    }

    /// <summary>   An OLE database SQL connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OleDBSqlConnectionProperties : OleDBSpecializedConnectionProperties
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OleDBSqlConnectionProperties()
            : base( "SQLOLEDB" )
        {
            this.LocalReset();
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => base.IsComplete
                    && (this.ConnectionStringBuilder["Data Source"] is string &&
                        (this.ConnectionStringBuilder["Data Source"] as string).Length != 0
                    && (this.ConnectionStringBuilder["Integrated Security"] != null &&
                    this.ConnectionStringBuilder["Integrated Security"].ToString().Equals( "SSPI", StringComparison.OrdinalIgnoreCase ) ||
                    this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0));

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( SqlNativeClientRegistered )
            {
                if ( descriptors.Find( "Provider", true ) is DynamicPropertyDescriptor providerDescriptor )
                {
                    if ( !this.DisableProviderSelection )
                    {
                        providerDescriptor.SetIsReadOnly( false );
                    }
                    providerDescriptor.SetConverterType( typeof( SqlProviderConverter ) );
                }
            }
            return descriptors;
        }

        /// <summary>   Local reset. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private void LocalReset()
        {
            // We always start with integrated security turned on
            this["Integrated Security"] = "SSPI";
        }

        /// <summary>   Gets the SQL native client providers. </summary>
        /// <value> The SQL native client providers. </value>
        public static List<string> SqlNativeClientProviders
        {
            get {
                if ( _SqlNativeClientProviders == null )
                {
                    _SqlNativeClientProviders = new List<string>();

                    List<string> providers = GetRegisteredProviders();
                    Debug.Assert( providers != null, "provider list is null" );
                    foreach ( string provider in providers )
                    {
                        if ( provider.StartsWith( "SQLNCLI" ) )
                        {
                            int idx = provider.IndexOf( "." );
                            if ( idx > 0 )
                            {
                                _SqlNativeClientProviders.Add( provider.Substring( 0, idx ).ToUpperInvariant() );
                            }
                        }
                    }

                    _SqlNativeClientProviders.Sort();
                }

                Debug.Assert( _SqlNativeClientProviders != null, "Native Client list is null" );
                return _SqlNativeClientProviders;
            }
        }

        /// <summary>   Gets a value indicating whether the SQL native client registered. </summary>
        /// <value> True if SQL native client registered, false if not. </value>
        private static bool SqlNativeClientRegistered
        {
            get {
                if ( !_GotSqlNativeClientRegistered )
                {
                    Microsoft.Win32.RegistryKey key = null;
                    try
                    {
                        _SqlNativeClientRegistered = SqlNativeClientProviders.Count > 0;
                    }
                    finally
                    {
                        if ( key != null )
                        {
                            key.Close();
                        }
                    }
                    _GotSqlNativeClientRegistered = true;
                }
                return _SqlNativeClientRegistered;
            }
        }

        /// <summary>   True if SQL native client registered. </summary>
        private static bool _SqlNativeClientRegistered;
        /// <summary>   The SQL native client providers. </summary>
        private static List<string> _SqlNativeClientProviders = null;
        /// <summary>   True if got SQL native client registered. </summary>
        private static bool _GotSqlNativeClientRegistered;

        /// <summary>   A SQL provider converter. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class SqlProviderConverter : StringConverter
        {
            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public SqlProviderConverter()
            {
            }

            /// <summary>
            /// Returns whether this object supports a standard set of values that can be picked from a list,
            /// using the specified context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context. </param>
            /// <returns>
            /// <see langword="true" /> if
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> should be called to
            /// find a common set of values the object supports; otherwise, <see langword="false" />.
            /// </returns>
            public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
            {
                return true;
            }

            /// <summary>
            /// Returns whether the collection of standard values returned from
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> is an exclusive list
            /// of possible values, using the specified context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context. </param>
            /// <returns>
            /// <see langword="true" /> if the
            /// <see cref="T:System.ComponentModel.TypeConverter.StandardValuesCollection" /> returned from
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> is an exhaustive list
            /// of possible values; <see langword="false" /> if other values are possible.
            /// </returns>
            public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
            {
                return true;
            }

            /// <summary>
            /// Returns a collection of standard values for the data type this type converter is designed for
            /// when provided with a format context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context that can be used to extract additional
            ///                         information about the environment from which this converter is
            ///                         invoked. This parameter or properties of this parameter can be
            ///                         <see langword="null" />. </param>
            /// <returns>
            /// A <see cref="T:System.ComponentModel.TypeConverter.StandardValuesCollection" /> that holds a
            /// standard set of valid values, or <see langword="null" /> if the data type does not support a
            /// standard set of values.
            /// </returns>
            public override StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
            {
                List<string> stdCollection = new() {
                    "SQLOLEDB"
                };

                foreach ( string provider in SqlNativeClientProviders )
                {
                    stdCollection.Add( provider );
                }

                return new StandardValuesCollection( stdCollection );
            }
        }
    }

    /// <summary>   An OLE database oracle connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OleDBOracleConnectionProperties : OleDBSpecializedConnectionProperties
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OleDBOracleConnectionProperties()
            : base( "MSDAORA" )
        {
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => base.IsComplete && (this.ConnectionStringBuilder["Data Source"] is string &&
                    (this.ConnectionStringBuilder["Data Source"] as string).Length != 0
                        && this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0);
    }

    /// <summary>   An OLE database access connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OleDBAccessConnectionProperties : OleDBSpecializedConnectionProperties
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OleDBAccessConnectionProperties()
            : base( "Microsoft.Jet.OLEDB.4.0" )
        {
            this._UserChangedProvider = false;
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public override void Reset()
        {
            base.Reset();
            this._UserChangedProvider = false;
        }

        /// <summary>
        /// Indexer to get or set items within this collection using array index syntax.
        /// </summary>
        /// <param name="propertyName"> Name of the property. </param>
        /// <returns>   The indexed item. </returns>
        public override object this[string propertyName]
        {
            set {
                base[propertyName] = value;
                if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
                {
                    if ( value != null && value != DBNull.Value )
                    {
                        this.OnProviderChanged( this.ConnectionStringBuilder, EventArgs.Empty );
                    }
                    else
                    {
                        this._UserChangedProvider = false;
                    }
                }
                if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
                {
                    this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
                }
            }
        }

        /// <summary>   Removes the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public override void Remove( string propertyName )
        {
            base.Remove( propertyName );
            if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
            {
                this._UserChangedProvider = false;
            }
            if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
            {
                this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
            }
        }

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public override void Reset( string propertyName )
        {
            base.Reset( propertyName );
            if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
            {
                this._UserChangedProvider = false;
            }
            if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
            {
                this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
            }
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => base.IsComplete && this.ConnectionStringBuilder["Data Source"] is string &&
                    (this.ConnectionStringBuilder["Data Source"] as string).Length != 0;

        /// <summary>   Tests this object. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        public override void Test()
        {
            if ( this.ConnectionStringBuilder["Data Source"] is not string dataSource || dataSource.Length == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.OleDBAccessConnectionProperties_MustSpecifyDataSource );
            }
            base.Test();
        }

        /// <summary>   Converts this object to a display string. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   This object as a string. </returns>
        public override string ToDisplayString()
        {
            string savedPassword = null;
            if ( this.ConnectionStringBuilder.ContainsKey( "Jet OLEDB:Database Password" ) &&
                this.ConnectionStringBuilder.ShouldSerialize( "Jet OLEDB:Database Password" ) )
            {
                savedPassword = this.ConnectionStringBuilder["Jet OLEDB:Database Password"] as string;
                _ = this.ConnectionStringBuilder.Remove( "Jet OLEDB:Database Password" );
            }
            string displayString = base.ToDisplayString();
            if ( savedPassword != null )
            {
                this.ConnectionStringBuilder["Jet OLEDB:Database Password"] = savedPassword;
            }
            return displayString;
        }

        /// <summary>
        /// Returns the properties for this instance of a component using the attribute array as a filter.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="attributes">   An array of type <see cref="T:System.Attribute" /> that is used
        ///                             as a filter. </param>
        /// <returns>
        /// A <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the
        /// filtered properties for this component instance.
        /// </returns>
        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( Access12ProviderRegistered )
            {
                if ( descriptors.Find( "Provider", true ) is DynamicPropertyDescriptor providerDescriptor )
                {
                    if ( !this.DisableProviderSelection )
                    {
                        providerDescriptor.SetIsReadOnly( false );
                    }
                    providerDescriptor.SetConverterType( typeof( JetProviderConverter ) );
                    providerDescriptor.AddValueChanged( this.ConnectionStringBuilder, new EventHandler( this.OnProviderChanged ) );
                }
                PropertyDescriptor dataSourceDescriptor = descriptors.Find( "DataSource", true );
                if ( dataSourceDescriptor != null )
                {
                    int index = descriptors.IndexOf( dataSourceDescriptor );
                    PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                    descriptors.CopyTo( descriptorArray, 0 );
                    descriptorArray[index] = new DynamicPropertyDescriptor( dataSourceDescriptor );
                    descriptorArray[index].AddValueChanged( this.ConnectionStringBuilder, new EventHandler( this.OnDataSourceChanged ) );
                    descriptors = new PropertyDescriptorCollection( descriptorArray, true );
                }
            }
            PropertyDescriptor passwordDescriptor = descriptors.Find( "Jet OLEDB:Database Password", true );
            if ( passwordDescriptor != null )
            {
                int index = descriptors.IndexOf( passwordDescriptor );
                PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                descriptors.CopyTo( descriptorArray, 0 );
                descriptorArray[index] = new DynamicPropertyDescriptor( passwordDescriptor, PasswordPropertyTextAttribute.Yes );
                descriptors = new PropertyDescriptorCollection( descriptorArray, true );
            }
            return descriptors;
        }

        /// <summary>   Gets a value indicating whether the access 12 provider registered. </summary>
        /// <value> True if access 12 provider registered, false if not. </value>
        private static bool Access12ProviderRegistered
        {
            get {
                if ( !_GotAccess12ProviderRegistered )
                {
                    Microsoft.Win32.RegistryKey key = null;
                    try
                    {
                        key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey( "Microsoft.ACE.OLEDB.12.0" );
                        _Access12ProviderRegistered = (key != null);
                    }
                    finally
                    {
                        if ( key != null )
                        {
                            key.Close();
                        }
                    }
                    _GotAccess12ProviderRegistered = true;
                }
                return _Access12ProviderRegistered;
            }
        }
        /// <summary>   True if access 12 provider registered. </summary>
        private static bool _Access12ProviderRegistered;
        /// <summary>   True if got access 12 provider registered. </summary>
        private static bool _GotAccess12ProviderRegistered;

        /// <summary>   A jet provider converter. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        private class JetProviderConverter : StringConverter
        {
            /// <summary>   Default constructor. </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            public JetProviderConverter()
            {
            }

            /// <summary>
            /// Returns whether this object supports a standard set of values that can be picked from a list,
            /// using the specified context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context. </param>
            /// <returns>
            /// <see langword="true" /> if
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> should be called to
            /// find a common set of values the object supports; otherwise, <see langword="false" />.
            /// </returns>
            public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
            {
                return true;
            }

            /// <summary>
            /// Returns whether the collection of standard values returned from
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> is an exclusive list
            /// of possible values, using the specified context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context. </param>
            /// <returns>
            /// <see langword="true" /> if the
            /// <see cref="T:System.ComponentModel.TypeConverter.StandardValuesCollection" /> returned from
            /// <see cref="M:System.ComponentModel.TypeConverter.GetStandardValues" /> is an exhaustive list
            /// of possible values; <see langword="false" /> if other values are possible.
            /// </returns>
            public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
            {
                return true;
            }

            /// <summary>
            /// Returns a collection of standard values for the data type this type converter is designed for
            /// when provided with a format context.
            /// </summary>
            /// <remarks>   David, 2021-06-14. </remarks>
            /// <param name="context">  An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that
            ///                         provides a format context that can be used to extract additional
            ///                         information about the environment from which this converter is
            ///                         invoked. This parameter or properties of this parameter can be
            ///                         <see langword="null" />. </param>
            /// <returns>
            /// A <see cref="T:System.ComponentModel.TypeConverter.StandardValuesCollection" /> that holds a
            /// standard set of valid values, or <see langword="null" /> if the data type does not support a
            /// standard set of values.
            /// </returns>
            public override StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
            {
                return new StandardValuesCollection( new string[] {
                    "Microsoft.Jet.OLEDB.4.0", "Microsoft.ACE.OLEDB.12.0"
                } );
            }
        }

        /// <summary>   Raises the provider changed event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnProviderChanged( object sender, EventArgs e )
        {
            if ( Access12ProviderRegistered )
            {
                this._UserChangedProvider = true;
            }
        }

        /// <summary>   Raises the data source changed event. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnDataSourceChanged( object sender, EventArgs e )
        {
            if ( Access12ProviderRegistered && !this._UserChangedProvider )
            {
                if ( this["Data Source"] is string dataSource )
                {
                    dataSource = dataSource.Trim().ToUpperInvariant();
                    base["Provider"] = dataSource.EndsWith( ".ACCDB", StringComparison.Ordinal ) ? "Microsoft.ACE.OLEDB.12.0" : "Microsoft.Jet.OLEDB.4.0";
                }
            }
        }

        /// <summary>   True to user changed provider. </summary>
        private bool _UserChangedProvider;
    }
}
