//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   An ODBC connection properties. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    public class OdbcConnectionProperties : AdoDotNetConnectionProperties
    {
        /// <summary>   The SQL native client drivers. </summary>
        private static List<string> _SqlNativeClientDrivers = null;

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public OdbcConnectionProperties()
            : base( "System.Data.Odbc" )
        {
        }

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
        public override bool IsComplete => this.ConnectionStringBuilder["DSN"] is string &&
                    (this.ConnectionStringBuilder["DSN"] as string).Length != 0 ||
                    this.ConnectionStringBuilder["DRIVER"] is string &&
                    (this.ConnectionStringBuilder["DRIVER"] as string).Length != 0;

        /// <summary>   Gets the SQL native client drivers. </summary>
        /// <value> The SQL native client drivers. </value>
        public static List<string> SqlNativeClientDrivers
        {
            get {
                if ( _SqlNativeClientDrivers == null )
                {
                    _SqlNativeClientDrivers = new List<string>();

                    List<string> driverDescList = ManagedSQLGetInstalledDrivers();
                    Debug.Assert( driverDescList != null, "driver list is null" );
                    foreach ( string driverDesc in driverDescList )
                    {
                        if ( driverDesc.Contains( "Native" ) && driverDesc.Contains( "Client" ) )
                        {
                            StringBuilder driverBuf = new( 1024 );
                            int len = NativeMethods.SQLGetPrivateProfileString( driverDesc, "Driver", "", driverBuf, driverBuf.Capacity, "ODBCINST.INI" );
                            if ( len > 0 && driverBuf.Length > 0 )
                            {
                                string driver = driverBuf.ToString();
                                int start = driver.LastIndexOf( '\\' );
                                if ( start > 0 )
                                {
                                    _SqlNativeClientDrivers.Add( driver.Substring( start + 1 ).ToUpperInvariant() );
                                }
                            }
                        }
                    }

                    _SqlNativeClientDrivers.Sort();
                }

                Debug.Assert( _SqlNativeClientDrivers != null, "Native Client list is null" );
                return _SqlNativeClientDrivers;
            }
        }

        /// <summary>   Managed SQL get installed drivers. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <returns>   A List&lt;string&gt; </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private static List<string> ManagedSQLGetInstalledDrivers()
        {
            char[] lpszBuf = new char[1024];
            int pcbBufOut = 0;
            List<string> driverList = new();

            bool succeed;
            try
            {
                succeed = NativeMethods.SQLGetInstalledDrivers( lpszBuf, lpszBuf.Length, ref pcbBufOut );

                while ( succeed && pcbBufOut > 0 &&
                    pcbBufOut == (lpszBuf.Length - 1) &&
                    lpszBuf.Length < Math.Pow( 2, 30 ) /* sanity limit */ )
                {
                    // The managed buffer needs to be bigger
                    lpszBuf = new char[lpszBuf.Length * 2];

                    succeed = NativeMethods.SQLGetInstalledDrivers( lpszBuf, lpszBuf.Length, ref pcbBufOut );
                }
            }
            catch ( Exception e )
            {
                Debug.Fail( e.ToString() );
                succeed = false;
            }

            if ( succeed )
            {
                for ( int start = 0, end = Array.IndexOf( lpszBuf, '\0', start, (pcbBufOut - 1) );
                    start < (pcbBufOut - 1);
                    start = end + 1, end = Array.IndexOf( lpszBuf, '\0', start, (pcbBufOut - 1) - end ) )
                {
                    driverList.Add( new string( lpszBuf, start, end - start ) );
                }
            }

            return driverList;
        }
    }
}
