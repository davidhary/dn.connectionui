//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Dialog for setting the add property. </summary>
    /// <remarks>   David, 2021-06-14. </remarks>
    internal partial class AddPropertyDialog : Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        public AddPropertyDialog()
        {
            this.InitializeComponent();

            // Make sure we handle a user preference change
            if ( this.components == null )
            {
                this.components = new System.ComponentModel.Container();
            }
            this.components.Add( new UserPreferenceChangedHandler( this ) );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="mainDialog">   The main dialog. </param>
        public AddPropertyDialog( DataConnectionDialog mainDialog )
            : this()
        {
            Debug.Assert( mainDialog != null );

            this._MainDialog = mainDialog;
        }

        /// <summary>   Gets the name of the property. </summary>
        /// <value> The name of the property. </value>
        public string PropertyName => this.propertyTextBox.Text;

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.FontChanged" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );

            this.propertyTextBox.Width =
                this.buttonsTableLayoutPanel.Right -
                this.propertyTextBox.Left;

            // Resize the dialog appropriately so that OK/Cancel buttons fit
            int preferredClientWidth =
                this.Padding.Left +
                this.buttonsTableLayoutPanel.Margin.Left +
                this.buttonsTableLayoutPanel.Width +
                this.buttonsTableLayoutPanel.Margin.Right +
                this.Padding.Right;
            if ( this.ClientSize.Width < preferredClientWidth )
            {
                this.ClientSize = new Size( preferredClientWidth, this.ClientSize.Height );
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.HelpRequested" /> event.
        /// </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="hevent">   A <see cref="T:System.Windows.Forms.HelpEventArgs" /> that contains
        ///                         the event data. </param>
        protected override void OnHelpRequested( HelpEventArgs hevent )
        {
            // Get the active control
            Control activeControl = HelpUtils.GetActiveControl( this );

            // Figure out the context
            DataConnectionDialogContext context = DataConnectionDialogContext.AddProperty;
            if ( activeControl == this.propertyTextBox )
            {
                context = DataConnectionDialogContext.AddPropertyTextBox;
            }
            if ( activeControl == this.okButton )
            {
                context = DataConnectionDialogContext.AddPropertyOkButton;
            }
            if ( activeControl == this.cancelButton )
            {
                context = DataConnectionDialogContext.AddPropertyCancelButton;
            }

            // Call OnContextHelpRequested
            ContextHelpEventArgs e = new( context, hevent.MousePos );
            this._MainDialog.OnContextHelpRequested( e );
            hevent.Handled = e.Handled;
            if ( !e.Handled )
            {
                base.OnHelpRequested( hevent );
            }
        }

        /// <summary>   Processes Windows messages. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="m">    [in,out] The Windows <see cref="T:System.Windows.Forms.Message" /> to
        ///                     process. </param>
        protected override void WndProc( ref Message m )
        {
            if ( this._MainDialog.TranslateHelpButton && HelpUtils.IsContextHelpMessage( ref m ) )
            {
                // Force the ? in the title bar to invoke the help topic
                HelpUtils.TranslateContextHelpMessage( this, ref m );
            }
            base.WndProc( ref m );
        }

        /// <summary>   Sets ok button status. </summary>
        /// <remarks>   David, 2021-06-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void SetOkButtonStatus( object sender, EventArgs e )
        {
            this.okButton.Enabled = (this.propertyTextBox.Text.Trim().Length > 0);
        }

        /// <summary>   The main dialog. </summary>
        private readonly DataConnectionDialog _MainDialog;
    }
}
