//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.IO;

using Microsoft.SqlServerCe.Client;

namespace Microsoft.Data.ConnectionUI
{

    /// <summary>   A SQL CE connection properties. </summary>
    /// <remarks>   David, 2022-02-25. </remarks>
    public class SqlCeConnectionProperties : AdoDotNetConnectionProperties
	{
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2022-02-25. </remarks>
		public SqlCeConnectionProperties()
			: base("System.Data.SqlClient")
		{
		}

        /// <summary>   Resets the given propertyName. </summary>
        /// <remarks>   David, 2022-02-25. </remarks>
		public override void Reset()
		{
			base.Reset();
		}

        /// <summary>   Gets a value indicating whether this object is complete. </summary>
        /// <value> True if this object is complete, false if not. </value>
		public override bool IsComplete
		{
			get
			{

				string dataSource = this["Data Source"] as string;

				if (String.IsNullOrEmpty(dataSource))
				{
					return false;
				}

				// Ensure file extension: 
				if (!(Path.GetExtension(dataSource).Equals(".sdf", StringComparison.OrdinalIgnoreCase)))
				{
					return false;
				}

				return true;
			}
		}

        /// <summary>   Converts this object to a test string. </summary>
        /// <remarks>   David, 2022-02-25. </remarks>
        /// <returns>   This object as a string. </returns>
		protected override string ToTestString()
		{
			bool savedPooling = (bool)ConnectionStringBuilder["Pooling"];
			bool wasDefault = !ConnectionStringBuilder.ShouldSerialize("Pooling");
			ConnectionStringBuilder["Pooling"] = false;
			string testString = ConnectionStringBuilder.ConnectionString;
			ConnectionStringBuilder["Pooling"] = savedPooling;
			if (wasDefault)
			{
				ConnectionStringBuilder.Remove("Pooling");
			}
			return testString;
		}

        /// <summary>   Tests this object. </summary>
        /// <remarks>   David, 2022-02-25. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <exception cref="SqlCeException">               Thrown when a SQL Ce error condition occurs. </exception>
		public override void Test()
		{
			string testString = ToTestString();


			// Create a connection object
			SqlCeConnection connection = new SqlCeConnection();

			// Try to open it
			try
			{
				connection.ConnectionString = ToFullString();
				connection.Open();
			}
			catch (SqlCeException e)
			{
				// Customize the error message for upgrade required
				if (e.Number == m_intDatabaseFileNeedsUpgrading)
				{
					throw new InvalidOperationException(Resources.SqlCeConnectionProperties_FileNeedsUpgrading);
				}
				throw;
			}
			finally
			{
				connection.Dispose();
			}
		}

		private const int m_intDatabaseFileNeedsUpgrading = 25138;

	}
}

