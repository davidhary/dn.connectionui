using System.Reflection;

[assembly: AssemblyCompany( "Microsoft" )]
[assembly: AssemblyCopyright( "(c) 2009 Microsoft Corporation. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: System.Resources.NeutralResourcesLanguage( "en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly )]
