# Change log
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## Known Issues
```
Package 'isr.Microsoft.SqlServerCe.Client 3.5.5692' was restored using 
'.NETFramework,[...]' instead of the project target framework 
'net6.0-windows7.0'[ ... and] may not be fully compatible with 
[the] Microsoft.Data.ConnectionUI.SqlCeDataProvider [project.]
```
```
Package 'isr.System.Data.SqlServerCe 4.0.8876.1' was restored using 
'.NETFramework,[...]' instead of the project target framework 
'net6.0-windows7.0'[ ... and] may not be fully compatible with 
[the] Microsoft.Data.ConnectionUI.SqlCeDataProvider [project.]
```

## [9.0.8125] - 2022-03-31
* Pass tests using project reference mode.

## [9.0.8108] - 2022-03-14
* Package.

## [9.0.8097] - 2022-02-03
### Added
* SQL CE Data Provider: Convert to SDK Project.
* Use project/package reference condition, package 
deterministic and symbol files import.
* SQL CE packages.

## [9.0.8069] - 2021-02-03
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.


## [9.0.7836] - 2021-06-14
Imported from the data library and converted to MS SDK format for .NET standard, 
.NET 4.72 and .NET 5.0.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
```

[9.0.8125]: https://bitbucket.org/davidhary/dn.connectionui.git

